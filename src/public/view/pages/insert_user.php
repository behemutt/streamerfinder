<html> 
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Let's Player Finder</title>

    <!-- Bootstrap -->
    <link href="view/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="view/css/style.css">
    <script src="view/js/jquery-3.1.1.min.js"></script>
    <script src="view/js/search_page.js"></script>
</head>

<body>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="./view/bootstrap/js/bootstrap.min.js"></script>

    <div class="container" style="margin-top:1%;" id="contentDiv">
            <div class="row text-center">
            <h1>Cadastrar Usuario</h1>
            
           
            </div>
            <div class="col-md-4 col-md-offset-4">

                <form action="" method="POST">
                    <div><label class ="form-label">username:*</label></div>
                    <div><input type="text" name="username"><br><p></div>
                    <div><label class ="form-label">email:*</label></div>
                    <div><input type="text" name="email"><br><p></div>
                    <div><label class ="form-label">password:*</label></div>
                    <div><input type="password" name="password"><br><p></div>
                    <div><label class ="form-label">reapeat passwor:*</label></div>
                    <div><input type="password" name="repeat_password"><br><p></div>
                   
                </form>
                <div class = "row hidden" id="errorFeedback">
                <div class="alert alert-danger" style = "display: inline-block;" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    *Login or Password incorrect
                </div>
            </div>

            <div class = "row hidden" id="copyToClipboardFeedback">
                <div class="alert alert-success" style = "display: inline-block;" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Success:</span>
                    Connected!
                </div>
            </div>

            <div class="row text-center">
                <form id="submitForm" class="" method="post" action="javascript:onClickFormButton()">
                    <input type="submit" class="btn btn-primary" onclick="this.form.submited=this.value" id="RegisterButton" name="LoginButton" value="Register">
                </form>
                
            </div>
        </div>

</body>