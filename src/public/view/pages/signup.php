<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Let's Player Finder</title>

    <!-- Bootstrap -->
    <link href="view/bootstrap/css/bootstrap.min.css" rel="stylesheet">
   
    <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="view/css/style.css">
    <script src="view/js/jquery-3.1.1.min.js"></script>
	<script src="./view/js/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
	<script src="view/js/signup.js"></script>

</head>

<body>
<div class="container">
	<h2>Registration</h2>
	<div class="col-md-4 col-md-offset-4">
		<form action="#" name="signup" class="form-group has-danger" role="form" method="post">

			<div><label class ="form-label" for="username">Username</label></div>
			<input   type="text" class="form-control" name="username" id="username" placeholder="username"/>

			
			<div><label class ="form-label" for="email">Email</label></div>
			<input type="email" class="form-control" name="email" id="email" placeholder="email"/>

			
			<div><label class ="form-label" for="password">Password</label></div>
			<input type="password" class="form-control" name="password" id="password" placeholder="Enter the password"/>

			<div><label class ="form-label" for="password_control">Repeat Password</label></div>
			<input type="password" class="form-control" name="password_control" id="password_control" placeholder="repeat the password"/>


			<form id="submitForm" method="post" action="javascript:onClickFormButton()">
                        <div class="col-xs-6 form-group" style = "text-align: right;">
                            <input type="submit" class="btn btn-primary" onclick="this.form.submited=this.value" id="registerButton" name="registerButton" value="Register">
                        </div>
                        <div class="col-xs-6 form-group" style = "text-align: left;">
                            <button class="btn btn-secundary" id="cancelButton" name="cancelButton" value="Cancel" data-toggle="modal" data-target="#confirmClear">Cancel</button>
                        </div>
                    </form>
            
			

		</form>

		<div class = "row hidden" id="errorFeedback">
                <div class="alert alert-danger" style = "display: inline-block;" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    *Confirm the data and Try Again
                </div>
        </div>

		<div class = "row hidden" id="errorFeedback">
                <div class="alert alert-danger" style = "display: inline-block;" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    *Confirm the data and Try Again
                </div>
        </div>

	</div>
	<div class="row" id="resultDiv" >
	
		
	</div>
</div>
</body>
</html>