<!DOCTYPE HTML>

<head>
	<script type="text/javascript" src="view/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="view/js/jqueryui/jquery-ui.js"></script>
	<script type="text/javascript" src="view/js/jquery.idTabs.min.js"></script>	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Streamer Finder</title>

	<link href="view/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<script src="./view/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="view/js/dashboard.js"></script>
	<script type="text/javascript" src="view/js/sidebar.js"></script>
	
	<!-- jQuery Custom Scroller CDN -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

	<!--Bootstrap toggle-->
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
	<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
	
	<link href="view/js/jtable/themes/metro/blue/jtable.css" rel="stylesheet" type="text/css" />
	<link href="view/js/jqueryui/jquery-ui.css" rel="stylesheet" type="text/css" />
	
	<script src="view/js/jtable/jquery.jtable.no-edit.js" type="text/javascript"></script>
	<script type="text/javascript" src="view/js/jtable/extensions/jquery.jtable.editinline.js"></script>

	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" href="view/css/dashboard.css">
	<link rel="stylesheet" href="view/css/sidebar.css">

	<!--jqplot-->
	<script language="javascript" type="text/javascript" src="view/js/jqplot/jquery.jqplot.min.js"></script>
	<link rel="stylesheet" type="text/css" href="view/js/jqplot/jquery.jqplot.css" />

	<script type="text/javascript" src="view/js/jqplot/plugins/jqplot.canvasTextRenderer.js"></script>
	<script type="text/javascript" src="view/js/jqplot/plugins/jqplot.dateAxisRenderer.js"></script>
	<script type="text/javascript" src="view/js/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"></script>
	<script type="text/javascript" src="view/js/jqplot/plugins/jqplot.highlighter.js"></script>
	<script type="text/javascript" src="view/js/jqplot/plugins/jqplot.cursor.js"></script>
	<script type="text/javascript" src="view/js/jqplot/plugins/jqplot.enhancedLegendRenderer.js"></script>
	<script type="text/javascript" src="view/js/jqplot/plugins/jqplot.logAxisRenderer.js"></script>
	<script type="text/javascript" src="view/js/jqplot/plugins/jqplot.barRenderer.js"></script>

</head>

<body>
	<div clas="wrapper">
		<!-- Sidebar Holder -->
		<nav id="sidebar">
			<div id="dismiss">
				<i class="glyphicon glyphicon-arrow-left"></i>
			</div>

			<div class="sidebar-header">
				<h3>Let's Player Finder</h3>
			</div>

			<ul class="list-unstyled components">
				<li class="active">
					<a href="#searchSubmenu" data-toggle="collapse" aria-expanded="false">Search</a>
					<ul class="collapse list-unstyled" id="searchSubmenu">
						<li><a href="/search_youtube">Youtube</a></li>
						<!-- <li><a href="/search_twitch">Twitch</a></li> -->
						<!-- <li><a href="/search_douyu">Douyu</a></li> -->
					</ul>
				</li>
				<li>
					<a href="/dashboard">Analytics</a>
				</li>
				<li>
					<a href="#evaluateSubmenu" data-toggle="collapse" aria-expanded="false">Evaluate Channels</a>
					<ul class="collapse list-unstyled" id="evaluateSubmenu">
						<li><a href="/evaluate_youtube_channels">Channels Scores</a></li>
					</ul>
				</li>
			</ul>
		</nav>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container navbar-header">
				<button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
					<i class="glyphicon glyphicon-align-justify"></i>
				</button>
			</div>
		</nav>
		<div id="content">
			<nav class="navbar navbar-default">
				<div class="container-fluid" id="contentDiv">
					<div id="main">
						<form class="form-inline form-group has-danger col" role="form" id="selectProject">
							<div class="row">		
								<div class="input-group">
									<label class ="form-label" style="margin:1%">Projeto:  </label>
									<select onchange="onChangeProjectDropdown()" class="btn btn-default dropdown-toggle" id="inputGroupSelect">
									</select>
									<!--button id="updateProjectButton" class="btn btn-outline-secondary" onclick="onClickUpdateProject()" type="button" style="margin:1%">Update</button-->
								</div>
							</div>
						</form>	
					</div>
					<div class="row" >
						
							<ul class="idTabs nav nav-tabs">
								<li><a id="dashboardTabId" href="#dashboard" class="nav-link active" >Dashboard</a></li>
								<li><a id="sellingDataTabId" href="#sellingData">Selling Data</a></li>
								<li><a id="eventsTabId" href="#eventsTab">Events</a></li>
								<li><a id="reviewVideoTabId" href="#reviewVideoTab">Review Videos <span id="reviewVideosCountBadge" class="badge" style="display: none;">0</span></a></li>
								<li><a id="reviewChannelsTabId" href="#reviewChannelsTab">Review Channels <span id="reviewChannelsCountBadge" class="badge" style="display: none;">0</span></a></li>
							</ul>
								
							<div id="dashboard" style = "text-align: center;">
								<input id="twitchViewsData" type="file" style="display: none" />

								<h2 >Views X Wishlist</h2>

								<form>					
									<br>						
									<div class="col-4"></div>
									<div class="col-4 text-right" style='margin-right:1%'>
										<div class="form-inline row">
											<div id="reportrangedashboard" class="form-control col">
												<i></i>&nbsp;
												<span></span> <i class="caret"></i>
											</div>
										</div>
										<p>
										<div class="form-inline row">
											<button id="exportPeriodReport" onclick="onClickExportReport()" type = "button" class ="btn btn-secondary">Export Period Report</button>
											<button id="updateTwitchViews" onclick="onClickImportTwitchData()" type="button" class="btn btn-primary">Update Twitch Data</button>
											<button id="resetChartZoomButton" type="button" class="btn btn-secundary">Reset Zoom</button>
										</div>
										</p>
									</div>
								</form>

								<div class="row">
									<div id="resizableChartDiv" class="ui-widget-content" style='margin-right:1%'>
										<div id="chartdiv" style="height:96%; width:96%;"></div>
									</div>
								</div>
							</div>
							
							<div id="reviewVideoTab">
								<div class="text-center"><h2>Videos Review</h2></div>
								<form>					
									<br>						
									<div class="col-4"></div>
									<div class="col-4 text-right">
										<div class="form-inline row">
											<p>
												<label>Current query: </label>
												<button id="currentQuery" type="button" class="btn btn-primary" data-toggle="modal" data-target="#queryInputModal">
													Set a query here
												</button>
											</p>
										</div>

										<div class="form-inline row">
											<div id="reportrangereviewvideos" class="form-control col">
												<i></i>&nbsp;
												<span></span> <i class="caret"></i>
											</div>
										</div>
										
										<p>
											<div class="form-inline row">
												<label>
													Reviewed videos: <input id="reviewedCheckbox" type="checkbox" data-toggle="toggle" data-on="Show" data-off="Hide">
												</label>

											</div>
										</p>
									</div>
								</form>
								
								<div class="row">
									<div id="video-data-grid" class="text-center"></div>	
								</div>
							</div>

							<div id="reviewChannelsTab">
								<div class="text-center"><h2>Videos Review</h2></div>
								<form>					
									<br>						
									<div class="col-4"></div>
									<div class="col-4 text-right">
										<!-- <div class="form-inline row">
											<p>
												<label>Current query: </label>
												<button id="currentQuery" type="button" class="btn btn-primary" data-toggle="modal" data-target="#queryInputModal">
													Set a query here
												</button>
											</p>
										</div> -->

										<div class="form-inline row">
											<div id="reportrangereviewchannels" class="form-control col">
												<i></i>&nbsp;
												<span></span> <i class="caret"></i>
											</div>
										</div>
										
										<p>
											<div class="form-inline row">
												<button id="exportChannelsCSV" onclick="onClickExportChannelsCSV()" type="button" class="btn btn-success">Export CSV</button>
												<label>
													Reviewed channels: <input id="reviewedChannelCheckbox" type="checkbox" data-toggle="toggle" data-on="Show" data-off="Hide">
												</label>

											</div>
										</p>
									</div>
								</form>
								
								<div class="row">
									<div id="channel-data-grid" class="text-center"></div>	
								</div>
							</div>

							<div id="eventsTab">
								<div class="text-center"><h2>Events</h2></div>
								<form>					
									<br>						
									<div class="col-4"></div>
									<div class="col-4 text-right">
										<div class="form-inline row">
											<div id="reportrangeevents" class="form-control col">
												<i></i>&nbsp;
												<span></span> <i class="caret"></i>
											</div>
										</div>
										<br>
									</div>
								</form>
								
								<div class="row">
									<div id="events-data-grid" class="text-center"></div>	
								</div>
							</div>

							<div id="sellingData">
								<input id="sellingDataImportWishlistsData" type="file" style="display: none" />
								<input id="sellingDataImportSellingData" type="file" style="display: none" />
								<div class="text-center"><h2>Selling Data</h2></div>
								<!--Filter form-->
								<form>					
									<br>						
									<div class="col-4"></div>
									<div class="col-4 text-right">
										<div class="form-inline row">
											<div id="reportrangesellingdata" class="form-control col">
												<i></i>&nbsp;
												<span></span> <i class="caret"></i>
											</div>
										</div>
									</div>
									<br>
								</form>

								<!--Data Grid-->
								<div class="row">
									<div id="selling-data-grid" class="text-center"></div>	
								</div>
								<br>
							</div>
							<div id="downloadCSVArea" style="text-align: center;">
							<div id="downloadChannelsCSVArea" style="text-align: center;">
					</div>
					</div>
				</div>
			</nav>
		</div>
	</div>

	<!-- Modal for inputing queries -->
	<div class="modal" id="queryInputModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Queries</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>
						<input id="addQueryInputText" placeholder="New query here"/>&nbsp;<button type="button" id="addQueryButton" class="btn btn-primary">+</button>
					</p>
				<div id="queries-data-grid" class="text-center"></div>	
				</div>
				<div class="modal-footer">
					<button id="saveQuery" onclick="onClickSaveSelectedQuery()" type="button" class="btn btn-primary">Save changes</button>
					<button id="saveQueryAndSearch" onclick="onClickSaveSelectedQueryAndSearch()" type="button" class="btn btn-success">Save &amp; Search</button>
					<button id="cancelSaveQuery" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal-animate-full-screen">
		<label class="center-label-modal">Loading...</label>
	</div>

	<div class="overlay"></div>

</body>
</html>