<html> 
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Let's Player Finder</title>

    <!-- Bootstrap -->
    <link href="view/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="./view/bootstrap/js/bootstrap.min.js"></script>

    <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="view/css/style.css">
    <script src="view/js/jquery-3.1.1.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

	<script type="text/javascript" src="view/js/sidebar.js"></script>
    <script src="view/js/search_douyu_page.js"></script>
    <link rel="stylesheet" href="view/css/sidebar.css">
</head>

<body>
    <div clas="wrapper">
        <!-- Sidebar Holder -->
		<nav id="sidebar">
			<div id="dismiss">
				<i class="glyphicon glyphicon-arrow-left"></i>
			</div>

			<div class="sidebar-header">
				<h3>Let's Player Finder</h3>
			</div>

			<ul class="list-unstyled components">
				<li class="active">
					<a href="#searchSubmenu" data-toggle="collapse" aria-expanded="false">Search</a>
					<ul class="collapse list-unstyled" id="searchSubmenu">
						<li><a href="/search_youtube">Youtube</a></li>
						<li><a href="/search_twitch">Twitch</a></li>
						<li><a href="/search_douyu">Douyu</a></li>
					</ul>
				</li>
				<li>
					<a href="/dashboard">Analytics</a>
                    <a href="/evaluate_youtube_channels">Evaluate Channels</a>
				</li>
			</ul>
		</nav>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container navbar-header">
				<button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
					<i class="glyphicon glyphicon-align-justify"></i>
				</button>
			</div>
        </nav>

        <div id="content">
            <nav class="navbar navbar-default">
                <div class="container-fluid" id="contentDiv">
                    <div class="row" style="text-align: center;">
                    <h1>Let's Player Finder</h1>
                    <h6>(Pre-Alpha v0.11)</a></h6>
                    <h5>by <a target="_blank" href="https://twitter.com/behemutt">BEHEMUTT</a></h5>
                    </div>
                    <form class="form-inline form-group has-danger" role="form" id="similarGamesForm">
                        <div class="col-xs-6" style = "text-align: right;"><label class ="form-label">Similar games:*</label></div>
                        <div class="col-xs-6" style = "text-align: left;">
                            <input type="text" class="form-control"  id="similarGamesField" placeholder="The Binding of Isaac"/>
                            <button type="button" class="btn btn-outline-danger" onclick="addSimilarGame()">+</button><p>
                        </div>
                        <ul class="list-inline" id="similarGamesTable" style="text-align: center;">
                        </ul>
                    </form>

                    <div class = "row hidden" id="errorFeedback" style="text-align: center;" style="text-align: center;">
                        <div class="alert alert-danger" style = "display: inline-block;" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            Please fill all required fields.
                        </div>
                    </div>

                    <div class = "row hidden" id="copyToClipboardFeedback" style="text-align: center;" style="text-align: center;">
                        <div class="alert alert-success" style = "display: inline-block;" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Success:</span>
                            All current data was copied to clipboard!
                        </div>
                    </div>

                    <form id="submitForm" method="post" action="javascript:onClickFormButton()">
                        <div class="col-xs-6 form-group" style = "text-align: right;">
                            <input type="submit" class="btn btn-primary" onclick="this.form.submited=this.value" id="searchButton" name="searchButton" value="Search">
                        </div>
                        <div class="col-xs-6 form-group" style = "text-align: left;">
                            <button class="btn btn-secundary" id="clearButton" name="clearButton" value="Clear Results" data-toggle="modal" data-target="#confirmClear">Clear Results</button>
                        </div>
                    </form>

                    <div class="modal-animate">
                        <label class="center-label-modal">Please, do not update the page while the search isn't finished.</label>
                    </div> 
                    <br><br><br>
                    <div class="row" id="resultDiv">
                    <div id="downloadCSVArea" style="text-align: center;">

                    </div>
                        <table id="search-data-grid" class="table table-striped table-responsive"></table>
                    </div>
                </div>
            </nav>
        </div>
    </div>

<div class="overlay"></div>

<div id="confirmClear" class="modal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>All your current results will be excluded. Are you sure you want to continue?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick = "clearResults()">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>

</body>
</html>
