<html> 
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Let's Player Finder</title>

    <!-- Bootstrap -->
    <link href="view/bootstrap/css/bootstrap.min.css" rel="stylesheet">
   
    <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="view/css/style.css">
    <script src="view/js/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <script src="view/js/login_page.js"></script>
</head>

<body>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="./view/bootstrap/js/bootstrap.min.js"></script>
    

    <div class="container" style="margin-top:1%;" id="contentDiv">
        <div class="row text-center">
        <h1>Let's Player Finder</h1>
        <h6>(Pre-Alpha v0.11)</a></h6>
        <h5>by <a target="_blank" href="https://twitter.com/behemutt">BEHEMUTT</a></h5>
        </div>
        <div class="col-md-4 col-md-offset-4">
            <form class="form-group has-danger" role="form" >
                <div><label class ="form-label">Username:*</label></div>
                <div><input type="text" class="form-control" id="username" placeholder="Your Login"/><p></div>
                <div><label class ="form-label">Password:*</label></div>
                <div><input type="password" class="form-control" id="password"/><p></div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="rememberme">
                    <span class="form-check-label" for="rememberme">Remember me</span>
                </div>
            </form>
            
            <div class = "row hidden" id="errorFeedback">
                <div class="alert alert-danger" style = "display: inline-block;" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    *Login or Password incorrect
                </div>
            </div>

            <div class = "row hidden" id="copyToClipboardFeedback">
                <div class="alert alert-success" style = "display: inline-block;" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Success:</span>
                    Connected!
                </div>
            </div>
            <div class="row text-center">
                <form id="submitForm" class="" method="post" action="javascript:loginValidate()">
                    <input type="submit" class="btn btn-primary" id="LoginButton" name="LoginButton" value="Login">
                </form>
                <a href="#recoverPassword" rel="modal:open" id="recover_link">Recover Password</a>
            </div>
        </div>        

<div id="recoverPassword" class="modal" role="dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <p>Aqui informar o e-mail</p>
        <a href="#" rel="modal:close">Close</a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick = "clearResults()">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>

</body>
</html>
