var similarGamesCount;
var addedSimilarGames = [];
var resultJson;
var gamesData = null;
var csvLinkGoogle;
var csvDataExcel;

function initSearchScreen() {
	similarGamesCount = 0;
}

function addSimilarGame() {
	var similarGamesForm = $("#contentDiv").find("#similarGamesForm");
	var similarGamesField = similarGamesForm.find("#similarGamesField");
	var games = similarGamesField.val().split('|');

	if(games.length > 0) {
		for(var i = 0; i < games.length; ++i) {
			addSimilarGameString(games[i]);
		}
	}
}

function addSimilarGameString(game) {
	var similarGamesForm = $("#contentDiv").find("#similarGamesForm");
	var similarGamesField = similarGamesForm.find("#similarGamesField");
	var similarGamesTable = similarGamesForm.find("#similarGamesTable");

	if(addedSimilarGames.indexOf(game.toLowerCase()) < 0 && game.length > 0) {
		addedSimilarGames.push(game.toLowerCase());
		similarGamesTable.append("<li><label id=\"game" + similarGamesCount + "\">" + game + "</label> " + 
				"<button class=\"btn btn-outline-secundary btn-sm\" id=\"similarGamesButton" + similarGamesCount + "\" type=\"button\">X</button></li>");

		similarGamesField.val('');

		$('#similarGamesButton' + similarGamesCount).click(function() {
				var elementToRemoveIndex = addedSimilarGames.indexOf($(this).closest("li").find("label").html().toLowerCase());
				if(elementToRemoveIndex >= 0)
				{
				addedSimilarGames.splice(elementToRemoveIndex, 1);
				}

				$(this).closest("li").remove();
				});
		similarGamesCount++;
	}
}

var channelsFound;

function onClickFormButton()
{
	var btn = $(document.activeElement);
	channelsFound = 0;
	if(btn[0].value == "Search") {
		beginSearch(true);
	}
}

function beginSearch(first)
{
	var btn = $(document.activeElement);

	//if(btn.context.value == "Search")
	{
		//TODO: validate data
		var similarGamesForm = $("#contentDiv").find("#similarGamesForm");

		var keywords = similarGamesForm.find("#keywords").val();
		var videoAge = similarGamesForm.find("#videoAge").val();
		var minSubscribers = similarGamesForm.find("#minSubscribers").val();
		var maxSubscribers = similarGamesForm.find("#maxSubscribers").val();
		var similarGames = '';
		var languages = 'none';//similarGamesForm.find("#preferedLanguage").val();

		//validating input
		if(keywords == "" || videoAge == "" || minSubscribers == "" || maxSubscribers == "" || addedSimilarGames.length == 0) {
			$("#contentDiv").find("#errorFeedback").removeClass("hidden");
			return;
		}
		else {
			$("#contentDiv").find("#errorFeedback").addClass("hidden");
		}

		for(var i = 0; i < addedSimilarGames.length; ++i) {
			similarGames += '/' + addedSimilarGames[i];
		}
		$body = $("body");
		$body.addClass("loading");
		//$("#contentDiv").find("#resultDiv").find("#downloadCSVArea textarea").remove();        
		//$("#contentDiv").find("#resultDiv").find("#downloadCSVArea button").remove();        

		$.ajax({
			type: 'GET',
			url: 'douyu/search' + similarGames,
			dataType: 'text',
			success: function (result) {
				var resultArea = $("#search-data-grid");
				var dataJson;
				resultJson = JSON.parse(result);
				console.log(resultJson);
				
				var previousGameDataLength = 0;

				if(typeof (gamesData) === 'undefined' || gamesData == null) {
					if(!resultJson.hasOwnProperty('data')) {
						$body.removeClass("loading");
						return;
					}
					resultArea.append('<tr><th class=\"text-right\">#</th><th>Channel Link</th><th>Channel contact</th><th>View count</th><th>Followers</th><th>Last Activity</th><th>Game Found</th></tr>');//<th>Similar game</th><th>Country</th></tr>');
					gamesData = resultJson['data'];
				}
				else {
					console.log(resultArea);
					if(!resultJson.hasOwnProperty('data')) {
						$body.removeClass("loading");
						return;
					}
					// previousGameDataLength = gamesData.length;
					gamesData = gamesData.concat(resultJson['data']);
				}
				channelsFound += gamesData.length;
				var csvLinkGoogle ="data:application/csv;charset=utf-8,";
				csvDataGoogle = "Channel Link\tChannel Contact\tView Count\tFollowers\tLast Activity\tGame Found\r\n";//\tSimilar Game\tCountry\r\n";
				var csvLinkExcel ="data:application/csv;charset=utf-8,";
				csvDataExcel = "Channel Link;Channel Contact;View Count;Followers;Last Activity;Game Found\r\n";//;Similar Game;Country\r\n";

				for(var i = previousGameDataLength; i < gamesData.length; ++i) {
					var link = gamesData[i].authorPage;
					// var country = (gamesData[i].country == null ? '-' : gamesData[i].country);
					resultArea.append("<tr><td class=\"text-right\">"+(i+1)+"</td><td><a target = \"_blank\" href=" +  link + ">" + link + "</a></td><td>" + gamesData[i].contactPage + "</td><td>" + gamesData[i].totalViews + "</td><td>" + gamesData[i].followers + "</td><td>" + gamesData[i].lastActivity + "</td><td>" + gamesData[i].gameFound +"</td></tr>");
				}

				for(var i = 0; i < gamesData.length; ++i) {
					// var country = (gamesData[i].country == null ? '-' : gamesData[i].country);
					var link = gamesData[i].authorPage;
				  	csvDataGoogle += link + "\t" + gamesData[i].contactPage + "\t" + gamesData[i].totalViews + "\t" + gamesData[i].followers + "\t" + gamesData[i].lastActivity + "\t" + gamesData[i].gameFound +"\r\n";// + "\t" + gamesData[i].similarGame + "\t" + country +"\r\n";
					csvDataExcel += link + ";" + gamesData[i].contactPage + ";" + gamesData[i].totalViews + ";" + gamesData[i].followers + ";" + gamesData[i].lastActivity + ";" + gamesData[i].gameFound + "\r\n";// + ";" + gamesData[i].similarGame + ";" + country + "\r\n";
				}

				csvLinkExcel += encodeURIComponent(csvDataExcel);
				csvLinkGoogle += encodeURIComponent(csvDataGoogle);
				$body.removeClass("loading");

				if($("#contentDiv").find("#resultDiv").find("#downloadCSVArea textArea").length == 0) {
					$("#contentDiv").find("#resultDiv").find("#downloadCSVArea").append("<textarea id=\"tempTextArea\" class=\"hidden\"/>");
					$("#contentDiv").find("#resultDiv").find("#downloadCSVArea").append("<textarea id=\"tempTextAreaExcel\" class=\"hidden\"/>");
					//TODO: The excel value is being accumulated. Uncomment the next line after fixing it
					//$("#contentDiv").find("#resultDiv").find("#downloadCSVArea").append("<button class=\"btn btn-link\" onclick=\"downloadExcelCSV()\">Download CSV (MS Excel)</button>");
					$("#contentDiv").find("#resultDiv").find("#downloadCSVArea").append("<button class=\"btn btn-link\" onclick=\"downloadGeneralCSV()\">Download CSV</button>");
					$("#contentDiv").find("#resultDiv").find("#downloadCSVArea").append("<button data-clipboard-target=\"#tempTextArea\" id=\"copyToClipboardButton\" type=\"button\" class=\"btn btn-link\"\">Copy to clipboard</btn>");
				
					//$("#contentDiv").find("#resultDiv").find("#downloadCSVArea").find("#tempTextArea").val(csvDataGoogle);
					$("#contentDiv").find("#resultDiv").find("#downloadCSVArea").find("#copyToClipboardButton").click(function() {
						var textArea = $("#contentDiv").find("#resultDiv").find("#downloadCSVArea").find("#tempTextArea");
						textArea.removeClass("hidden");
						textArea.select();
						document.execCommand('copy');
						textArea.addClass("hidden");
						$("#contentDiv").find("#copyToClipboardFeedback").removeClass("hidden").delay(3000).queue(function(){
							$("#contentDiv").find("#copyToClipboardFeedback").addClass("hidden");                        
						});
					});
				}
				else {
					$("#contentDiv").find("#resultDiv").find("#downloadCSVArea").find("#tempTextArea").val(csvDataGoogle);
					$("#contentDiv").find("#resultDiv").find("#downloadCSVArea").find("#tempTextAreaExcel").val(csvDataExcel);
				}
		},
		error : function (a,b,c) {
				console.log(a);
				console.log(b);
				console.log(c);
				$body.removeClass("loading");
			}
		});
	}
	//else
	//{
		// if(!confirm("All your current results will be excluded. Are you sure you want to continue?")) {
		//     return;
		// }
	//	console.log("Clearing");
	//}
}

function downloadExcelCSV() {
	var textFileAsBlob = new Blob([csvDataExcel], {type:'text/plain'});
	var fileNameToSaveAs = 'StreamersFoundMSExcel.csv';

	var downloadLink = document.createElement("a");
	downloadLink.download = fileNameToSaveAs;
	downloadLink.innerHTML = "Download File";
	if (window.URL != null)
	{
		// Chrome allows the link to be clicked
		// without actually adding it to the DOM.
		downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
	}
	else
	{
		// Firefox requires the link to be added to the DOM
		// before it can be clicked.
		downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
		downloadLink.onclick = destroyClickedElement;
		downloadLink.style.display = "none";
		document.body.appendChild(downloadLink);
	}

	downloadLink.click();
}

function downloadGeneralCSV() {
	var textFileAsBlob = new Blob([csvDataGoogle], {type:'text/plain'});
	var fileNameToSaveAs = 'StreamersFound.csv';

	var downloadLink = document.createElement("a");
	downloadLink.download = fileNameToSaveAs;
	downloadLink.innerHTML = "Download File";
	if (window.URL != null)
	{
		// Chrome allows the link to be clicked
		// without actually adding it to the DOM.
		downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
	}
	else
	{
		// Firefox requires the link to be added to the DOM
		// before it can be clicked.
		downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
		downloadLink.onclick = destroyClickedElement;
		downloadLink.style.display = "none";
		document.body.appendChild(downloadLink);
	}

	downloadLink.click();
}

function clearResults() {
	var similarGamesForm = $("#contentDiv").find("#similarGamesForm");
	similarGamesForm.find("#keywords").val('');
	similarGamesForm.find("#videoAge").val('');
	similarGamesForm.find("#minSubscribers").val('');
	similarGamesForm.find("#maxSubscribers").val('');
	similarGamesForm.find("#similarGamesField").val('');
	//similarGamesForm.find("#preferedLanguage").prop('selectedIndex', 0);

	addedSimilarGames = [];
	similarGamesForm.find("#similarGamesTable li").remove();

	//remove previous rows
	$("#contentDiv").find("#resultDiv").find("#resultTable tbody").remove();
	$("#contentDiv").find("#resultDiv").find("#resultTable th").remove();
	$("#contentDiv").find("#resultDiv").find("#resultTable tr").remove();
	$("#contentDiv").find("#resultDiv").find("#downloadCSVArea textarea").remove();        
	$("#contentDiv").find("#resultDiv").find("#downloadCSVArea button").remove();   

// 	$.ajax({
// type: 'GET',
// url: 'clear',
// dataType: 'text',
// success: function (result) {
// console.log(result);
// },
// error : function (a,b,c)
// {
// console.log(a);
// console.log(b);
// console.log(c);
// }
// });

delete resultJson;
delete gamesData;
gamesData = null;
}

function getLanguagePerCode(code) {
	switch(code)
	{
		case "en":
			return "English";
		case "zh":
			return "Chinese";
		case "nl":
			return "Dutch";
		case "fr":
			return "French";
		case "de":
			return "German";
		case "it":
			return "Italian";
		case "ko":
			return "Korean";
		case "pt":
			return "Portuguese";
		case "ru":
			return "Russian";
		case "es":
			return "Spanish";
		case "sv":
			return "Swedish";            
	}
}

$(document).ready(function() {
		initSearchScreen();
        initSidebar();

		$("#contentDiv").find("#similarGamesForm").find("#similarGamesField").keypress(function(event) {
				if(event.which == 13) {
					addSimilarGame();
				}
			});
		});

$(document).bind({
ajaxStart: function() {$body.addClass("loading");},
ajaxStop: function() {$body.removeClass("loading");}
});
