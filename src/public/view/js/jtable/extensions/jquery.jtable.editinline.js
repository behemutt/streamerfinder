/** JTABLE editinline extension 
 by : nubuntu
**/

(function ($) {
	var base={
		_createCellForRecordField:$.hik.jtable.prototype._createCellForRecordField,
		_updateRowTexts:$.hik.jtable.prototype._updateRowTexts	

	}
    $.extend(true, $.hik.jtable.prototype, {
		options: {
			editinline:{enable:false,img:""}
		},
		/** Overrides Method
		*****************************/
		_createCellForRecordField: function (record, fieldName) {
			if(this.options.editinline.enable){
            	return $('<td></td>')
                .addClass(this.options.fields[fieldName].listClass)
                .append((this._getDisplayTextEditInline(record, fieldName)));
			}else{
			     return $('<td></td>')
                .addClass(this.options.fields[fieldName].listClass)
                .append((this._getDisplayTextForRecordField(record, fieldName)));

			}
        },
		/** Overrides Method
		*****************************/
		_updateRowTexts: function ($tableRow) {
            var record = $tableRow.data('record');
            var $columns = $tableRow.find('td');
			if(this.options.editinline){
            for (var i = 0; i < this._columnList.length; i++) {
				if(this.options.fields[this._columnList[i]].type=='date'){
                var displayItem = this._getDisplayTextForRecordField(record, this._columnList[i]);
                $columns.eq(this._firstDataColumnOffset + i).html(displayItem || '');				
				}else{
                var displayItem = this._getDisplayTextEditInline(record, this._columnList[i]);
                	$columns.eq(this._firstDataColumnOffset + i).html(displayItem || '');
				}
            }
			}else{
            for (var i = 0; i < this._columnList.length; i++) {
                var displayItem = this._getDisplayTextForRecordField(record, this._columnList[i]);
                $columns.eq(this._firstDataColumnOffset + i).html(displayItem || '');
            }
			}
            this._onRowUpdated($tableRow);
        },		
		_getDisplayTextEditInline:function (record, fieldName) {
			var field = this.options.fields[fieldName];
            var fieldValue = record[fieldName];
			if (field.display) {
					return field.display({ record: record });
            }
            if (field.type == 'date') {
					return this._editInline_date(record,fieldName);
			} else if (field.type == 'checkbox') {
                return this._editInline_checkbox(record, fieldName);
            } else if (field.type == 'textarea') {
                return this._editInline_textarea(record, fieldName);
            } else if (field.options) { //combobox or radio button list since there are options.
                var options = this._getOptionsForField(fieldName, {
                    record: record,
                    value: fieldValue,
                    source: 'list',
                    dependedValues: this._createDependedValuesUsingRecord(record, field.dependsOn)
                });
                return this._editInline_options(options, fieldValue,record,fieldName);
            } else { //other types

					return this._editInline_default(record,fieldName);
            }
        },
        _editInline_options: function (options, value,record,fieldName) {
			var self = this;
			var val = value;
			var valtext ='';
			var field = this.options.fields[fieldName];
			if(typeof field.edit === 'undefined'){
				field.edit = true;
			};
            var $inputhtml = $('<select></select>');
			$inputhtml.css('background-repeat','no-repeat');
			$inputhtml.css('background-position','right center');
			for (var i = 0; i < options.length; i++) {
                $inputhtml.append('<option value="' + options[i].Value + '"' + (options[i].Value == value ? ' selected="selected"' : '') + '>' + options[i].DisplayText + '</option>');
				if(options[i].Value == value){
					valtext = options[i].DisplayText;
				}
            }
			var defaulttext = (valtext) ? valtext :'Null';  
			var $txt = $('<span>' + defaulttext + '</span>');
			$txt.click(function(){
				if($(this).children().length < 1){
					$inputhtml.val(val);
					$(this).html($inputhtml);
					$inputhtml.bind('change blur focusout',function(){											
						$(this).css('background-image','url("' + self.options.editinline.img + 'loading.gif")');
						var postData = {};
						postData[fieldName]=$(this).val();
						postData[self._keyField]=record[self._keyField];

						// if(self._editInline_ajax(postData)){
						// 	val = $(this).val();
						// 	$txt.html($(this).find("option:selected").text());
						// 	record[fieldName]=$(this).val();
						// 	$(this).css('background','none');
						// 	self._showUpdateAnimationForRow($txt.closest("tr"));
						// }

						var selfText = this;
						self._showUpdateAnimationForRow($txt.closest("tr"));
						self._editInline_ajax(postData, function(success) {
							if(success) {
								val = $(selfText).val();
								$txt.html($(selfText).find("option:selected").text());
								record[fieldName]=$(selfText).val();
							}
							else {
								$(selfText).val(val);
								$txt.html($(selfText).find("option:selected").text());
								record[fieldName] = val;
							}
							$(selfText).css('background','none');
							//self._showUpdateAnimationForRow($txt.closest("tr"));
							});

					});
					$inputhtml.focus();
					
				}
			
			});
			if(field.edit){	
				return $txt;	
			}else{
				return defaulttext;
			}
        },		
		_editInline_date:function(record,fieldName){
	            var self = this;
				var field = this.options.fields[fieldName];
				if(typeof field.edit === 'undefined'){
					field.edit = true;
				};
    	        var fieldValue = record[fieldName];
				var displayFormat = field.displayFormat || this.options.defaultDateFormat;
				var defaultval = (fieldValue) ? fieldValue :'2010-01-01';  
        		var $txt = $('<span>' + defaultval + '</span>');
				$txt.click(function(){
					if($(this).children().length < 1){
						var $inputhtml = $('<input type="text" value="' + $(this).html() + '"/>');
						$inputhtml.css('background-repeat','no-repeat');
						$inputhtml.css('background-position','right center');
						$(this).html($inputhtml);							
							$inputhtml.datepicker({dateFormat:displayFormat,onClose: function(calDate) {
							$(this).css('background-image','url(' + self.options.editinline.img + 'loading.gif)');
							var postData = {};
							postData[fieldName]=$(this).val();
							postData[self._keyField] = record[self._keyField];
							var selfTxt = this;

							self._editInline_ajax(postData, function(success) {
								if(success) {
									$txt.html($(selfTxt).val());
									record[fieldName] = $(selfTxt).val();
								}
								else {
									$txt.html(fieldValue);
									record[fieldName] = fieldValue;
								}
								$(selfTxt).css('background','none');
								self._showUpdateAnimationForRow($txt.closest("tr"));
								});
							
						}});
						$inputhtml.focus();
					}
				});	
										
				if(field.edit){	
					return $txt;	
				}else{
					return defaultval;
				}

		},
		_editInline_checkbox:function(record,fieldName){
	            var self = this;
				var field = this.options.fields[fieldName];
				if(typeof field.edit === 'undefined'){
					field.edit = true;
				};
				var defaultValue = this.options.fields[fieldName]['defaultValue'];
    	        var fieldValue = record[fieldName];
				
				var $imgnoedit = ((defaultValue && fieldValue == null) || (fieldValue != 0 && fieldValue != null)) ?
					$('<span val="1" style="cursor:pointer; color:green;" class="glyphicon glyphicon-ok" </span>') : //src="' + this.options.editinline.img + 'apply.png"></img>') :
					$('<span val="0" style="cursor:pointer; color:red;" class="glyphicon glyphicon-remove" </span>');//src="' + this.options.editinline.img + 'cross.png"></img>');
					
				var $img = ((defaultValue && fieldValue == null) || (fieldValue != 0 && fieldValue != null)) ?
					$('<span val="1" style="cursor:pointer; color:green;" class="glyphicon glyphicon-ok" </span>')://src="' + this.options.editinline.img + 'apply.png"></img>') :
					$('<span val="0" style="cursor:pointer; color:red;" class="glyphicon glyphicon-remove" </span>');//src="' + this.options.editinline.img + 'cross.png"></img>');
					
				$img.click(function(){
					var postData = {};
					postData[fieldName]=($(this).attr('val'))!=0?0:1;
					postData[self._keyField] = record[self._keyField];

					if(self._editInline_ajax(postData, null, record, $img.closest("tr"))){
						if($(this).attr('val')=='0'){
							record[fieldName]=1;
							$(this).attr('title','click to uncheck');
							$(this).attr('val','1');
							$(this).attr('src',self.options.editinline.img + 'apply.png');	
						}else{
							record[fieldName]=0;
							$(this).attr('title','click to check');
							$(this).attr('val','0');
							$(this).attr("src",self.options.editinline.img + 'cross.png');	
						}
						self._showUpdateAnimationForRow($img.closest("tr"));
					}
				});	
										
				if(field.edit){	
					return $img;	
				}else{
					return $imgnoedit;
				}

		},
		_editInline_textarea:function(record,fieldName){
	            var self = this;
				var field = this.options.fields[fieldName];
				if(typeof field.edit === 'undefined'){
					field.edit = true;
				};
    	        var fieldValue = record[fieldName];
				var defaultval = (fieldValue) ? fieldValue :'&nbsp;&nbsp;&nbsp;';  
				var $txt = $('<span>' + defaultval + '</span>');
				$txt.click(function(){
					if($(this).children().length < 1){
						var $inputhtml = $('<textarea>' + $(this).html() + '</textarea>');
						$inputhtml.css('background-repeat','no-repeat');
						$inputhtml.css('background-position','right center');
						$(this).html($inputhtml);
						$inputhtml.bind('change blur focusout',function(){											
							$(this).css('background-image','url("' + self.options.editinline.img + 'loading.gif")');
							var postData = {};
							postData[fieldName]=$(this).val();
							postData[self._keyField] = record[self._keyField];
							var selfTxt = this;
							self._editInline_ajax(postData, function(success) {
								if(success) {
									$txt.html($(selfTxt).val());
									record[fieldName] = $(selfTxt).val();
								}
								else {
									$txt.html(fieldValue);
									record[fieldName] = fieldValue;
								}
								$(selfTxt).css('background','none');
								self._showUpdateAnimationForRow($txt.closest("tr"));
								});
						});
						$inputhtml.focus();
					}
				});	
										
				if(field.edit){	
					return $txt;	
				}else{
					return defaultval;
				}

		},
		_editInline_default:function(record,fieldName){
	            var self = this;
				var field = this.options.fields[fieldName];
				if(typeof field.edit === 'undefined'){
					field.edit = true;
				};
				
    	        var fieldValue = record[fieldName];
				var defaultval = (fieldValue) ? fieldValue :'&nbsp;&nbsp;&nbsp;';

				if(fieldValue === 0) {
					defaultval = '0';
				}

				var $txt = $('<span>' + defaultval + '</span>');
				$txt.click(function(){
					if($(this).children().length < 1){
						var $inputhtml = $('<input type="text" value="' + $(this).html() + '"/>');
						$inputhtml.css('background-repeat','no-repeat');
						$inputhtml.css('background-position','right center');
						$(this).html($inputhtml);
						$inputhtml.bind('change blur focusout',function(){											
							$(this).css('background-image','url("' + self.options.editinline.img + 'loading.gif")');
							var postData = {};
							postData[fieldName]=$(this).val();
							postData[self._keyField] = record[self._keyField];
							var selfTxt = this;
							self._editInline_ajax(postData, function(success) {
								if(success) {
									$txt.html($(selfTxt).val());
									record[fieldName] = $(selfTxt).val();
								}
								else {
									$txt.html(fieldValue);
									record[fieldName] = fieldValue;
								}
								$(selfTxt).css('background','none');
								self._showUpdateAnimationForRow($txt.closest("tr"));
								});
						});
						$inputhtml.focus();
					}
				});	
										
				if(field.edit){	
					return $txt;	
				}else{
					return defaultval;
				}

		},
		_editInline_ajax:function(postData, onFinished){
            var self = this;
			var res = true;
			
			if (!self.options.url && $.isFunction(self.options.actions.updateAction)){
				self.options = $.extend({
					record: postData,
					clientOnly: false,
					animationsEnabled: self.options.animationsEnabled,
					success: function () { },
					error: function () { }
				}, self.options);

				self.options.record = postData;
				if (!self.options.record) {
					self._logWarn('options parameter in updateRecord method must contain a record property.');
					return;
				}

				var key = self._getKeyValueOfRecord(self.options.record);
				if (key == undefined || key == null) {
					self._logWarn('options parameter in updateRecord method must contain a record that contains the key field property.');
					return;
				}

				var $updatingRow = self.getRowByKey(key);
				if ($updatingRow == null) {
					self._logWarn('Can not found any row by key "' + key + '" on the table. Updating row must be visible on the table.');
					return;
				}

				if (self.options.clientOnly) {
					$.extend($updatingRow.data('record'), self.options.record);
					self._updateRowTexts($updatingRow);
					self._onRecordUpdated($updatingRow, null);
					if (self.options.animationsEnabled) {
						self._showUpdateAnimationForRow($updatingRow);
					}

					self.options.success();
					return;
				}

				var completeEdit = function (data) {
					if (data.Result != 'OK') {
						self._showError(data.Message);
						self.options.error(data);
						return;
					}

					$.extend($updatingRow.data('record'), self.options.record);
					self._updateRecordValuesFromServerResponse($updatingRow.data('record'), data);

					self._updateRowTexts($updatingRow);
					self._onRecordUpdated($updatingRow, data);
					if (self.options.animationsEnabled) {
						self._showUpdateAnimationForRow($updatingRow);
					}

					self.options.success(data);
				};

				//Execute the function
                var funcResult = self.options.actions.updateAction($.param(self.options.record));

                //Check if result is a jQuery Deferred object
                if (self._isDeferredObject(funcResult)) {
                    //Wait promise
                    funcResult.done(function (data) {
                        completeEdit(data);
                    }).fail(function () {
                        self._showError(self.options.messages.serverCommunicationError);
                        self.options.error();
                    });
                } else { //assume it returned the creation result
                    completeEdit(funcResult);
                }
            }
			else {
				this._ajax({
					url: (self.options.actions.updateAction),
					data: postData,
					success: function (data) {
						if (data.Result != 'OK') {
							self._showError(data.Message);
							if(onFinished != undefined) {
								onFinished(false);
							}
							res = false;
							return false;
						}
						if(onFinished != undefined) {
							onFinished(true);
						}
						self._trigger("recordUpdated", data, data);
					},
					error: function () {
						//self._trigger("recordUpdated", data, data);
						if (error) {
							error(self.options.messages.serverCommunicationError);
						}
						res = false;
						return false;
					}
				});			
				return res;
			}
		}
	});
	
})(jQuery);