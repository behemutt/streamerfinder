
$( document ).ready(function() {
 // Initialize form validation on the registration form.
    // It has the name attribute "registration"
    $("form[name='signup']").validate({
      // Specify validation rules
      rules: {
        // The key name on the left side is the name attribute
        // of an input field. Validation rules are defined
        // on the right side
        username: {
            required: true,
            minlength: 5
          },
        email: {
          required: true,
          // Specify that email should be validated
          // by the built-in "email" rule
          email: true
        },
        password: {
          required: true,
          minlength: 8
        },
        password_control: {
            equalTo: "#password"
          }
      },
      // Specify validation error messages
      messages: {
        username: {
            required: "Please provide a username",
            minlength: "Your password must be at least 5 characters long"
          },
        password: {
          required: "Please provide a password",
          minlength: "Your password must be at least 8 characters long"
        },
        password_control: {
            required: "Please confirm the password",
            equalTo: "the passwords are not equal"
          },
        email: "Please enter a valid email address"
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
        form.submit();
        insertUser();

      }
    });
  
     
})
    
  function insertUser() {
    var user = $("#username").val();
    var pass = $("#password").val();
    var email = $("#email").val();
    

    
  
    if (user != "" && pass != "") {
      $("body").addClass("loading");
      $.ajax
        ({
          type: 'post',
          url: '/insert_user',
          data: {
            username: user,
            password: pass,
            email: email
          },
          success: function (response) {
           
            var responseJson = JSON.parse(response);
            
            if (responseJson['status'] == 200) {

              var resultArea = $("#resultDiv");
              $("body").removeClass("loading");
              var messageFeedback = responseJson.data.message;
              console.log(messageFeedback);
              resultArea.append('<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a>'+ messageFeedback +'</div>"');
              window.location.href = URL + "/";
            }
            else {
              alert("Sign in");
              $("body").removeClass("loading");
            }
          },
          error: function (a, b, c) {
           
            var responseJson = JSON.parse(a.responseText);
            alert(responseJson['message']);
            $("body").removeClass("loading");
          }
        });
        
    }
    else {
      alert("Please Fill All The Details");
    }
  
    return;
  }
  
  $(document).bind({
    ajaxStart: function() {$body.addClass("loading");},
    ajaxStop: function() {$body.removeClass("loading");}
    });
    