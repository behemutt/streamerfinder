var YOUTUBE_BASEURL = "https://www.youtube.com/channel/";
var similarGamesCount;
var addedSimilarGames = [];
var resultJson;
var gamesData = null;
var csvLinkGoogle;
var currentChannelsData = '';
var lastVideoAge = 30;
var similarGames = '';

function initEvaluateChannelsScreen() {
	similarGamesCount = 0;
	initFileDialogUploadChannelsData();
}

function addSimilarGame() {
	var similarGamesForm = $("#contentDiv").find("#similarGamesForm");
	var similarGamesField = similarGamesForm.find("#similarGamesField");
	var games = similarGamesField.val().split('|');

	if(games.length > 0) {
		for(var i = 0; i < games.length; ++i) {
			addSimilarGameString(games[i]);
		}
	}
}

function addSimilarGameString(game) {
	var similarGamesForm = $("#contentDiv").find("#similarGamesForm");
	var similarGamesField = similarGamesForm.find("#similarGamesField");
	var similarGamesTable = similarGamesForm.find("#similarGamesTable");

	if(addedSimilarGames.indexOf(game.toLowerCase()) < 0 && game.length > 0) {
		addedSimilarGames.push(game.toLowerCase());
		similarGamesTable.append("<li><label id=\"game" + similarGamesCount + "\">" + game + "</label> " + 
				"<button class=\"btn btn-outline-secundary btn-sm\" id=\"similarGamesButton" + similarGamesCount + "\" type=\"button\">X</button></li>");

		similarGamesField.val('');

		$('#similarGamesButton' + similarGamesCount).click(function() {
				var elementToRemoveIndex = addedSimilarGames.indexOf($(this).closest("li").find("label").html().toLowerCase());
				if(elementToRemoveIndex >= 0)
				{
				addedSimilarGames.splice(elementToRemoveIndex, 1);
				}

				$(this).closest("li").remove();
				});
		similarGamesCount++;
	}
}

var channelsFound;

function onClickFormButton()
{
	var btn = $(document.activeElement);
	channelsFound = 0;
	if(btn[0].value == "Evaluate") {
		beginSearch(true);
	}
}

function beginSearch(first)
{
	var btn = $(document.activeElement);

	//if(btn.context.value == "Search")
	{
		//TODO: validate data
		var similarGamesForm = $("#contentDiv").find("#similarGamesForm");

		lastVideoAge = similarGamesForm.find("#lastVideoAge").val();
		similarGames = '';

		//validating input
		if(lastVideoAge == "" || addedSimilarGames.length == 0 || currentChannelsData == '') {
			$("#contentDiv").find("#errorFeedback").removeClass("hidden");
			return;
		}
		else {
			$("#contentDiv").find("#errorFeedback").addClass("hidden");
		}

		for(var i = 0; i < addedSimilarGames.length; ++i) {
			similarGames += '/' + addedSimilarGames[i];
		}
		$body = $("body");
		$body.addClass("loading");
		//$("#contentDiv").find("#resultDiv").find("#downloadCSVArea textarea").remove();        
		//$("#contentDiv").find("#resultDiv").find("#downloadCSVArea button").remove();        
		$.ajax({
			type: 'POST',
			url: '/youtube/evaluator/' + lastVideoAge + '/' + similarGames,
			dataType: 'text',
			data: {channelsData: currentChannelsData},
			success: function (result) {
				var resultArea = $("#contentDiv").find("#resultDiv").find("#resultTable");
				var messageJson;
				console.log(result);
				resultJson = JSON.parse(result);
				messageJson = JSON.parse(resultJson.message);
				console.log(resultJson);
				
				if(!resultJson.hasOwnProperty('data')) {
					$body.removeClass("loading");
					console.log("No data was found.");
					return;
				}

				resultArea.append('<tr><th class=\"text-right\">#</th><th>Channel Link</th><th>Channel Name</th><th>Subscriber Count</th>' +
					'<th>Active</th><th>Refs to Sim Games</th>' +
					'<th>Views</th><th>Views Avg</th><th>Views Err</th></tr>' +
					'<th>Comments</th><th>Comments Avg</th><th>Comments Err</th></tr>' +
					'<th>Likes</th><th>Likes Avg</th><th>Likes Err</th></tr>' +
					'<th>Dislikes</th><th>Dislikes Avg</th><th>Dislikes Err</th></tr>');
				gamesData = resultJson['data'];

				var csvLinkGoogle ="data:application/csv;charset=utf-8,";
				csvDataGoogle = "Channel Link\tChannel Name\tSubscriber Count\tActive\tRefs to Sim Games\t" +
					"Views\tViews Avg\tViews Err\t" +
					"Comments\tComments Avg\tComments Err\t" +
					"Likes\tLikes Avg\tLikes Err\t" +
					"Dislikes\tDislikes Avg\tDislikess Err\t" +
					"\r\n";

				for(var i = 0; i < gamesData.length; ++i) {
					var link = YOUTUBE_BASEURL + gamesData[i].channelId;
					resultArea.append("<tr><td class=\"text-right\">"+(i+1)+"</td><td><a target = \"_blank\" href=" +  link + ">" + link + "</a></td><td>" + gamesData[i].channelTitle + "</td><td>" + gamesData[i].subscriberCount + "</td>" +
					"<td>" + gamesData[i].active + "</td><td>" + gamesData[i].refsToSimilarGames + "</td>" +
					"<td>" + gamesData[i].views + "</td><td>" + gamesData[i].viewsAvg + "</td><td>" + gamesData[i].viewsError + "</td>" +
					"<td>" + gamesData[i].comments + "</td><td>" + gamesData[i].commentsAvg + "</td><td>" + gamesData[i].commentsError + "</td>" +
					"<td>" + gamesData[i].likes + "</td><td>" + gamesData[i].likesAvg + "</td><td>" + gamesData[i].likesError + "</td>" +
					"<td>" + gamesData[i].dislikes + "</td><td>" + gamesData[i].dislikesAvg + "</td><td>" + gamesData[i].dislikesError + "</td>" +
					"</tr>");
				}

				for(var i = 0; i < gamesData.length; ++i) {
						var link = YOUTUBE_BASEURL + gamesData[i].channelId;
						csvDataGoogle += link + "\t" + gamesData[i].channelTitle + "\t" + gamesData[i].subscriberCount + "\t" + 
						gamesData[i].active + "\t" + gamesData[i].refsToSimilarGames + "\t" +
						gamesData[i].views + "\t" + gamesData[i].viewsAvg + "\t" + gamesData[i].viewsError + "\t" +
						gamesData[i].comments + "\t" + gamesData[i].commentsAvg + "\t" + gamesData[i].commentsError + "\t" +
						gamesData[i].likes + "\t" + gamesData[i].likesAvg + "\t" + gamesData[i].likesError + "\t" +
						gamesData[i].dislikes + "\t" + gamesData[i].dislikesAvg + "\t" + gamesData[i].dislikesError + "\r\n";
				}

				csvLinkGoogle += encodeURIComponent(csvDataGoogle);
				$body.removeClass("loading");

				if($("#contentDiv").find("#resultDiv").find("#downloadCSVArea textArea").length == 0) {
					$("#contentDiv").find("#resultDiv").find("#downloadCSVArea").append("<textarea id=\"tempTextArea\" class=\"hidden\"/>");
					$("#contentDiv").find("#resultDiv").find("#downloadCSVArea").append("<button class=\"btn btn-link\" onclick=\"downloadGeneralCSV()\">Download CSV</button>");
					$("#contentDiv").find("#resultDiv").find("#downloadCSVArea").append("<button data-clipboard-target=\"#tempTextArea\" id=\"copyToClipboardButton\" type=\"button\" class=\"btn btn-link\"\">Copy to clipboard</btn>");
				
					$("#contentDiv").find("#resultDiv").find("#downloadCSVArea").find("#copyToClipboardButton").click(function() {
						var textArea = $("#contentDiv").find("#resultDiv").find("#downloadCSVArea").find("#tempTextArea");
						textArea.removeClass("hidden");
						textArea.select();
						document.execCommand('copy');
						textArea.addClass("hidden");
						$("#contentDiv").find("#copyToClipboardFeedback").removeClass("hidden").delay(3000).queue(function(){
							$("#contentDiv").find("#copyToClipboardFeedback").addClass("hidden");                        
						});
					});
				}
				else {
					$("#contentDiv").find("#resultDiv").find("#downloadCSVArea").find("#tempTextArea").val(csvDataGoogle);
				}
		},
		error : function (a,b,c) {
				console.log(a);
				console.log(b);
				console.log(c);
				$body.removeClass("loading");
			}
		});
	}
	//else
	//{
		// if(!confirm("All your current results will be excluded. Are you sure you want to continue?")) {
		//     return;
		// }
	//	console.log("Clearing");
	//}
}

function downloadGeneralCSV() {
	var textFileAsBlob = new Blob([csvDataGoogle], {type:'text/plain'});
	var fileNameToSaveAs = 'ChannelsEvaluation.csv';

	var downloadLink = document.createElement("a");
	downloadLink.download = fileNameToSaveAs;
	downloadLink.innerHTML = "Download File";
	if (window.URL != null)
	{
		// Chrome allows the link to be clicked
		// without actually adding it to the DOM.
		downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
	}
	else
	{
		// Firefox requires the link to be added to the DOM
		// before it can be clicked.
		downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
		downloadLink.onclick = destroyClickedElement;
		downloadLink.style.display = "none";
		document.body.appendChild(downloadLink);
	}

	downloadLink.click();
}

function clearResults() {
	var similarGamesForm = $("#contentDiv").find("#similarGamesForm");
	similarGamesForm.find("#keywords").val('');
	similarGamesForm.find("#videoAge").val('');
	similarGamesForm.find("#minSubscribers").val('');
	similarGamesForm.find("#maxSubscribers").val('');
	similarGamesForm.find("#similarGamesField").val('');
	//similarGamesForm.find("#preferedLanguage").prop('selectedIndex', 0);

	addedSimilarGames = [];
	similarGamesForm.find("#similarGamesTable li").remove();

	//remove previous rows
	$("#contentDiv").find("#resultDiv").find("#resultTable tbody").remove();
	$("#contentDiv").find("#resultDiv").find("#resultTable th").remove();
	$("#contentDiv").find("#resultDiv").find("#resultTable tr").remove();
	$("#contentDiv").find("#resultDiv").find("#downloadCSVArea textarea").remove();        
	$("#contentDiv").find("#resultDiv").find("#downloadCSVArea button").remove();   

	// $.ajax({
	// 	type: 'GET',
	// 	url: 'clear',
	// 	dataType: 'text',
	// 	success: function (result) {
	// 		console.log(result);
	// 	},
	// 	error : function (a,b,c) {
	// 		console.log(a);
	// 		console.log(b);
	// 		console.log(c);
	// 	}
	// });

	delete resultJson;
	delete gamesData;
	gamesData = null;
}

function onClickUploadChannelsData() {
    $("#uploadChannelsData").trigger("click");
}

function initFileDialogUploadChannelsData() {
    $("#uploadChannelsData").change(postYoutubeChannelsDataFile);
}

function postYoutubeChannelsDataFile(evt) {
    var f = evt.target.files[0];
    if(f) {
        var r = new FileReader();
        r.onload = function(e) {
			var contents = e.target.result;
			// var parsedContents = $.tsv.parseRows(contents);
			currentChannelsData = contents;
		};
		$("#uploadChannelsDataButton").text(f.name);
        r.readAsText(f);
    }
    else {
        alert("Error loading file");
    }
}

$(document).ready(function() {
	initEvaluateChannelsScreen();
        initSidebar();

		$("#contentDiv").find("#similarGamesForm").find("#similarGamesField").keypress(function(event) {
				if(event.which == 13) {
				addSimilarGame();
				}
				});
		});

$(document).bind({
ajaxStart: function() {$body.addClass("loading");},
ajaxStop: function() {$body.removeClass("loading");}
});
