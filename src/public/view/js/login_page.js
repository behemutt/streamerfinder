function loginValidate() {
  var user = $("#username").val();
  var pass = $("#password").val();

  console.log(user);
  console.log(pass);

  if (user != "" && pass != "") {
    $("body").addClass("loading");
    $.ajax
      ({
        type: 'post',
        url: 'verify_login',
        data: {
          username: user,
          password: pass
        },
        success: function (response) {
          console.log(response);
          var responseJson = JSON.parse(response);
          console.log(responseJson);
          if (responseJson['status'] == 200) {
            $("body").removeClass("loading");
            //$.session.set('', ''); //This jquery notations is valid just in case we had a plugin called jquery.session
            window.location.href = "/dashboard";
          }
          else {
            alert("Wrong Details");
            $("body").removeClass("loading");
          }
        },
        error: function (a, b, c) {
          console.log(a.responseText);
          var responseJson = JSON.parse(a.responseText);
          alert(responseJson['message']);
          $("body").removeClass("loading");
        }
      });
  }
  else {
    alert("Please Fill All The Details");
  }

  return false;
}