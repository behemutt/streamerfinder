var YOUTUBE_VIDEO_BASE_URL = "https://www.youtube.com/watch?v=";
var YOUTUBE_CHANNEL_BASE_URL = "https://www.youtube.com/channel/";
var fromDateChart = '0000-00-00';
var toDateChart = '9999-12-31';
var fromDateEvents = '0000-00-00';
var toDateEvents = '9999-12-31';
var fromDateVideoReview = '0000-00-00';
var toDateVideoReview = '9999-12-31';
var fromDateChannelReview = '0000-00-00';
var toDateChannelReview = '9999-12-31';
var fromDateWishlists = '0000-00-00';
var toDateWishlists = '9999-12-31';
var showReviewed = false;
var showReviewedChannels = false;

var currentProjectId;

//Dashboard - Start  --------------------------
function initDashboardScreen() {
    $body = $("body");
    $body.addClass("loading");

    if(localStorage.getItem("currentProjectId") === null) {
        currentProjectId = 1;
    }
    else {
        currentProjectId = localStorage['currentProjectId'];
    }

    initProjectsDropdown();
    initDateRangeChart();
    initChart();
    initFileDialogTwitchViews();
}

function onClickUpdateProject() {
    $projectsDropdown = $("#contentDiv").find("#main").find("#selectProject").find("#inputGroupSelect");
    currentProjectId = $projectsDropdown.val();
    localStorage['currentProjectId'] = currentProjectId;
    location.reload();
}

function onChangeProjectDropdown() {
    $projectsDropdown = $("#contentDiv").find("#main").find("#selectProject").find("#inputGroupSelect");
    var projId = $projectsDropdown.val();

    if(projId == -1)
    {
        //DO nothing
    }
    else if(projId != currentProjectId)
    {
        currentProjectId = $projectsDropdown.val();
        localStorage['currentProjectId'] = currentProjectId;
        location.reload();
    }
}

function onClickImportTwitchData() {
    $("#twitchViewsData").trigger("click");
}

function onClickExportReport() {
	$("body").addClass("loading");
    
    $.ajax({
        url: '/dashboard/graph_data/list',
        type: 'POST',
        dataType: 'json',
        data: {projectId: currentProjectId, toDate: toDateChart, fromDate: fromDateChart },
        success: function (data) {
            $("body").removeClass("loading");
            console.log(data);

            var wishlistsData = data.data.wishlistsData;
            var sellingData = data.data.sellingData;
            var eventsData = data.data.eventsData;
            var analyticsData = data.data.analyticsData;
            var viewsData = data.data.viewsData;
            var videosData = data.data.videosCountData;
            var twitchViews = data.data.twitchViews;
            var maxYAxis2 = 0;

			var csvLinkGoogle ="data:application/csv;charset=utf-8,";
			var csvDataGoogle = "Date\tSteam Units Sold\tSteam Wishlists\tSteam page visits\tYoutube Views\tNumber Of Youtube Videos\r\n";

			csvLinkGoogle = $("#tempTextArea").val();

			for(var i = 0; i < viewsData.length; ++i) {
				csvDataGoogle +=
					(i >= viewsData.length ? 0 : viewsData[i].date) + "\t" +
					(i >= sellingData.length ? 0 : sellingData[i].balance) + "\t" +
					(i >= wishlistsData.length ? 0 : wishlistsData[i].balance) + "\t" +
					(i >= analyticsData.sessions.length ? 0 : analyticsData.sessions[i]) + "\t" +
					(i >= viewsData.length ? 0 : viewsData[i].views) + "\t" +
					(i >= videosData.length ? 0 : videosData[i].videos) +"\r\n";
				console.log(csvDataGoogle);
			}

			csvLinkGoogle += encodeURIComponent(csvDataGoogle);

			if($("#downloadCSVArea textArea").length == 0) {
				$("#downloadCSVArea").append("<textarea id=\"tempTextArea\" class=\"hidden\"/>");
				
				var textFileAsBlob = new Blob([csvDataGoogle], {type:'text/plain'});
				var fileNameToSaveAs = 'StreamerFinderReport-' + fromDateChart + '_' + toDateChart + '.csv';

				var downloadLink = document.createElement("a");
				downloadLink.download = fileNameToSaveAs;
				downloadLink.innerHTML = "Download File";
				if (window.URL != null)
				{
					// Chrome allows the link to be clicked
					// without actually adding it to the DOM.
					downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
				}
				else
				{
					// Firefox requires the link to be added to the DOM
					// before it can be clicked.
					downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
					downloadLink.onclick = destroyClickedElement;
					downloadLink.style.display = "none";
					document.body.appendChild(downloadLink);
				}
				downloadLink.click();
			}
			else {
				$("#downloadCSVArea").find("#tempTextArea").val(csvDataGoogle);
			}
        },
        error: function (a,b,c) {
            $("body").removeClass("loading");
            console.log(a);
            console.log(b);
            console.log(c);
        }
    });
}

function initFileDialogTwitchViews() {
    $("#twitchViewsData").change(postTwitchViewsFile);
}

function postTwitchViewsFile(evt) {
    var f= evt.target.files[0];
    console.log(f);
    if(f) {
        var r = new FileReader();
        r.onload =function(e) {
            var contents = e.target.result;
            var parsedContents = JSON.parse(contents);

            $("body").addClass("loading");

            $.ajax({
                url: 'dashboard/twitch_views_stats/insertMultiple',
                type: 'POST',
                dataType: 'html',
                data: { projectId: currentProjectId, data: parsedContents },
                success: function (data) {
                    console.log(data);
                    // updateSellingDataTable();
                    udpateDashboardChart();
                    $("body").removeClass("loading");
                },
                error: function (a,b,c) {
                    $("body").removeClass("loading");
                    console.log(a);
                    console.log(b);
                    console.log(c);
                }
            });
        };
        r.readAsText(f);
    }
    else {
        alert("Error loading file");
    }
}

function initProjectsDropdown() {
    $projectsDropdown = $("#contentDiv").find("#main").find("#selectProject").find("#inputGroupSelect");
    //$projectsDropdown.append("<option selected value ='" + -1 + "'>Select a project...</option>");
	
    $.ajax({
			type: 'GET',
			url: 'dashboard/projects',
			dataType: 'text',
			success: function (result) {
                var resultJson = JSON.parse(result);
                var projectsData = JSON.parse(resultJson.data);

                for(var i = 0; i < projectsData.length; ++i) {
                    console.log(projectsData[i])
                    $projectsDropdown.append("<option value='" + projectsData[i]['project_id'] + "'>" + projectsData[i]['project_name'] + "</option>");
                }

                $projectsDropdown.val(localStorage['currentProjectId']);
            },
            error : function (a,b,c) {
				console.log(a);
				console.log(b);
				console.log(c);
				$body.removeClass("loading");
			}
        });
}

function initDateRangeChart() {
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrangedashboard span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrangedashboard').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		   'Last 6 Months': [moment().subtract(6, 'month'), moment()],
           'Last Year': [moment().subtract(12, 'month'), moment()],
           'All Time': [moment("20160101"), moment()],
        }
    }, cb);
	
	$('#reportrangedashboard').on('apply.daterangepicker', function(ev, picker) {
		fromDateChart = picker.startDate.format('YYYY-MM-DD');
		toDateChart = picker.endDate.format('YYYY-MM-DD');
        udpateDashboardChart();
	});

	fromDateChart = start.format('YYYY-MM-DD');
	toDateChart = end.format('YYYY-MM-DD');
    cb(start, end);
}

function udpateDashboardChart() {
    initChart();
}

function initChart() {

    $("body").addClass("loading");
    
    $.ajax({
        url: '/dashboard/graph_data/list',
        type: 'POST',
        dataType: 'json',
        data: {projectId: currentProjectId, toDate: toDateChart, fromDate: fromDateChart },
        success: function (data) {
            $("body").removeClass("loading");
            console.log(data);
            console.log(currentProjectId);

            var wishlistsData = data.data.wishlistsData;
            var sellingData = data.data.sellingData;
            var eventsData = data.data.eventsData;
            var analyticsData = data.data.analyticsData;
            var viewsData = data.data.viewsData;
            var videosData = data.data.videosCountData;
            var twitchViews = data.data.twitchViews;
            var maxYAxis2 = 0;

            var sellingLine = [];
            for(var i = 0; i < sellingData.length; ++i) {
                var d = [sellingData[i].date, sellingData[i].balance];
                sellingLine.push(d);

                if(parseInt(sellingData[i].balance) > maxYAxis2) {
                    maxYAxis2 = parseInt(sellingData[i].balance);
                }
            }

            var wishlistsLine = [];
            for(var i = 0; i < wishlistsData.length; ++i) {
                var d = [wishlistsData[i].date, wishlistsData[i].balance];
                wishlistsLine.push(d);

                if(parseInt(wishlistsData[i].balance) > maxYAxis2) {
                    maxYAxis2 = parseInt(wishlistsData[i].balance);
                }
            }

            var eventsLine = [];
            for(var i = 0; i < eventsData.length; ++i) {
                var d = [eventsData[i].date, 1, eventsData[i].name];
                eventsLine.push(d);
            }

            var analyticsLine = [];
            for(var i = 0; i < analyticsData.dates.length; ++i) {
                var d = [analyticsData.dates[i], analyticsData.sessions[i]];
                analyticsLine.push(d);

                if(parseInt(analyticsData.sessions[i]) > maxYAxis2) {
                    maxYAxis2 = parseInt(analyticsData.sessions[i]);
                }
            }

            var viewsLine = [];
            for(var i = 0; i < viewsData.length; ++i) {
                var d = [viewsData[i].date, viewsData[i].views];
                viewsLine.push(d);
            }

            var videosLine = [];
            for(var i = 0; i < videosData.length; ++i) {
                var d = [videosData[i].date, videosData[i].videos];
                videosLine.push(d);

                if(parseInt(videosData[i].videos) > maxYAxis2) {
                    maxYAxis2 = parseInt(videosData[i].videos);
                }
            }

            var twitchViewsLine = [];
            for(var i = 0; i < twitchViews.length; ++i) {
                var d = [twitchViews[i].twitch_stats_date, twitchViews[i].twitch_stats_views];
                twitchViewsLine.push(d);
            }
            //createChart(maxYAxis2, [viewsLine, twitchViewsLine, videosLine, eventsLine, wishlistsLine, sellingLine, analyticsLine]);
            createSteamSessionsVsYoutube([viewsLine, analyticsLine]);
        },
        error: function (a,b,c) {
            $("body").removeClass("loading");
            console.log(a);
            console.log(b);
            console.log(c);
        }
    });
}

function createSteamSessionsVsYoutube(lines) {
    $("#chartdiv").empty();

    var maxViews = 0;
    var maxSteamSessions = 0;

    var i;
    var j;
    for(i = 0; i < lines[0].length; ++i) {
        var data = lines[0][i];
        if(parseInt(data[1]) > maxViews) {
            maxViews = parseInt(data[1]);
        }
    }

    for(j = 0; j < lines[1].length; ++j) {
        var data = lines[1][j];
        if(parseInt(data[1]) > maxSteamSessions) {
            maxSteamSessions = parseInt(data[1]);
        }
    }

    maxViews *= 1.1;
    maxSteamSessions *= 1.1;

    var plot1 = $.jqplot('chartdiv', lines, {
        title:'Steam Sessions vs Youtube Views',
        height: 500,
        stackSeries: true,
        axes:{
            xaxis:{
                renderer:$.jqplot.DateAxisRenderer,
                tickOptions:{
                    formatString:'%b,%#d&nbsp;%y'
                } 
            },
            yaxis:{
                label: "Views",
                min:0,
                max: maxViews,
                tickOptions: {
                    formatString:'%d'
                } 
                // renderer:$.jqplot.LogAxisRenderer,
            },
            y2axis: {
                label: "Steam",
                //renderer: $.jqplot.LogAxisRenderer,
                min: 0,
                max: maxSteamSessions,
            },         
        },
        highlighter: {
            show: true,
            sizeAdjust: 7.5,
            tooltipAxes: 'xy',
        },
        cursor: {
          show: true,
          zoom: true,
          showTooltip: false
        },
        legend:{
            renderer: $.jqplot.EnhancedLegendRenderer,
            show:true,
            location: 'e', 
            showSwatches: true,
            placement: 'outsideGrid',
            rendererOptions:{
                seriesToggleReplot: { resetAxes: true }
            },
        },
        series: [
            {
                color:'rgba(255, 0, 0, 1)',
                label: "Views Youtube",
                showLine: true,
                markerOptions: {size: 4},
                highlighter: {
                    yvalues: 2,
                    formatString: '<table class="jqplot-highlighter"><tr><td class="text-left">date:</td><td>%s</td></tr><tr><td class="text-left">views:</td><td>%s</td></tr></table>'
                }
            },
            {
                disableStack: true,
                color:'rgba(30, 200, 30, 1)',
                label: "Steam page sessions",
                showLine: true,
                yaxis:'y2axis',
                markerOptions: {size: 4},
                highlighter: {
                    yvalues: 2,
                    formatString: '<table class="jqplot-highlighter"><tr><td class="text-left">date:</td><td>%s</td></tr><tr><td class="text-left">sessions:</td><td>%s</td></tr></table>'
                }
            }
        ]
    });

    $(window).resize(function() {
        plot1.replot( { resetAxes: true });
    });

    $('#resetChartZoomButton').click(function() { plot1.resetZoom() });
    $("#resizableChartDiv").resizable({delay:20});
    $('#resizableChartDiv').bind('resize', function(event, ui) {
        $('#chartdiv').attr('style', 'height: ' + $('#chartdiv').parent().height() + 'px');
        plot1.replot( { resetAxes: true } );
    });
}

function createChart(maxYAxis2, lines) {
    $("#chartdiv").empty();

    var plot1 = $.jqplot('chartdiv', lines, {
        title:'Project History',
        height: 500,
        stackSeries: true,
        axes:{
            xaxis:{
                renderer:$.jqplot.DateAxisRenderer,
                tickOptions:{
                    formatString:'%b,%#d&nbsp;%y'
                } 
            },
            yaxis:{
                label: "Views",
                min:0,
                tickOptions:{
                    formatString:'%d'
                } 
                // renderer:$.jqplot.LogAxisRenderer,
            },
            y2axis: {
                label: "Steam",
                renderer: $.jqplot.LogAxisRenderer,
                min: 0.5,
                max: maxYAxis2 * 1.1
            },         
        },
        highlighter: {
            show: true,
            sizeAdjust: 7.5,
            tooltipAxes: 'xy',
        },
        cursor: {
          show: true,
          zoom: true,
          showTooltip: false
        },
        legend:{
            renderer: $.jqplot.EnhancedLegendRenderer,
            show:true,
            location: 'e', 
            showSwatches: true,
            placement: 'outsideGrid',
            rendererOptions:{
                seriesToggleReplot: { resetAxes: true }
            },
        },
        series: [
            {
                color:'rgba(255, 0, 0, 1)',
                label: "Views Youtube",
                showLine: true,
                markerOptions: {size: 4},
                highlighter: {
                    yvalues: 2,
                    formatString: '<table class="jqplot-highlighter"><tr><td class="text-left">date:</td><td>%s</td></tr><tr><td class="text-left">views:</td><td>%s</td></tr></table>'
                }
            },
            {
                color:'rgba(128, 128, 255, 1)',
                label: "Views Twitch",
                showLine: true,
                markerOptions: {size: 4},
                highlighter: {
                    yvalues: 2,
                    formatString: '<table class="jqplot-highlighter"><tr><td class="text-left">date:</td><td>%s</td></tr><tr><td class="text-left">views:</td><td>%s</td></tr></table>'
                }
            },
            {
                disableStack: true,
                color:'rgba(231, 0, 135, 1)',
                label: "YT Videos count",
                //showLine: true,
                yaxis:'y2axis',
                markerOptions: {size: 1},
                renderer: $.jqplot.BarRenderer,
                rendererOptions: {
                    barWidth: 10
                },
                highlighter: {
                    yvalues: 2,
                    formatString: '<table class="jqplot-highlighter"><tr><td class="text-left">date:</td><td>%s</td></tr><tr><td class="text-left">videos:</td><td>%s</td></tr></table>'
                }
            },
            {
                disableStack: true,
                color:'rgba(13, 113, 206, 1)',
                label: "Events",
                showLine:false,
                yaxis:'y2axis',
                markerOptions: {
                    size: 10,
                    style: 'diamond',
                },
                highlighter: {
                    yvalues: 3,
                    formatString: '<table class="jqplot-highlighter"><tr><td class="text-left">date:</td><td>%s</td></tr><tr><tr style="visibility: collapse"><td>%s</td></tr><tr><td class="text-left">name:</td><td>%s</td></tr></table>'
                }
            },
            {
                disableStack: true,
                color:'rgba(216, 159, 60, 1)',
                label: "Wishlists",
                yaxis:'y2axis',
                showLine: true,
                markerOptions: {size: 4},
                highlighter: {
                    yvalues: 2,
                    formatString: '<table class="jqplot-highlighter"><tr><td class="text-left">date:</td><td>%s</td></tr><tr><td class="text-left">balance:</td><td>%s</td></tr></table>'
                }
            },
            {
                disableStack: true,
                color:'rgba(116, 159, 60, 1)',
                label: "Sold Units",
                yaxis:'y2axis',
                showLine: true,
                markerOptions: {size: 4},
                highlighter: {
                    yvalues: 2,
                    formatString: '<table class="jqplot-highlighter"><tr><td class="text-left">date:</td><td>%s</td></tr><tr><td class="text-left">balance:</td><td>%s</td></tr></table>'
                }
            },
            {
                disableStack: true,
                color:'rgba(30, 200, 30, 1)',
                label: "Steam page sessions",
                showLine: true,
                yaxis:'y2axis',
                markerOptions: {size: 4},
                highlighter: {
                    yvalues: 2,
                    formatString: '<table class="jqplot-highlighter"><tr><td class="text-left">date:</td><td>%s</td></tr><tr><td class="text-left">sessions:</td><td>%s</td></tr></table>'
                }
            }
        ]
    });

    $(window).resize(function() {
        plot1.replot( { resetAxes: true });
    });

    $('#resetChartZoomButton').click(function() { plot1.resetZoom() });
    $("#resizableChartDiv").resizable({delay:20});
    $('#resizableChartDiv').bind('resize', function(event, ui) {
        $('#chartdiv').attr('style', 'height: ' + $('#chartdiv').parent().height() + 'px');
        plot1.replot( { resetAxes: true } );
    });

}
//Dashboard - End    --------------------------

//Review videos - Start  --------------------------
function initReviewVideosScreen() {
	initDateRangeVideosReview();
    initReviewVideosToggle();
    createReviewVideosGrid();
}

function initReviewVideosToggle() {
	$("#reviewedCheckbox").change(function() {
		showReviewed = this.checked;
		updateReviewVideosTable();
	});
}

function initDateRangeVideosReview() {
    var start = moment("20160101");
    var end = moment();

    function cb(start, end) {
        $('#reportrangereviewvideos span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrangereviewvideos').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'Last 6 Months': [moment().subtract(6, 'month'), moment()],
            'Last Year': [moment().subtract(12, 'month'), moment()],
            'All Time': [moment("20160101"), moment()],
        }
    }, cb);
	
	$('#reportrangereviewvideos').on('apply.daterangepicker', function(ev, picker) {
		fromDateVideoReview = picker.startDate.format('YYYY-MM-DD');
		toDateVideoReview = picker.endDate.format('YYYY-MM-DD');
		updateReviewVideosTable();
	});

	fromDateVideoReview = start.format('YYYY-MM-DD');
	toDateVideoReview = end.format('YYYY-MM-DD');
    cb(start, end);

}

function updateReviewVideosTable() {
	$("#video-data-grid").jtable("load");
}

function setPageReviewed() {
    var videoIds = [];
    $('#video-data-grid tr:visible').each(function(i, tr) {
        var key = $(this).attr('data-record-key');
        if(key !== undefined) {
            videoIds.push(key);
        }
    });

    $.ajax({
        url: '/dashboard/video_review/update_list',
        type: 'POST',
        dataType: 'json',
        data: {project_id: currentProjectId, video_ids: videoIds, reviewed: 1},
        success: function (data) {
            console.log(data);

            videoIds.forEach(function(v)
            {
                $('#video-data-grid').jtable('deleteRecord', {
					key: v,
					clientOnly: true,
					success: (function() {
						// $('#video-data-grid').jtable('_changePage', {pageNo: 2});
					}),
					error: (function() {
						// alert("record deletion error!");
					})
				}); 
            });
        },
        error: function (a,b,c) {
            console.log(a);
            console.log(b);
            console.log(c);
        }
    });
}

function togglePageValid() {
    var videoIds = [];
    var spanNewValues = [];
    $('#video-data-grid tr:visible').each(function(i, tr) {
        var key = $(this).attr('data-record-key');
        var span = $(this).find('td:eq(5)').find('span')[0];
        if(span !== undefined) {
            console.log(($(span).attr('val') == 1 ? 0 : 1));
            spanNewValues.push(($(span).attr('val') == 1 ? 0 : 1));
            videoIds.push(key);
        }
    });

    $.ajax({
        url: '/dashboard/video_review/update_list',
        type: 'POST',
        dataType: 'json',
        data: {project_id: currentProjectId, video_ids: videoIds, valid_values: spanNewValues},
        success: function (data) {
            $("#video-data-grid").jtable("load");
            console.log(data);
        },
        error: function (a,b,c) {
            console.log(a);
            console.log(b);
            console.log(c);
        }
    });
}

function updateVideosToReviewBadge(amount) {
    if(!$("#reviewedCheckbox")[0].checked) {
        if(amount > 0) {
            $('#reviewVideosCountBadge').show();
            $('#reviewVideosCountBadge').text(amount);
        }
        else{
            $('#reviewVideosCountBadge').hide();
        }
    }
    else {
        $('#reviewVideosCountBadge').hide();
    }
}

/*Cria a grid do CRUD usando Jtable*/
function createReviewVideosGrid() {
    $('#video-data-grid').jtable({
        title: 'Videos to review',
        useBootstrap: true,
        paging: true, //Enable paging
		sorting: true,
		defaultSorting: 'published_at ASC',
        editinline:{enable:true, img:"img/"},

        toolbar: {
            hoverAnimation: true, //Enable/disable small animation on mouse hover to a toolbar item.
            hoverAnimationDuration: 60, //Duration of the hover animation.
            hoverAnimationEasing: undefined, //Easing of the hover animation. Uses jQuery's default animation ('swing') if set to undefined.
            //Array of your custom toolbar items.
            items: [
                {
                    text: 'Toggle page valid',
                    click: function() {
                        togglePageValid();
                    }
                },
                {                          
                    text: 'Set page reviewed',
                    click: function () {
                        setPageReviewed();
                    }
                },
            ]
        },

        recordsLoaded: function(event, data) { //when tha table shows
            console.log(data);
			// $('td:nth-child(8),th:nth-child(8)').hide(); //hide the 5th column for the 2nd level child table
		},
		
		recordUpdated: function(event, data) {
            console.log(data);
            
			var record = JSON.parse(data.serverResponse.Records);
			
			if(record['removed']) {
				$('#video-data-grid').jtable('deleteRecord', {
					key: record['videoId'],
					clientOnly: true,
					success: (function() {
						// $('#video-data-grid').jtable('_changePage', {pageNo: 2});
					}),
					error: (function() {
						// alert("record deletion error!");
					})
				});
			}
		},
		
        actions: {
            listAction:  function (postData, jtParams) {
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: '/dashboard/video_review/list?projectId='+currentProjectId +'&reviewed=' + showReviewed + '&fromDate=' + fromDateVideoReview + '&toDate=' + toDateVideoReview + '&jtStartIndex='+  jtParams.jtStartIndex.toString()  +'&jtPageSize='+ jtParams.jtPageSize.toString() + '&jtSorting=' + encodeURI(jtParams.jtSorting),
                        type: 'POST',
                        dataType: 'json',
                        data: postData,
                        success: function (data) {
                            $dfd.resolve(data);
                            updateVideosToReviewBadge(data.TotalRecordCount);
                        },
                        error: function (a,b,c) {
                            console.log(a);
                            console.log(b);
                            console.log(c);
                            $dfd.reject();
                        }
                    });
                });
            },
			updateAction: function (postData) {
                newPostData = postData + '&project_id='+currentProjectId;
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: '/dashboard/video_review/update',
                        type: 'POST',
                        dataType: 'json',
                        data: newPostData,
                        success: function (data) {
                            // console.log(data);
                            $dfd.resolve(data);
                        },
                        error: function (a,b,c) {
                            // console.log(a);
                            // console.log(b);
                            // console.log(c);
                            $dfd.reject();
                        }
                    });
                });
            },
        },
        fields: {
            project_id: {
                title: 'proj id',
                width: '1%',
                type:'hidden',
                edit: false,
                create: false,
				sorting: false,
            },

            thumbnail: {
                title: 'Thumb',
                type: 'boolean',
                width: '15%',
                edit: false,
                create: false,
				sorting: false,
                display: function (data) {
                    return '<a target="_blank" href="' + YOUTUBE_VIDEO_BASE_URL + data.record.video_id + '"><img src=' + data.record.thumbnail + ' /></a>';
                }
            },

            video_id: {
                title: 'Link',
                width: '8%',
                key: true,
                list: true,
                edit: false,
                create: false,
				sorting: false,
                display: function (data) {
                    return '<a target="_blank" href="' + YOUTUBE_VIDEO_BASE_URL + data.record.video_id + '">' + YOUTUBE_VIDEO_BASE_URL + data.record.video_id + '</a>';
                }
            },

            video_title: {
                title: 'Title',
                width: '12%',
                create: false,
                edit: false,
				sorting: false,
            },

            video_desc: {
                title: 'Description',
                width: '40%',
				sorting: false,
                create: false,
                edit: false
            },

            published_at: {
                    title: 'Published at',
                    width: '19%',
                    type: 'date',
                    create: false,
                    edit: false
                },
				
            valid: {
                title: 'Valid',
                type: 'checkbox',
                width: '3%',
                values: { 'false': '0', 'true': '1' },
                defaultValue: true
            },
			
			reviewed: {
                title: 'Reviewed',
                type: 'checkbox',
                width: '3%',
                values: { 'false': '0', 'true': '1' },
                defaultValue: false,
            },
        }
    });
    $('#video-data-grid').jtable('option', 'pageSize', 10);

    $("#video-data-grid").jtable("load");
}
//Review videos - End    --------------------------

//Queries stuff - start  --------------------------
function initQueriesStuff() {
    setupShowQueryPopup();
    updateQueryButtonText();
    initAddQueryButton();
}

function setupShowQueryPopup() {
    $(document).on('shown.bs.modal', '#queryInputModal', function () {
        createQueriesGrid();
    });
}

function updateQueryButtonText() {
    var button = $('#currentQuery');
    var text = "Set a query here";

    $("body").addClass("loading");

    $.ajax({
        url: '/dashboard/query/last',
        type: 'POST',
        dataType: 'json',
        data: {project_id : currentProjectId},
        success: function (data) {
            console.log(data);
            text = (data.data[0].query_terms + ' | ' + data.data[0].initial_search_date);
            button.html(text);
            $("body").removeClass("loading");    
        },
        error: function (a,b,c) {
            console.log(a);
            console.log(b);
            console.log(c);
            button.html(text);
            $("body").removeClass("loading");    
        }
    });
}

function createQueriesGrid() {
    $('#queries-data-grid').jtable({
        title: 'Queries history',
        useBootstrap: true,
        paging: true, //Enable paging
		sorting: true,
		defaultSorting: 'query_date DESC',
        editinline:{enable:true, img:"img/"},
        selecting: true,
        multiselect:false,
        selectingCheckboxes:true,

        selectionChanged: function () {
            //Get all selected rows
            var $selectedRows = $('#queries-data-grid').jtable('selectedRows');
            if ($selectedRows.length > 0) {
                    //Show selected rows
                    $('#saveQuery').prop('disabled', false);
                    $('#saveQueryAndSearch').prop('disabled', false);;
                    $selectedRows.each(function () {
                    var record = $(this).data('record');
                });
            } else {
                $('#saveQuery').prop('disabled', true);;
                $('#saveQueryAndSearch').prop('disabled', true);;
            }
        },

        rowInserted: function (event, data) {
            // console.log(data.row);
            // if (data.record.Name.indexOf('Andrew') >= 0) {
            //     $('#queries-data-grid').jtable('selectRows', data.row);
            // }
        },

        recordsLoaded: function(event, data) { //when tha table shows
            setSelectedQuery(data);
            //Don't let the player delete the last query
            if(data.records.length == 1) {
                var deleteButton = $('#deleteQueryButton' + data.records[0].query_id);
                deleteButton.hide();
            }
		},
		
		recordUpdated: function(event, data) {
            if(data.Records !== undefined) {
                var record = JSON.parse(data.Records);
                if(record['removed']) {
                    $('#queries-data-grid').jtable('deleteRecord', {
                        key: record['query_id'],
                        clientOnly: true,
                        success: (function() {
                        }),
                        error: (function() {
                        })
                    });
                }
            }
		},
		
        actions: {
            listAction:  function (postData, jtParams) {
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: '/dashboard/query/list?project_id='+ currentProjectId + '&jtStartIndex='+  jtParams.jtStartIndex.toString()  +'&jtPageSize='+ jtParams.jtPageSize.toString() + '&jtSorting=' + encodeURI(jtParams.jtSorting),
                        type: 'POST',
                        dataType: 'json',
                        data: postData,
                        success: function (data) {
                            console.log(data);
                            $dfd.resolve(data);
                        },
                        error: function (a,b,c) {
                            console.log(a);
                            console.log(b);
                            console.log(c);
                            $dfd.reject(data);
                        }
                    });
                });
            },
			updateAction: function (postData) {
                newPostData = postData + '&project_id='+currentProjectId;
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: '/dashboard/query/update',
                        type: 'POST',
                        dataType: 'json',
                        data: newPostData,
                        success: function (data) {
                            console.log(data);
                            $dfd.resolve(data);
                        },
                        error: function (a,b,c) {
                            console.log(a);
                            console.log(b);
                            console.log(c);
                            $dfd.reject();
                        }
                    });
                });
            }
            //deleteAction: '/dashboard/query/delete',
        },
        fields: {
            query_id: {
                title: 'id',
                width: '1%',
                type:'hidden',
                key: true,
                edit: false,
                create: false,
				sorting: false,
            },

            project_id: {
                title: 'proj id',
                width: '1%',
                type:'hidden',
                edit: false,
                create: false,
                sorting: false
            },

            query_date: {
                title: 'Last updated',
                width: '15%',
                create: false,
                edit: false,
				sorting: true,
            },

            query_terms: {
                title: 'Query Terms',
                width: '40%',
				sorting: true,
                create: true,
                edit: false
            },

            initial_search_date: {
                title: 'Search From',
                width: '15%',
                sorting: true,
                create: true,
                edit: true,
                type: 'date',
            },

            delete_button: {
                title: '',
                width: '5%',
                sorting: false,
                create: false,
                edit: false,
                display: function (data) {
                    var buttonString = '<button type="button" id="deleteQueryButton'+data.record.query_id+'" ' +
                        'class="jtable-command-button jtable-delete-command-button" ' +
                        'onclick="onClickDeleteQuery(' + data.record.query_id + ')"/>';
                    return buttonString;
                }
            }
        }
    });
    $('#queries-data-grid').jtable('option', 'pageSize', 10);
    $('#queries-data-grid').jtable("load");
}

function setSelectedQuery(data) {
    var records = data.records;
    var newestDate = new Date(records[0].query_date);
    var newestRecord = data.records[0];
    records.forEach(function (r) {
        var date = new Date(r.query_date);
        if(date.getTime() > newestDate.getTime()) {
            newestDate = date;
            newestRecord = r;
        }
    });
    var row = $('#queries-data-grid').jtable('getRowByKey', newestRecord.query_id);
    $('#queries-data-grid').jtable('selectRows', row);
}

function onClickSaveSelectedQuery() {
    var $selectedRows = $('#queries-data-grid').jtable('selectedRows');
    if($selectedRows.data('record') === undefined) {
        console.log('no selection');
    }
    else {
        $("body").addClass("loading");
        $.ajax({
            url: 'dashboard/query/update',
            type: 'POST',
            dataType: 'json',
            data: {project_id: currentProjectId, query_id: $selectedRows.data('record').query_id, initial_search_date: $selectedRows.data('record').initial_search_date },
            success: function (data) {
                $('#currentQuery').html($selectedRows.data('record').query_terms);
                $("body").removeClass("loading");
                $('#queryInputModal').modal('toggle');
            },
            error: function (a,b,c) {
                console.log(a);
                console.log(b);
                console.log(c);
                alert("Something went wrong");
                $("body").removeClass("loading");
            }
        });
    }
}

function onClickSaveSelectedQueryAndSearch() {
    var $selectedRows = $('#queries-data-grid').jtable('selectedRows');
    if($selectedRows.data('record') === undefined) {
        console.log('no selection');
    }
    else {
        $("body").addClass("loading");
        $.ajax({
            url: 'dashboard/query/update',
            type: 'POST',
            dataType: 'json',
            data: {project_id: currentProjectId, query_id: $selectedRows.data('record').query_id, initial_search_date: $selectedRows.data('record').initial_search_date, result_count: 0 },
            success: function (data) {
                $('#currentQuery').html($selectedRows.data('record').query_terms);
                $('#queryInputModal').modal('toggle');
                searchVideos();
            },
            error: function (a,b,c) {
                alert("Something went wrong");
                $("body").removeClass("loading");
            }
        });
    }
}

function searchVideos() {
    $.ajax({
        url: 'dashboard/videos_stats/collect',
        type: 'POST',
        dataType: 'json',
        data: {project_id: currentProjectId},
        success: function (data) {
            console.log(data);
            updateReviewVideosTable();
            $("body").removeClass("loading");
        },
        error: function (a,b,c) {
            alert("Something went wrong");
            $("body").removeClass("loading");
            console.log(a);
            console.log(b);
            console.log(c);
        }
    });
}

function onClickDeleteQuery(queryId) {
    $('#addQueryButton').prop('disabled', true);
    $.ajax({
        url: 'dashboard/query/delete',
        type: 'POST',
        dataType: 'json',
        data: {query_id: queryId },
        success: function (data) {
            $('#queries-data-grid').jtable("load");
            $('#addQueryButton').prop('disabled', false);
        },
        error: function (a,b,c) {
            console.log(a);
            console.log(b);
            console.log(c);
            $('#addQueryButton').prop('disabled', false);
            alert("Something went wrong");
        }
    });
}

function initAddQueryButton() {
	$("#addQueryButton").click(function() {
        $('#addQueryButton').prop('disabled', true);
		$.ajax({
            url: 'dashboard/query/insert?project_id=' + currentProjectId,
            type: 'POST',
            dataType: 'json',
            data: {query_terms: $("#addQueryInputText").val() },
            success: function (data) {
                $('#queries-data-grid').jtable("load");
                console.log(data);
                $('#addQueryButton').prop('disabled', false);
            },
            error: function (a,b,c) {
                console.log(a);
                console.log(b);
                console.log(c);
                $('#addQueryButton').prop('disabled', false);
                alert("Something went wrong");
            }
        });
	});
}

//Queries stuff - end    --------------------------

//Wishlists - Start -------------------------------
function initSellingDataScreen() {
    initDateRangeSellingData();
    initFileDialogSellingData();
    createSellingdataGrid();
}

function initDateRangeSellingData() {
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrangesellingdata span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrangesellingdata').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'Last 6 Months': [moment().subtract(6, 'month'), moment()],
            'Last Year': [moment().subtract(12, 'month'), moment()],
            'All Time': [moment("20160101"), moment()],
        }
    }, cb);
	
	$('#reportrangesellingdata').on('apply.daterangepicker', function(ev, picker) {
		fromDateWishlists = picker.startDate.format('YYYY-MM-DD');
		toDateWishlists = picker.endDate.format('YYYY-MM-DD');
        updateSellingDataTable();
	});

	fromDateWishlists = start.format('YYYY-MM-DD');
	toDateWishlists = end.format('YYYY-MM-DD');
    cb(start, end);
}

function createSellingdataGrid() {
    
    $('#selling-data-grid').jtable({
        title: 'Selling Data',
        useBootstrap: true,
        paging: true, //Enable paging
		sorting: true,
		defaultSorting: 'stats_date ASC',
        editinline:{enable:true, img:"img/"},

        recordsLoaded: function(event, data) { //when tha table shows
            // $('td:nth-child(7),th:nth-child(7)').hide(); //Hide the edit button column
            // $('td:nth-child(6),th:nth-child(6)').show();
		},
		
		recordUpdated: function(event, data) {
			console.log(data);
        },
        
        toolbar: {
            hoverAnimation: true, //Enable/disable small animation on mouse hover to a toolbar item.
            hoverAnimationDuration: 60, //Duration of the hover animation.
            hoverAnimationEasing: undefined, //Easing of the hover animation. Uses jQuery's default animation ('swing') if set to undefined.
            //Array of your custom toolbar items.
            items: [
                {                          
                    text: 'Import selling data',
                    click: function () {
                        $("#sellingDataImportSellingData").trigger("click");
                    }
                },
                {                          
                    text: 'Import wishlists data',
                    click: function () {
                        $("#sellingDataImportWishlistsData").trigger("click");
                    }
                }
            ]
        },
		
        actions: {
            listAction:  function (postData, jtParams) {
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: '/dashboard/wishlist_purchase_stats/list?project_id=' + currentProjectId + '&fromDate=' + fromDateWishlists + '&toDate=' + toDateWishlists + '&jtStartIndex='+  jtParams.jtStartIndex.toString()  +'&jtPageSize='+ jtParams.jtPageSize.toString() + '&jtSorting=' + encodeURI(jtParams.jtSorting),
                        type: 'POST',
                        dataType: 'json',
                        data: postData,
                        success: function (data) {
                            console.log(data);
                            $dfd.resolve(data);
                        },
                        error: function (a,b,c) {
                            console.log(a);
                            console.log(b);
                            console.log(c);
                            $dfd.reject();
                        }
                    });
                });
            },
			updateAction: '/dashboard/wishlist_purchase_stats/update',
            deleteAction: function (postData) {
                //project_id=&stats_date=&stats_type=1&stats_additions=0&stats_deletions=0&stats_activations=0&stats_gifts=0
                //var newPostData = postData.replace('project_id=','project_id='+currentProjectId);
                console.log(postData);
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: 'dashboard/wishlist_purchase_stats/delete',
                        type: 'POST',
                        dataType: 'json',
                        data: postData,
                        success: function (data) {
                            console.log(data);
                            $dfd.resolve(data);
                        },
                        error: function (a,b,c) {
                            console.log(a);
                            console.log(b);
                            console.log(c);
                            $dfd.reject();
                        }
                    });
                });
            },
            // createAction:  function (postData) {
            //     //project_id=&stats_date=&stats_type=1&stats_additions=0&stats_deletions=0&stats_activations=0&stats_gifts=0
            //     var newPostData = postData.replace('project_id=','project_id='+currentProjectId);
            //     return $.Deferred(function ($dfd) {
            //         $.ajax({
            //             url: 'dashboard/wishlist_stats/insert',
            //             type: 'POST',
            //             dataType: 'json',
            //             data: newPostData,
            //             success: function (data) {
            //                 console.log(data);
            //                 $dfd.resolve(data);
            //             },
            //             error: function (a,b,c) {
            //                 console.log(a);
            //                 console.log(b);
            //                 console.log(c);
            //                 $dfd.reject();
            //             }
            //         });
            //     });
            // },
        },
        fields: {
            stats_id: {
                title: 'id',
                width: '1%',
                type:'hidden',
                key: true,
                edit: false,
                create: false,
				sorting: false,
            },

            project_id: {
                title: 'proj id',
                width: '1%',
                type:'hidden',
                edit: false,
                create: true,
                sorting: false
            },

            stats_date: {
                title: 'Date',
                width: '20%',
                type: 'date',
                create: true,
                edit: true,
                dependsOn: ['stats_id', 'project_id', 'stats_type'],
            },

            stats_type: {
                title: 'Type',
                width: '20%',
                edit: true,
                create: true,
                sorting: true,
                options: {'1' : 'Purchased', '2' : 'Wishlist'}
            },

            stats_additions: {
                title: 'Additions',
                width: '10%',
                edit: true,
                create: true,
                sorting: true,
                defaultValue: '0'
            },

            stats_deletions: {
                title: 'Deletions',
                width: '10%',
                create: true,
                edit: true,
                sorting: true,
                defaultValue: '0'
            },

            stats_activations: {
                title: 'Activations',
                width: '10%',
                create: true,
                edit: true,
                sorting: true,
                defaultValue: '0'
            },

            stats_gifts: {
                title: 'Gifts',
                width: '10%',
				create: true,
                edit: true,
                sorting: true,
                defaultValue: '0'
            },

            go_to_chart_button: {
                title: '',
                width: '5%',
                sorting: false,
                create: false,
                edit: false,
                display: function (data) {
                    var buttonString = '<button type="button" id="goToGraphics'+data.record.stats_id+'" ' +
                        'style="border:0px solid transparent; background-color:transparent;" class="btn btn-default glyphicon glyphicon-stats" ' +                        
                        'onclick="onClickViewDateOnChart(\'' + data.record.stats_date + '\')"/>';
                    return buttonString;
                }
            }
        }
    });
    
    $('#selling-data-grid').jtable('option', 'pageSize', 10);

    $('#selling-data-grid').jtable('load');
}

function updateSellingDataTable() {
	$("#selling-data-grid").jtable("load");
}

function initFileDialogSellingData() {
    $("#sellingDataImportWishlistsData").change(postWishlistsFile);
    $("#sellingDataImportSellingData").change(postSellingDataFile);
}

function postWishlistsFile(evt) {
    var f= evt.target.files[0];
    if(f) {
        var r = new FileReader();
        r.onload =function(e) {
            var contents = e.target.result;
            var parsedContents = contents.replace(/\'/g, '\"');
            parsedContents = JSON.parse(parsedContents);

            $("body").addClass("loading");

            $.ajax({
                url: 'dashboard/wishlist_stats/insertMultiple',
                type: 'POST',
                dataType: 'html',
                data: { projectId: currentProjectId, statsType: 2, data: parsedContents },
                success: function (data) {
                    console.log(data);
                    updateSellingDataTable();
                    udpateDashboardChart();
                    $("body").removeClass("loading");
                },
                error: function (a,b,c) {
                    $("body").removeClass("loading");
                    console.log(a);
                    console.log(b);
                    console.log(c);
                }
            });
        };
        r.readAsText(f);
    }
    else {
        alert("Error loading file");
    }
}

function postSellingDataFile(evt) {
    var f= evt.target.files[0];
    if(f) {
        var r = new FileReader();
        r.onload =function(e) {
            var contents = e.target.result;
            var parsedContents = contents.replace(/\'/g, '\"');
            parsedContents = JSON.parse(parsedContents);

            $("body").addClass("loading");

            $.ajax({
                url: 'dashboard/purchase_stats/insertMultiple',
                type: 'POST',
                dataType: 'html',
                data: { projectId: currentProjectId, statsType: 1, data: parsedContents },
                success: function (data) {
                    console.log(data);
                    updateSellingDataTable();
                    udpateDashboardChart();
                    $("body").removeClass("loading");
                },
                error: function (a,b,c) {
                    $("body").removeClass("loading");
                    console.log(a);
                    console.log(b);
                    console.log(c);
                }
            });
        };
        r.readAsText(f);
    }
    else {
        alert("Error loading file");
    }
}

function onClickViewDateOnChart(date) {
    $("#dashboardTabId").trigger('click');

    var start = moment(date.replace(/-/g, '')).subtract(3, 'days');
    var end = moment(date.replace(/-/g, '')).add(3, 'days');
    $('#reportrangedashboard').data('daterangepicker').setStartDate(start);
    $('#reportrangedashboard').data('daterangepicker').setEndDate(end);
    $('#reportrangedashboard').trigger('apply.daterangepicker', $('#reportrangedashboard').data('daterangepicker'));
}
//Wishlists - End   -------------------------------

//Events - Start ----------------------------------
function initEventsScreen() {
    initDateRangeEvents();
    createEventsGrid();
}

function initDateRangeEvents() {
    var start = moment("20170101");
    var end = moment();

    function cb(start, end) {
        $('#reportrangeevents span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrangeevents').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'Last 6 Months': [moment().subtract(6, 'month'), moment()],
            'Last Year': [moment().subtract(12, 'month'), moment()],
            'All Time': [moment("20160101"), moment()],
        }
    }, cb);
	
	$('#reportrangeevents').on('apply.daterangepicker', function(ev, picker) {
		fromDateEvents = picker.startDate.format('YYYY-MM-DD');
		toDateEvents = picker.endDate.format('YYYY-MM-DD');
        updateEventsTable();
	});

	fromDateEvents = start.format('YYYY-MM-DD');
	toDateEvents = end.format('YYYY-MM-DD');
    cb(start, end);
}

function createEventsGrid() {
    
    $('#events-data-grid').jtable({
        title: 'Events',
        useBootstrap: true,
        paging: true, //Enable paging
		sorting: true,
		defaultSorting: 'project_stats_event_date ASC',
        editinline:{enable:true, img:"img/"},

        recordsLoaded: function(event, data) { //when tha table shows
            // $('td:nth-child(7),th:nth-child(7)').hide(); //Hide the edit button column
            // $('td:nth-child(6),th:nth-child(6)').show();
		},
		
		recordUpdated: function(event, data) {
			console.log(data);
        },
        
        toolbar: {
            hoverAnimation: true, //Enable/disable small animation on mouse hover to a toolbar item.
            hoverAnimationDuration: 60, //Duration of the hover animation.
            hoverAnimationEasing: undefined, //Easing of the hover animation. Uses jQuery's default animation ('swing') if set to undefined.
        },
		
        actions: {
            listAction:  function (postData, jtParams) {
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: '/dashboard/stats_events/list?project_id=' + currentProjectId + '&fromDate=' + fromDateEvents + '&toDate=' + toDateEvents + '&jtStartIndex='+  jtParams.jtStartIndex.toString()  +'&jtPageSize='+ jtParams.jtPageSize.toString() + '&jtSorting=' + encodeURI(jtParams.jtSorting),
                        type: 'POST',
                        dataType: 'json',
                        data: postData,
                        success: function (data) {
                            console.log(data);
                            $dfd.resolve(data);
                        },
                        error: function (a,b,c) {
                            console.log(a);
                            console.log(b);
                            console.log(c);
                            $dfd.reject();
                        }
                    });
                });
            },
			updateAction: '/dashboard/stats_events/update',
            deleteAction: function (postData) {
                console.log(postData);
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: 'dashboard/stats_events/delete',
                        type: 'POST',
                        dataType: 'json',
                        data: postData,
                        success: function (data) {
                            console.log(data);
                            $dfd.resolve(data);
                        },
                        error: function (a,b,c) {
                            console.log(a);
                            console.log(b);
                            console.log(c);
                            $dfd.reject();
                        }
                    });
                });
            },
            createAction:  function (postData) {
                var newPostData = postData.replace('project_id=','project_id='+currentProjectId);
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: 'dashboard/stats_events/insert',
                        type: 'POST',
                        dataType: 'json',
                        data: newPostData,
                        success: function (data) {
                            console.log(data);
                            $dfd.resolve(data);
                        },
                        error: function (a,b,c) {
                            console.log(a);
                            console.log(b);
                            console.log(c);
                            $dfd.reject();
                        }
                    });
                });
            },
        },
        fields: {
            project_stats_event_id: {
                title: 'id',
                width: '1%',
                type:'hidden',
                key: true,
                edit: false,
                create: false,
				sorting: false,
            },

            project_id: {
                title: 'proj id',
                width: '1%',
                type:'hidden',
                edit: false,
                create: true,
                sorting: false
            },

            project_stats_event_date: {
                title: 'Date',
                width: '15%',
                type: 'date',
                create: true,
                edit: true,
                sorting: true,
            },

            project_stats_event_name: {
                title: 'Name',
                width: '15%',
                edit: true,
                create: true,
                sorting: true,
                defaultValue: ''
            },

            project_stats_event_description: {
                title: 'Description',
                width: '35%',
                create: true,
                edit: true,
                defaultValue: ''
            },

            go_to_chart_button: {
                title: '',
                width: '5%',
                sorting: false,
                create: false,
                edit: false,
                display: function (data) {
                    var buttonString = '<button type="button" id="goToGraphics'+data.record.stats_id+'" ' +
                        'style="border:0px solid transparent; background-color:transparent;" class="btn btn-default glyphicon glyphicon-stats" ' +
                        'onclick="onClickViewDateOnChart(\'' + data.record.project_stats_event_date + '\')"/>';
                    return buttonString;
                }
            }
        }
    });
    
    $('#events-data-grid').jtable('option', 'pageSize', 10);

    $('#events-data-grid').jtable('load');
}

function updateEventsTable() {
	$("#events-data-grid").jtable("load");
}
//Events - End ------------------------------------

//Review channels - Start  --------------------------
function initReviewChannelsScreen() {
	initDateRangeChannelsReview();
    initReviewChannelsToggle();
    createReviewChannelsGrid();
    updateExportChannelsButton();
}

function initReviewChannelsToggle() {
	$("#reviewedChannelCheckbox").change(function() {
        showReviewedChannels = this.checked;
        updateExportChannelsButton();
		updateReviewChannelsTable();
	});
}

function initDateRangeChannelsReview() {
    var start = moment("20160101");
    var end = moment();

    function cbChannels(start, end) {
        $('#reportrangereviewchannels span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrangereviewchannels').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'Last 6 Months': [moment().subtract(6, 'month'), moment()],
            'Last Year': [moment().subtract(12, 'month'), moment()],
            'All Time': [moment("20160101"), moment()],
        }
    }, cbChannels);
	
	$('#reportrangereviewchannels').on('apply.daterangepicker', function(ev, picker) {
		fromDateChannelReview = picker.startDate.format('YYYY-MM-DD');
		toDateChannelReview = picker.endDate.format('YYYY-MM-DD');
		updateReviewChannelsTable();
	});

	fromDateChannelReview = start.format('YYYY-MM-DD');
	toDateChannelReview = end.format('YYYY-MM-DD');
    cbChannels(start, end);
}

function updateExportChannelsButton() {
    $("#exportChannelsCSV").toggle(showReviewedChannels);
}

function updateReviewChannelsTable() {
	$("#channel-data-grid").jtable("load");
}

function setChannelsPageReviewed() {
    var channelIds = [];
    $('#channel-data-grid tr:visible').each(function(i, tr) {
        var key = $(this).attr('data-record-key');
        if(key !== undefined) {
            channelIds.push(key);
        }
    });

    $.ajax({
        url: '/dashboard/channel_review/update_list',
        type: 'POST',
        dataType: 'json',
        data: {project_id: currentProjectId, channel_ids: channelIds, reviewed: 1},
        success: function (data) {
            console.log(data);

            //channelIds.forEach(function(v)
            {
                $('#channel-data-grid').jtable('deleteRecord', {
					keys: channelIds,
					clientOnly: true,
					success: (function() {
						// $('#video-data-grid').jtable('_changePage', {pageNo: 2});
					}),
					error: (function() {
						// alert("record deletion error!");
					})
				}); 
            }//);
        },
        error: function (a,b,c) {
            console.log(a);
            console.log(b);
            console.log(c);
        }
    });
}

function toggleChannelsPageValid() {
    var channelIds = [];
    var spanNewValues = [];
    $('#channel-data-grid tr:visible').each(function(i, tr) {
        var key = $(this).attr('data-record-key');
        var span = $(this).find('td:eq(5)').find('span')[0];
        if(span !== undefined) {
            console.log(($(span).attr('val') == 1 ? 0 : 1));
            spanNewValues.push(($(span).attr('val') == 1 ? 0 : 1));
            channelIds.push(key);
        }
    });

    $.ajax({
        url: '/dashboard/channel_review/update_list',
        type: 'POST',
        dataType: 'json',
        data: {project_id: currentProjectId, channel_ids: channelIds, valid_values: spanNewValues},
        success: function (data) {
            $("#video-data-grid").jtable("load");
            console.log(data);
        },
        error: function (a,b,c) {
            console.log(a);
            console.log(b);
            console.log(c);
        }
    });
}

function onClickExportChannelsCSV() {
    $("body").addClass("loading");
    
    $.ajax({
        url: '/dashboard/channel_review_csv',
        type: 'POST',
        dataType: 'json',
        data: { project_id: currentProjectId },
        success: function (data) {
            $("body").removeClass("loading");
            console.log(data);

			var csvLinkGoogle ="data:application/csv;charset=utf-8,";
            var csvDataGoogle = "Channel Link\tChannel Name\tSubscribers\tSimilar Game Found\tCountry\tSearch\tInteresting\tContacted\tDate of Contact\tContact Info\tContact Secondary\t"
                + "Version to Contact\tComments\tRefs to Sim\tViews\tViews per Sub \t Views Avg\tViews Err\r\n";

			csvLinkGoogle = $("#tempTextArea").val();

            console.log(data.data);

			for(var i = 0; i < data.data.length; ++i) {
                csvDataGoogle +=
                    YOUTUBE_CHANNEL_BASE_URL + data.data[i].project_id + "\t" + //link
                    data.data[i].name + "\t" + //name
                    data.data[i].subscribers + "\t" + //subs
                    data.data[i].sim_games_list + "\t" + //sim game found
                    data.data[i].country + "\t" + //country
                    "" + "\t" + //search
                    "" + "\t" + //interesting
                    "" + "\t" + //contacted
                    "" + "\t" + //date of contact
                    "" + "\t" + //contact info
                    "" + "\t" + //contact secondary
                    "" + "\t" + //version to contact
                    "" + "\t" + //comments
                    data.data[i].sim_games_count + "\t" + //refs to sim
                    "" + "\t" + //views
                    "" + "\t" + //views / sub
                    data.data[i].views_avg + "\t" + //views avg
                    data.data[i].views_err + "\t" + //views err
                    "\r\n";
			}
            console.log(csvDataGoogle);

			csvLinkGoogle += encodeURIComponent(csvDataGoogle);

			if($("#downloadChannelsCSVArea textArea").length == 0) {
				$("#downloadChannelsCSVArea").append("<textarea id=\"tempTextArea\" class=\"hidden\"/>");
				
				var textFileAsBlob = new Blob([csvDataGoogle], {type:'text/plain'});
				var fileNameToSaveAs = 'ChannelsEvaluationReport-' + fromDateChart + '_' + toDateChart + '.csv';

				var downloadLink = document.createElement("a");
				downloadLink.download = fileNameToSaveAs;
				downloadLink.innerHTML = "Download File";
				if (window.URL != null)
				{
					// Chrome allows the link to be clicked
					// without actually adding it to the DOM.
					downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
				}
				else
				{
					// Firefox requires the link to be added to the DOM
					// before it can be clicked.
					downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
					downloadLink.onclick = destroyClickedElement;
					downloadLink.style.display = "none";
					document.body.appendChild(downloadLink);
				}
				downloadLink.click();
			}
			else {
				$("#downloadChannelsCSVArea").find("#tempTextArea").val(csvDataGoogle);
			}
        },
        error: function (a,b,c) {
            $("body").removeClass("loading");
            console.log(a);
            console.log(b);
            console.log(c);
        }
    });
}

function updateChannelsToReviewBadge(amount) {
    if(!$("#reviewedChannelCheckbox")[0].checked) {
        if(amount > 0) {
            $('#reviewChannelsCountBadge').show();
            $('#reviewChannelsCountBadge').text(amount);
        }
        else{
            $('#reviewChannelsCountBadge').hide();
        }
    }
    else {
        $('#reviewChannelsCountBadge').hide();
    }
}

/*Cria a grid do CRUD usando Jtable*/
function createReviewChannelsGrid() {
    $('#channel-data-grid').jtable({
        title: 'Channels to review',
        useBootstrap: true,
        paging: true, //Enable paging
		sorting: true,
		defaultSorting: 'sim_games_count DESC',
        editinline:{enable:true, img:"img/"},

        toolbar: {
            hoverAnimation: true, //Enable/disable small animation on mouse hover to a toolbar item.
            hoverAnimationDuration: 60, //Duration of the hover animation.
            hoverAnimationEasing: undefined, //Easing of the hover animation. Uses jQuery's default animation ('swing') if set to undefined.
            //Array of your custom toolbar items.
            items: [
                {                          
                    text: 'Set page reviewed',
                    click: function () {
                        setChannelsPageReviewed();
                    }
                },
            ]
        },

        recordsLoaded: function(event, data) { //when tha table shows
            console.log(data);
            channelsData = data;
			// $('td:nth-child(8),th:nth-child(8)').hide(); //hide the 5th column for the 2nd level child table
		},
		
		recordUpdated: function(event, data) {
            console.log(data);
            
			var record = JSON.parse(data.serverResponse.Records);
			
			if(record['removed']) {
				$('#channel-data-grid').jtable('deleteRecord', {
					key: record['id'],
					clientOnly: true,
					success: (function() {
						// $('#video-data-grid').jtable('_changePage', {pageNo: 2});
					}),
					error: (function() {
						// alert("record deletion error!");
					})
				});
			}
		},
		
        actions: {
            listAction:  function (postData, jtParams) {
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: '/dashboard/channel_review/list?projectId='+currentProjectId +'&reviewed=' + showReviewedChannels + '&fromDate=' + fromDateChannelReview + '&toDate=' + toDateVideoReview + '&jtStartIndex='+  jtParams.jtStartIndex.toString()  +'&jtPageSize='+ jtParams.jtPageSize.toString() + '&jtSorting=' + encodeURI(jtParams.jtSorting),
                        type: 'POST',
                        dataType: 'json',
                        data: postData,
                        success: function (data) {
                            $dfd.resolve(data);
                            updateChannelsToReviewBadge(data.TotalRecordCount);
                        },
                        error: function (a,b,c) {
                            console.log(a);
                            console.log(b);
                            console.log(c);
                            $dfd.reject();
                        }
                    });
                });
            },
			updateAction: function (postData) {
                newPostData = postData + '&project_id='+currentProjectId;
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: '/dashboard/channel_review/update',
                        type: 'POST',
                        dataType: 'json',
                        data: newPostData,
                        success: function (data) {
                            // console.log(data);
                            $dfd.resolve(data);
                        },
                        error: function (a,b,c) {
                            // console.log(a);
                            // console.log(b);
                            // console.log(c);
                            $dfd.reject();
                        }
                    });
                });
            },
        },
        fields: {
            project_id: {
                title: 'proj id',
                width: '1%',
                type:'hidden',
                edit: false,
                create: false,
				sorting: false,
            },

            id: {
                title: 'Link',
                width: '8%',
                key: true,
                list: true,
                edit: false,
                create: false,
				sorting: false,
                display: function (data) {
                    return '<a target="_blank" href="' + YOUTUBE_CHANNEL_BASE_URL + data.record.id + '">' + YOUTUBE_CHANNEL_BASE_URL + data.record.id + '</a>';
                }
            },

            name: {
                title: 'Title',
                width: '12%',
                create: false,
                edit: false,
				sorting: false,
            },

            date_found: {
                title: 'Date Found',
                width: '10%',
                type: 'date',
                create: false,
                edit: false
            },

            active: {
                title: 'Active',
                width: '3%',
                create: false,
                edit: false
            },

            subscribers: {
                title: 'Subs',
                width: '3%',
                create: false,
                edit: false
            },

            sim_games_count: {
                title: 'Sim Games',
                width: '3%',
                create: false,
                edit: false
            },

            views_avg: {
                title: 'Views Avg',
                width: '3%',
                create: false,
                edit: false
            },

            views_err: {
                title: 'Views Err',
                width: '3%',
                create: false,
                edit: false
            },

            valid: {
                title: 'Valid',
                type: 'checkbox',
                width: '3%',
                values: { 'false': '0', 'true': '1' },
                defaultValue: true
            },

            reviewed: {
                title: 'Reviewed',
                type: 'checkbox',
                width: '3%',
                values: { 'false': '0', 'true': '1' },
                defaultValue: false,
            },
        }
    });
    $('#channel-data-grid').jtable('option', 'pageSize', 10);

    $("#channel-data-grid").jtable("load");
}
//Review Channels - End    --------------------------

$(document).ready(function() {
        initDashboardScreen();
        initQueriesStuff();
        initReviewVideosScreen();
        initSellingDataScreen();
        initEventsScreen();
        initSidebar();
        initReviewChannelsScreen();
});

$(document).on({
    ajaxStart: function() {$body.addClass("loading");},
    ajaxStop: function() {$body.removeClass("loading");}
});



//utils
function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}
