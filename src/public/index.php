<?php
    session_start();

    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;

    require '../vendor/autoload.php';
    require_once '../api/classes/utils/JsonUtils.php';
    require_once '../api/classes/utils/ResponseCodes.php';
    require_once '../api/classes/YoutuberFinder.php';
    require_once '../api/classes/VideoFinder.php';
    require_once '../api/classes/YoutubeChannelEvaluator.php';
    require_once '../api/classes/Authenticator.php';
    require_once '../vendor/google/apiclient/src/Google/Client.php';
    require_once '../vendor/google/apiclient-services/src/Google/Service/YouTube.php';

    //database requires
    require_once '../api/classes/DatabaseHandler.php';
    #require_once DBAPI;

    #Instancia do pdo


    $settings = ['settings' => [
            'displayErrorDetails' => true, // set to false in production
            ],];

    mb_internal_encoding('UTF-8');
    mb_http_output('UTF-8');

    // $c = new \Slim\Container();
    // $c['view'] = function($c) {
    //         return new \Slim\Views\PhpRenderer($_SERVER["DOCUMENT_ROOT"] . "/view");
    //     };

    $app = new \Slim\App($settings);



    $c = $app->getContainer();
    $c['view'] = function($c) {
            return new \Slim\Views\PhpRenderer($_SERVER["DOCUMENT_ROOT"] . "/view");
        };

    require_once '../api/classes/routes/WishlistsPurchasesRoutes.php';
    require_once '../api/classes/routes/DashboardRoutes.php';
    require_once '../api/classes/routes/ProjectStatsEventRoutes.php';
    require_once '../api/classes/routes/VideosStatsRoutes.php';
    require_once '../api/classes/routes/ReviewVideosRoutes.php';
    require_once '../api/classes/routes/ReviewChannelsRoutes.php';
    require_once '../api/classes/routes/YoutuberFinderRoutes.php';
    require_once '../api/classes/routes/TwitchFinderRoutes.php';
    require_once '../api/classes/routes/DouyuFinderRoutes.php';
    require_once '../api/classes/routes/EvaluateYoutubeChannelRoutes.php';

    $app->get('/', function($request, $response, array $args) {
        if(isset($_SESSION['user'])) {
            $username = $_SESSION['user']['user_login'];
            $password = $_SESSION['user']['user_password'];

            $user_data = DatabaseHandler::getUser($username);
            $authenticator = new Authenticator($user_data);
        
            if ($authenticator->check($username, $password)){
                $authenticator->access(); 
                return $this->view->render($response->withStatus(200)->withHeader('Location', '/dashboard'), 'pages/dashboard.php');
            }
            return $this->view->render($response, 'pages/login_page.php');
        }
        else {
            return $this->view->render($response, 'pages/login_page.php');
        }
    });

    


    $app->post('/verify_login', function($request, $response, array $args) {
        $username = $request->getParsedBody()['username'];
        $password =  $request->getParsedBody()['password'];
        
        $user_data = DatabaseHandler::getUser($username);
        
        #$response = JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", $user_data);
        $authenticator = new Authenticator($user_data);
        
        #$teste = $authenticator->check($password);
        if ($authenticator ->check($username, $password)){
            $authenticator->access(); 
            $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, "Success"));
            return $response->withRedirect('/dashboard', 200);
        }
        $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::UNAUTHORIZED, "Invalid login or password"));
        return $response->withRedirect('/', 401);
        
    });

    
    $app->get('/hello/{name}', function (Request $request, Response $response) {
        $name = $request->getAttribute('name');
        $response->getBody()->write("Hello, $name");

        return $response;
    });

    $app->get('/dashboard',  function($request, $response, array $args) {
        if (!isset($_SESSION['user']) or !is_array($_SESSION['user'])){
            return $response->withStatus(301)->withHeader('Location', '/');
        }
        else{
            return $this->view->render($response, 'pages/dashboard.php');
        }
    });

    $app->get('/search_twitch', function($request, $response, array $args) {
        return $this->view->render($response, 'pages/search_twitch_page.php');
    });

    $app->get('/search_youtube', function($request, $response, array $args) {
        return $this->view->render($response, 'pages/search_page.php');
    });

    $app->get('/search_douyu', function($request, $response, array $args) {
        return $this->view->render($response, 'pages/search_douyu_page.php');
    });

    $app->get('/evaluate_youtube_channels', function($request, $response, array $args) {
        return $this->view->render($response, 'pages/evaluate_youtube_channels.php');
    });

    //images{
    $app->get('/img/{imageName}', function($request, $response, array $args) {
        $imageName = $request->getAttribute('imageName');
        $image = file_get_contents('view/img/'.$imageName);
        $response->write($image);
        return $response->withHeader('Content-Type', FILEINFO_MIME_TYPE);
    });

    $auth = function () use($app) {
        if (!isset($_SESSION['user']) or !is_array($_SESSION['user']))
            $app->redirect('/login');
    };

    $app->post('/insert_user', function($request, $response, array $args) {
        
        $username = $request->getParsedBody()['username'];
        $password =  $request->getParsedBody()['password'];
        $email = $request->getParsedBody()['email'];
        $fetch_user = DatabaseHandler::getUser($username);
        
        if(!is_null($fetch_user['user_id'])){
            $result = new stdClass();
            $result->user_login = $fetch_user['user_login'];
            $result->message = 'The user login '.$fetch_user['user_login'].'already exists!. Try another' ;
            $response = JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", $result);
        }

        else{
            
            
            $hash= password_hash($password, PASSWORD_DEFAULT);
            DatabaseHandler::insertUser($username,$hash,$email);

            $result = new stdClass();
            $result->user_login = $username;
            $result->message = 'The user login '.$username.'was created!';
            #$response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, json_encode($result)));
            $response = JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", $result);
            

        }
        

    
        
        
        return $response;
        
        
        return $response;

        
    });

    $app->get('/logout', function($request, $response, array $args) {
        
        unset($_SESSION['user']);
        
      


        #return $this->view->render($response, 'pages/insert_user.php');
    });

    $app->post('/signup',  function($request, $response, array $args) {
        
            return $this->view->render($response, 'pages/signup.php');
        
    });

    $app->run();

?>