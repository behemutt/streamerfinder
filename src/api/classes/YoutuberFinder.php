<?php

class YoutuberFinder {
	private static $API_KEY = "AIzaSyBagTzijrSdGdJCxj0wlyWFxi253hQ9Vlo"; //production key
	//private static $API_KEY = "AIzaSyDM3AgBT3RqV87BhhR7Qv3tmWN0_t9EVrw"; //Test key
	// private static $API_KEY = "AIzaSyC4wdeY2XfY0z2cequFQE5pRPfCg_0zFgM"; //Test key 2
	//private static $API_KEY = "AIzaSyDeoSwdFdZRNfSThnP6VHgS17bmN7P4n_g"; //Test key 3
	private static $client;
	private static $youtube;
	private static $MAX_SEARCH_PAGES = 6;
	private static $MAX_SEARCH_QUOTA = 7000;

	private static $QUOTA_SEARCH_LIST = 100;
	private static $QUOTA_CHANNELS_LIST = 1;
	private static $QUOTA_PLAYLISTITEMS_LIST = 1;
	private static $MIN_QUOTA_TO_BEGIN_SEARCH = 500;

	public static function exception_error_handler($errno, $errstr, $errfile, $errline) {
		throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
	}

	public static function initApiKey() {
		// $key_data = null;
		
		// if(DatabaseHandler::hasQuota(self::$MIN_QUOTA_TO_BEGIN_SEARCH)) {
		// 	$key_data = DatabaseHandler::getYoutubeApiKey(DatabaseHandler::getCurrentYoutubeApiKey()['key_id']);
		// }
		// else {
		// 	$key_data = DatabaseHandler::getNextYoutubeApiKey(self::$MIN_QUOTA_TO_BEGIN_SEARCH);
		// }

		// if($key_data == NULL) {
		// 	return false;
		// }

		$key = self::$API_KEY;//$key_data['api_key'];
		self::$client = new Google_Client();

		self::$client->setApplicationName("Youtube Reviewer Finder");
		self::$client->setDeveloperKey($key);

		self::$youtube = new Google_Service_YouTube(self::$client);
		return true;
	}

	public static function executeSearch($projectId) {
		$result = null;
		$quotaLimit = self::$MAX_SEARCH_QUOTA;
		$required_quota = self::$QUOTA_SEARCH_LIST * self::$MAX_SEARCH_PAGES;
		$cur_consumed_quota = 0;

		$search = DatabaseHandler::getSearch($projectId);

		if(empty($search['current_sim_game'])) {
			DatabaseHandler::updateCurrentSearchSimilarGame($projectId);
		}

		while(DatabaseHandler::hasQuota($required_quota) && ($cur_consumed_quota + $required_quota) < $quotaLimit)
		{
			$cur_sim_game = DatabaseHandler::getCurrentSearchSimilarGame($projectId);
			$result = self::searchChannelsSimple($projectId, $cur_sim_game, $search['video_age']);
				
			if($result->status != ResponseCode::OK) {
				return $result;
			}

			$cur_consumed_quota += $required_quota;
			DatabaseHandler::updateCurrentSearchSimilarGame($projectId);
		}

		$result = new stdClass();
		$result->status = ResponseCode::OK;
		$result->data = "Success!";
		return $result;
	}

	public static function searchChannelsSimple($projectId, $keywords, $videoAge) {
		set_error_handler("YoutuberFinder::exception_error_handler");

		if(is_null(self::$client) || !isset(self::$client)) {
			if(!self::initApiKey()) {
				return array();
			}
		}

		try {
			$today = new DateTime();
			$todayFormatted = $today->format('Y-m-d');
			$maxVideoAge = new DateTime();
			$maxVideoAge->sub(new DateInterval('P'.$videoAge.'M'));
			$publishedAfter = $maxVideoAge->format('Y-m-d\TH:i:s.s\Z');

			$first = true;
			$nextPageToken = '';
			$numberOfPagesToTry = 0;
			$usedChannels = array();
			
			while((isset($nextPageToken) || $first) && $numberOfPagesToTry < self::$MAX_SEARCH_PAGES) {
				$first = false;
				try {
					if(!DatabaseHandler::hasQuota(self::$QUOTA_SEARCH_LIST)) {
						if(!self::initApiKey()) {
							$result = new stdClass();
							$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
							$result->data = "Quota usage exceeded";
							return $result;
						}
					}

					DatabaseHandler::updateYoutubeKeyQuota(self::$QUOTA_SEARCH_LIST, 0);

					$searchResponse = self::$youtube->search->listSearch('id,snippet', array(
								'q' => $keywords,
								'maxResults' => 50,
								'pageToken' => $nextPageToken,
								'publishedAfter' => $publishedAfter
								));

				} catch(Exception $e) {
					$result = new stdClass();
					$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
					$result->data = "Error on listSearch. Params: " . $keywords . " " . $nextPageToken . " " . $publishedAfter . "\n" . $e->getTraceAsString();
					return $result;
				}
				
				$numberOfPagesToTry++;

				foreach ($searchResponse['items'] as $searchResult) {
					switch ($searchResult['id']['kind']) {
						case 'youtube#video':
							if(!in_array($searchResult['snippet']['channelId'], $usedChannels)) {
								array_push($usedChannels, $searchResult['snippet']['channelId']);

								$channelId = $searchResult['snippet']['channelId'];
								$channelTitle = $searchResult['snippet']['channelTitle'];
								$similar_game = $keywords;

								if(!DatabaseHandler::hasChannelDetails($projectId, $channelId)) {
									DatabaseHandler::insertChannelDetails($projectId, $channelId, $channelTitle, '');
								}

								if(!DatabaseHandler::hasChannelSimilarGame($projectId, $channelId, $keywords)) {
									DatabaseHandler::insertChannelSimilarGame($projectId, $channelId, $keywords);
									continue;
								}

								if(!DatabaseHandler::hasChannelEvaluation($channelId)) {
									DatabaseHandler::insertOrUpdateChannelEvaluation($channelId, 0, 0, 0, $todayFormatted, 0, 0);
								}
							}
					}
				}

				if(isset($searchResponse['nextPageToken'])) {
					$nextPageToken = $searchResponse['nextPageToken'];
				}
				else {
					break;
				}
			}
			$result = new stdClass();
			$result->status = ResponseCode::OK;
			$result->data = "Success!";
			return $result;
		} catch (Google_Service_Exception $e) {
			$result = new stdClass();
			$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
			$result->data = "Google_Service_Exception " . $e->getTraceAsString();
			return $result;
		} catch (Google_Exception $e) {
			$result = new stdClass();
			$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
			$result->data = "Google_Exception " . $e->getTraceAsString();
			return $result;
		} catch (Exception $e) {
			$result = new stdClass();
			$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
			$result->data = "Exception " . $e->getTraceAsString();
			return $result;
		}
	}

	public static function searchChannels($keywords, $videoAge, $minSubscribers, $maxSubscribers, $language, $similarGames, $apiToUse = 0) {
		set_error_handler("YoutuberFinder::exception_error_handler");
		if(is_null(self::$client) || !isset(self::$client)) {
			if(!self::initApiKey()) {
				return array();
			}
		}

		try {
			$maxVideoAge = new DateTime();
			$maxVideoAge->sub(new DateInterval('P'.$videoAge.'M'));
			$publishedAfter = $maxVideoAge->format('Y-m-d\TH:i:s.s\Z');
			$useLanguage = ($language != 'none');

			if($minSubscribers < 0) {
				$minSubscribers = 0;
			}

			if($maxSubscribers <= 0) {
				$maxSubscribers = 999999999999;
			}

			$nextPageToken = '';
			if(isset($_SESSION['used_channels'])) {
				$usedChannels = $_SESSION['used_channels'];
			}
			else {
				$usedChannels = array();
			}

			$resultChannels = array();
			$numberOfEvaluatedPages = 0;
			$currentSimilarGameIndex = 0;

			if(isset($_SESSION['similarGameIndex'])) {
				$currentSimilarGameIndex = $_SESSION['similarGameIndex'];
			}
			$_SESSION['similarGameIndex'] = $currentSimilarGameIndex + 1;
			$currentSimilarGame = $similarGames[$currentSimilarGameIndex];
			//$processedKeyWords = "+\"" . $currentSimilarGame . "\" " . $keywords;
			$numberOfPagesToTry = 0;
			
			while(isset($nextPageToken) && $numberOfPagesToTry < self::$MAX_SEARCH_PAGES) {
				try {
					// if(!DatabaseHandler::hasQuota(self::$QUOTA_SEARCH_LIST)) {
					// 	if(!self::initApiKey()) {
					// 		$_SESSION['evaluated_pages'] = $numberOfEvaluatedPages;
					// 		$_SESSION['used_channels'] = $usedChannels;
					// 		return $resultChannels;
					// 	}
					// }
					// DatabaseHandler::updateYoutubeKeyQuota(self::$QUOTA_SEARCH_LIST, 0);

					if($useLanguage)
					{
						$searchResponse = self::$youtube->search->listSearch('id,snippet', array(
									'q' => $keywords,
									'maxResults' => 50,
									'pageToken' => $nextPageToken,
									'publishedAfter' => $publishedAfter,
									'relevanceLanguage' => $language
									));
					}
					else
					{
						$searchResponse = self::$youtube->search->listSearch('id,snippet', array(
									'q' => $keywords,
									'maxResults' => 50,
									'pageToken' => $nextPageToken,
									'publishedAfter' => $publishedAfter
									));
					}
				} catch(Exception $e) {
					error_log($e->getMessage());
				}
				
				$numberOfPagesToTry++;
				$numberOfEvaluatedPages++;

				foreach ($searchResponse['items'] as $searchResult) {
					switch ($searchResult['id']['kind']) {
						case 'youtube#video':
							if(!in_array($searchResult['snippet']['channelId'], $usedChannels)) {
								//array_push($alreadyCheckedChannels, $searchResult['snippet']['channelId']);
								//$_SESSION["used_channels"] = $alreadyCheckedChannels;

								// if(!DatabaseHandler::hasQuota(self::$QUOTA_CHANNELS_LIST)) {
								// 	if(!self::initApiKey()) {
								// 		$_SESSION['evaluated_pages'] = $numberOfEvaluatedPages;
								// 		$_SESSION['used_channels'] = $usedChannels;
								// 		return $resultChannels;
								// 	}
								// }
								// DatabaseHandler::updateYoutubeKeyQuota(self::$QUOTA_CHANNELS_LIST, 0);

								$statistics = self::$youtube->channels->listChannels('snippet,contentDetails,statistics,brandingSettings', array(
                                        				'id' => $searchResult['snippet']['channelId']
														));
								if(count($statistics['items']) == 0) {
									continue;
								}
								$statistics_original = $statistics;
								$snippet = $statistics['items'][0]['snippet'];
								$statistics = $statistics['items'][0]['statistics'];
								if(!is_null($statistics) && ($statistics['subscriberCount'] < $minSubscribers || $statistics['subscriberCount'] > $maxSubscribers)) {
									//TODO: return error messagge
									continue;
								}

								//is the main game in the title
								$main_game_in_title = false;
								foreach($similarGames as $sg) {
									if(strpos(strtolower($searchResult['snippet']['title']), strtolower($sg)) !== false ||
									   strpos(strtolower($searchResult['snippet']['description']), strtolower($sg)) != false) {
										if(!is_null($statistics)) {
											array_push($usedChannels, $searchResult['snippet']['channelId']);
											$_SESSION["used_channels"] = $usedChannels;
											$resultFormatted['channelId'] = $searchResult['snippet']['channelId'];
											if(isset($searchResult['snippet']['channelTitle'])) {
												$resultFormatted['channelTitle'] = $searchResult['snippet']['channelTitle'];
											}
											else {
												$resultFormatted['channelTitle'] = $statistics['hiddenSubscriberCount'];
											}
		
											$resultFormatted['subscriberCount'] = $statistics['subscriberCount'];
											$resultFormatted['similarGame'] = $sg;
											$resultFormatted['country'] = $snippet['country'];
											array_push($resultChannels, $resultFormatted);
											$main_game_in_title = true;
											break;
										}
									}
								}

								if($main_game_in_title) {
									continue;
								}

								$uploads = self::retrieveUploads($statistics_original, $similarGames);

								if(is_null($uploads)) {
									continue;
								}
								
								array_push($usedChannels, $searchResult['snippet']['channelId']);
								$_SESSION["used_channels"] = $usedChannels;
								
								$resultFormatted['channelId'] = $searchResult['snippet']['channelId'];
								$resultFormatted['channelTitle'] = $uploads['hiddenSubscriberCount'];
								$resultFormatted['subscriberCount'] = $statistics['subscriberCount'];
								$resultFormatted['similarGame'] = $uploads['commentCount'];
								$resultFormatted['country'] = $snippet['country'];

								array_push($resultChannels, $resultFormatted);
							}
							break;
					}
				}
				if(isset($searchResponse['nextPageToken'])) {
					$nextPageToken = $searchResponse['nextPageToken'];
				}
				else {
					break;
				}
			}
			$_SESSION['evaluated_pages'] = $numberOfEvaluatedPages;
			$_SESSION['used_channels'] = $usedChannels;
			return $resultChannels;
		} catch (Google_Service_Exception $e) {
			return array();
		} catch (Google_Exception $e) {
			return array();
		} catch (Exception $e) {
			return array();
		}
	}

	public static function retrieveUploads($results, $similarGames) {

		if(!isset($results['items'])) {
			return null;
		}

		foreach($results['items'] as $item) {
			$playlistId = $item['contentDetails']['relatedPlaylists']['uploads'];
			$nextPageToken = '';
			$numberOfPagesToTry = 0;

			while($numberOfPagesToTry < self::$MAX_PAGES) {

				$numberOfPagesToTry++;

				// if(!DatabaseHandler::hasQuota(self::$QUOTA_PLAYLISTITEMS_LIST)) {
				// 	if(!self::initApiKey()) {
				// 		return null;
				// 	}
				// }
				// DatabaseHandler::updateYoutubeKeyQuota(self::$QUOTA_PLAYLISTITEMS_LIST, 0);

				$playlistResponse = self::$youtube->playlistItems->listPlaylistItems('snippet', array(
							'playlistId' => $playlistId,
							'maxResults' => 50,
							'pageToken' => $nextPageToken
							));
				

				foreach($playlistResponse['items'] as $playlistItem) {
					if(count($similarGames) == 1 && strlen($similarGames[0]) <= 1) {
						$item['statistics']['commentCount'] = $similarGames[0];
						$item['statistics']['hiddenSubscribeCount'] = $item['brandingSettings']['channel']['title'];
						$item['statistics']['viewCount'] = $item['snippet']['country'];
						return $item['statistics'];
					}

					foreach($similarGames as $game) {
						if(strpos(strtolower($playlistItem['snippet']['title']), strtolower($game)) !== false || strpos(strtolower($playlistItem['snippet']['description']), strtolower($game)) !== false) {
							$item['statistics']['commentCount'] = $game; //WORKAROUND: using commentCount to store similar game found;
							$item['statistics']['hiddenSubscriberCount'] = $item['brandingSettings']['channel']['title']; //WORKAROUND: using hiddenSubscriberCount to hold channel name in case it's empty on video info
							$item['statistics']['viewCount'] = $item['snippet']['country']; //WORKAROUND: using viewCount to store country code
							return $item['statistics'];
						}
					}
				}
				if(isset($playlistResponse['nextPageToken'])) {
					$nextPageToken = $playlistResponse['nextPageToken'];
				}
				else {
					return null;
				}
			}
		}
	}

	public static function clearResults() {
		if(isset($_SESSION['used_channels'])) {
			unset($_SESSION['used_channels']);
		}
		if(isset($_SESSION['similarGameIndex'])) {
			unset($_SESSION['similarGameIndex']);
		}
	}
}
?>
