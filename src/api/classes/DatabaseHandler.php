<?php
	require_once '../api/classes/config.php';

	class DatabaseHandler {
		private static $pdo;
		private static $MAX_QUOTA = 10000;
		private static $KEYS_COUNT = 0;
		private static $CUR_KEYS_COUNT = 0;

		public static function connect() {
			if(is_null(DatabaseHandler::$pdo)) {
				$host = DB_HOST;
				$db   = DB_NAME;
				$user = DB_USER;
				$pass = DB_PASSWORD;
				$charset = 'utf8mb4';

				$opt = [
						PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
						PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
						PDO::ATTR_EMULATE_PREPARES   => false,
						PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci"
						];

				$dsn = "mysql:host=".$host.";dbname=".$db.";charset=".$charset;
				self::$pdo = new PDO($dsn, DB_USER, DB_PASSWORD, $opt);
				
				self::$pdo->exec("set names utf8mb4");
			}
		}

		public static function disconnect() {
			self::$conn = null;
		}
			
		public static function getProjectData($projectId) {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM projects WHERE project_id = ?');
			$stmt->execute([$projectId]);

			$row = $stmt->fetch();

			$result = new stdClass();
			$result->status = ResponseCode::OK;
			$result->data = $row;
			return $result;
		}

		//Dashboard methods - START ----------------------------
		public static function getProjects() {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM projects');
			$stmt->execute();
			return $stmt->fetchAll();
		}

		public static function getWishlistsOrPurchaseBalance($projectId, $fromDate, $toDate) {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM project_purchase_stats WHERE project_id = ? AND stats_date BETWEEN ? AND ? ORDER BY stats_date ASC');
			$stmt->execute([$projectId, $fromDate, $toDate]);
			return $stmt->fetchAll();
		}

		public static function getTwitchStatsDataId($projectId, $date) {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT twitch_stats_id FROM project_twitch_stats WHERE project_id = ? AND twitch_stats_date = ?');
			$stmt->execute([$projectId, $date]);
			return $stmt->fetchAll(PDO::FETCH_COLUMN);
		}

		public static function insertTwitchViewsData($projectId, $dates, $views) {
			//add or update twitch views
			self::connect();
			$i = 0;
			foreach($dates as $d) {
				$processedDate = strtotime($d);
				$processedDate = date('Y-m-d', $processedDate);

				$result = self::getTwitchStatsDataId($projectId, $processedDate);
				
				if(count($result) == 0) {
					self::insertTwitchViews($projectId, $processedDate, $views[$i]);
				}
				else {
					self::updateTwitchViews($result[0], $views[$i]);
				}
				$i++;
			}
			
			$insertMultipleResult = new stdClass();
			$insertMultipleResult->status = ResponseCode::OK;
			return $insertMultipleResult;
		}
		
		public static function insertTwitchViews($project_id, $date, $views) {
			self::connect();
			$stmt = self::$pdo->prepare('INSERT INTO project_twitch_stats (project_id, twitch_stats_date, twitch_stats_views) VALUES (?,?,?)');
			
			try {
				$stmt->execute([$project_id, $date, $views]);
				
				$stmt = self::$pdo->prepare("SELECT * FROM project_twitch_stats WHERE twitch_stats_id = ?");
				$stmt->execute([self::$pdo->lastInsertId()]);
				$row = $stmt->fetch();
				
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				$result->data = $row;
				return $result;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function updateTwitchViews($twitch_stats_id, $views) {
			self::connect();

			$stmt = self::$pdo->prepare('UPDATE project_twitch_stats SET twitch_stats_views = ? WHERE twitch_stats_id = ?');
			
			try {
				$stmt->execute([$views, $twitch_stats_id]);
				return ResponseCode::OK;
			}
			catch(Exception $e) {
				error_log($e);
				return $e->getMessage();
			}
		}

		//Dashboard methods - END ----------------------------

		//Videos
		public static function getVideosIdsById($ids) {
			self::connect();
			$result = array();
			$stmt = self::$pdo->prepare("SELECT video_id FROM video_review_status_view WHERE video_id = ?");
			foreach($ids as $id) {
				$stmt->execute([$id]);
				$fetchResult = $stmt->fetch();
				if($fetchResult) {
					array_push($result, $fetchResult);
				}
			}
			return $result;
		}
		
		public static function getNonExistentVideosIds($ids) {
			self::connect();
			$result = array();
			$stmt = self::$pdo->prepare("SELECT video_id FROM video_review_status_view WHERE video_id = ?");
			foreach($ids as $id) {
				$stmt->execute([$id]);
				$fetchResult = $stmt->fetch();
				if($fetchResult) {
					//array_push($result, $fetchResult);
				}
				else {
					array_push($result, $id);
				}
			}
			return $result;
		}


		public static function getOldValidIds($ids) {
			self::connect();
			$result = array();
			$stmt = self::$pdo->prepare("SELECT video_id FROM video_review_status_view WHERE video_id = ? and valid = TRUE");
			foreach($ids as $id) {
				$stmt->execute([$id]);
				$fetchResult = $stmt->fetch();
				if($fetchResult) {
					array_push($result, $id);
				}
				else {
					
				}
			}
			return $result;
		}

		public static function getVideosFoundToday() {
			self::connect();
			$today = date("2020-12-21");

			$stmt = self::$pdo->prepare("SELECT projects.project_name, video_review_status_view.video_id, video_review_status_view.video_title, video_review_status_view.video_desc, video_review_status_view.published_at, video_review_status_view.thumbnail FROM video_review_status_view, videos, projects WHERE video_review_status_view.project_id = projects.project_id AND video_review_status_view.video_id = videos.video_id AND video_review_status_view.published_at = :todayDate ORDER BY video_review_status_view.project_id");
			$stmt->execute(['todayDate' => $today]);
			return $stmt->fetchAll();
		}

		public static function getVideos($fromDate, $toDate) {
			self::connect();
			$stmt = self::$pdo->prepare("SELECT * FROM videos WHERE published_at BETWEEN :fromDate AND :toDate ORDER BY published_at");
			$stmt->execute(['fromDate' => $fromDate, 'toDate' => $toDate]);
			return $stmt->fetchAll();
		}

		public static function insertVideos($projectId, $data) {
			self::connect();
			$i = 0;
			try {
				$stmt = self::$pdo->prepare('INSERT INTO videos (video_id,video_title,video_desc,published_at,thumbnail) VALUES (?,?,?,?,?)');
				foreach($data as $v) {
					if($v) {
						$stmt->execute([$v->id, $v->title, mb_substr($v->description, 0, 255), $v->publishedAt, $v->thumbnail]);
						$i++;
						self::insertVideoStatus($projectId, $v->id);
						if($v->tags != NULL) {
							self::insertTags($v->id,$v->tags);
						}
					}
				}
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				$result->data = $i;
				return $result;
			}
			catch(Exception $e) {
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function insertVideoStatus($projectId, $videoId) {
			self::connect();
			try {
				$stmt = self::$pdo->prepare('INSERT INTO video_review_status (project_id, video_id) VALUES (?,?)');
				$stmt->execute([$projectId, $videoId]);
				return 1;
			}
			catch(Exception $e) {
				return $e->getMessage();
			}
		}

		public static function updateVideos($data) {
			self::connect();
			$i = 0;		
			try {	
				$stmt = self::$pdo->prepare('UPDATE video_review_status_view SET video_title=?, video_desc=? WHERE video_id=?');
				foreach($data as $v) {
					if($v) {
						$stmt->execute([$v->title, mb_substr($v->description, 0, 255), $v->id]);
						$i++;
					}
				}
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				$result->data = $i;
				return $result;
			}
			catch(Exception $e) {
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				$result->data = $e->getMessage();
				return $result;
			}
		}
		
		public static function getVideosForReview($projectId, $fromDate, $toDate, $reviewed, $sorting) {
			self::connect();
			$stmt = self::$pdo->prepare("SELECT * FROM video_review_status_view WHERE project_id = :projectId AND reviewed = :reviewed AND published_at BETWEEN :fromDate AND :toDate ORDER BY " . $sorting);
			$stmt->bindValue(':projectId', $projectId);
			$stmt->bindValue(':fromDate', $fromDate);
			$stmt->bindValue(':toDate', $toDate);
			$stmt->bindValue(':reviewed', $reviewed);
			$stmt->execute();
			
			return $stmt->fetchAll();
		}
		
		public static function setVideoValid($projectId, $videoId, $valid) {
			self::connect();
			$stmt = self::$pdo->prepare('UPDATE video_review_status_view SET valid = ? WHERE video_id = ? AND project_id = ?');
			$stmt->execute([$valid, $videoId, $projectId]);
		}

		public static function setVideoReviewed($projectId, $videoId, $reviewed) {
			self::connect();
			$stmt = self::$pdo->prepare('UPDATE video_review_status_view SET reviewed = ? WHERE video_id = ? AND project_id = ?');
			$stmt->execute([$reviewed, $videoId, $projectId]);
		}

		public static function insertVideoStats($projectId, $videosData, $newVideo) {
			self::connect();
			$today = date("Y-m-d");
			$i = 0;			
			try {
				//if the video doesn't exist, insert it 
				if($newVideo) {
					$stmt = self::$pdo->prepare('INSERT video_stats (project_id, video_id, date, like_counting, dislike_counting, favorite_counting, comment_counting, view_counting) VALUES (?, ?,?,?,?,?,?,?)');
					foreach($videosData as $v) {
						if($v) {
							$stmt->execute([$projectId, $v->id, $v->publishedAt, $v->likeCount, $v->dislikeCount, $v->favoriteCount, $v->commentCount, $v->viewCount]);
							$i++;
						}
					}
					$result = new stdClass();
					$result->status = ResponseCode::OK;
					$result->data = $i;
					return $result;
				}
				else {
					foreach($videosData as $v) {
						$i++;
						//get the total views until now
						$stmt = self::$pdo->prepare('SELECT SUM(like_counting) AS like_counting, SUM(dislike_counting) AS dislike_counting, SUM(favorite_counting) AS favorite_counting, ' .
							'SUM(comment_counting) AS comment_counting, SUM(view_counting) AS view_counting FROM video_stats WHERE video_id=? AND project_id=?'); 
						$stmt->execute([$v->id, $projectId]);
						$videoTotals = $stmt->fetchAll();

						//get the last date to check if the video stats must be updated
						$stmt = self::$pdo->prepare('SELECT * FROM video_stats WHERE project_id = ? AND video_id = ? AND date = ? ORDER BY date DESC LIMIT 1');
						$stmt->execute([$projectId, $v->id, $today]);
						$videoStats = $stmt->fetchAll();

						//should update
						if(count($videoStats) > 0) {
							//calculate the diff
							$curStats = new stdClass();
							$curStats->like_counting = $v->likeCount - ($videoTotals[0]['like_counting'] - $videoStats[0]['like_counting']);
							$curStats->dislike_counting = $v->dislikeCount - ($videoTotals[0]['dislike_counting'] - $videoStats[0]['dislike_counting']);
							$curStats->favorite_counting = $v->favoriteCount - ($videoTotals[0]['favorite_counting'] - $videoStats[0]['favorite_counting']);
							$curStats->comment_counting = $v->commentCount - ($videoTotals[0]['comment_counting'] - $videoStats[0]['comment_counting']);
							$curStats->view_counting = $v->viewCount - ($videoTotals[0]['view_counting'] - $videoStats[0]['view_counting']);

							$stmt = self::$pdo->prepare('UPDATE video_stats SET like_counting=?, dislike_counting=?, favorite_counting=?, comment_counting=?, view_counting=? WHERE project_id = ? AND video_id=? AND date = ?');
							$stmt->execute([$curStats->like_counting, $curStats->dislike_counting, $curStats->favorite_counting,
								$curStats->comment_counting, $curStats->view_counting,
								$projectId, $v->id, $today]);
						}
						else { //should insert new
							//calculate the diff
							$curStats = new stdClass();
							$curStats->like_counting = $v->likeCount - $videoTotals[0]['like_counting'];
							$curStats->dislike_counting = $v->dislikeCount - $videoTotals[0]['dislike_counting'];
							$curStats->favorite_counting = $v->favoriteCount - $videoTotals[0]['favorite_counting'];
							$curStats->comment_counting = $v->commentCount - $videoTotals[0]['comment_counting'];
							$curStats->view_counting = $v->viewCount - $videoTotals[0]['view_counting'];

							//we can save a lot of database space if we simply don't insert nothing if anything changed =]
							if($curStats->like_counting != 0 || $curStats->dislike_counting != 0 || $curStats->favorite_counting != 0 ||
								$curStats->comment_counting != 0 || $curStats->view_counting != 0) {
									$stmt = self::$pdo->prepare('INSERT video_stats (project_id, video_id, date, like_counting, dislike_counting, favorite_counting, comment_counting, view_counting) VALUES (?, ?,?,?,?,?,?,?)');
									$stmt->execute([$projectId, $v->id, $today, $curStats->like_counting, $curStats->dislike_counting,
										$curStats->favorite_counting, $curStats->comment_counting, $curStats->view_counting]);
							}
						}
					}
					$result = new stdClass();
					$result->status = ResponseCode::OK;
					$result->data = "Success";
					return $result;
				}
			}
			catch(Exception $e) {
				error_log($e->getMessage());
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function insertTags($video_id,$video_tags) {
			self::connect();
			
			foreach($video_tags as $tag) {
    			if($tag) {
					$stmt = self::$pdo->prepare("SELECT tag_name FROM tags WHERE tag_name = ?");
					$stmt->execute([$tag]);
					if(!$stmt->fetch()){
						
						// insert new tag
						$stmt = self::$pdo->prepare('INSERT INTO tags VALUES (DEFAULT,?)');
						$stmt->execute([$tag]);
						
						$tag_id = self::$pdo->lastInsertId();

						$stmt = self::$pdo->prepare('INSERT INTO videos_tags (tag_id,video_id) VALUES (?,?)');
						$stmt->execute([$tag_id, $video_id]);
					}
					else{
						//new tag is autoincremented so is necessary to get the new generated tag_id
						$stmt = self::$pdo->prepare("SELECT tag_id FROM tags WHERE tag_name = ?");
						$stmt->execute([$tag]);
						$tag_id = $stmt->fetch(PDO::FETCH_COLUMN);

						$stmt = self::$pdo->prepare('INSERT INTO videos_tags (tag_id,video_id) VALUES (?,?)');
						$stmt->execute([$tag_id, $video_id]);
					}
				}
				else{
					
					printf('tag is null');
				
				}
			}
		}
	
		public static function getViewsDataForChart($projectId, $fromDate, $toDate) {
			$data = self::getDailyVideosStats($projectId, $fromDate, $toDate);
			return self::processVideoStatsForChart($data, $fromDate, $toDate);
		}

		public static function getDailyVideosStats($projectId, $fromDate, $toDate) {
			self::connect();
			$stmt = self::$pdo->prepare(
				'SELECT video_stats.video_id, video_stats.date, video_stats.view_counting ' .
				'FROM video_stats, video_review_status ' .
				'WHERE video_stats.project_id = ? ' .
				'AND video_stats.video_id = video_review_status.video_id AND video_review_status.valid = 1 ' .
				'AND video_stats.date BETWEEN ? AND ? ORDER BY video_stats.date ASC'
			);
			$stmt->execute([$projectId, $fromDate, $toDate]);
			return $stmt->fetchAll();
		}

		public static function processVideoStatsForChart($data, $fromDate, $toDate) {
			$viewsPerDay = array();
			$rowsData = array();

			//init the dates from the range. Wee need all the dates from the range, even if there are no views, to correctly plot the chart
			$date = $fromDate;
			while (strtotime($date) <= strtotime($toDate)) {
				$rowData = new stdClass();
				$rowData->date = $date;
				$rowData->views = 0;
				$rowsData[$date] = $rowData;
				$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
			}

			if(count($data) > 0) {
				foreach($data as $row) {
					$rowsData[$row['date']]->views += $row['view_counting'];
				}
			}

			foreach($rowsData as $r) {
				array_push($viewsPerDay, $r);
			}
			return $viewsPerDay;
		}

		public static function getTwitchViewsStats($projectId, $fromDate, $toDate) {
			self::connect();

			$stmt = self::$pdo->prepare('SELECT * FROM project_twitch_stats WHERE project_id = ? AND twitch_stats_date BETWEEN ? AND ? ORDER BY twitch_stats_date ASC');
			$stmt->execute([$projectId, $fromDate, $toDate]);
			return $stmt->fetchAll();
		}

		public static function getVideosPerDayForChart($projectId, $fromDate, $toDate) {
			self::connect();
			$videosPerDay = array();
			$rowsData = array();

			$date = $fromDate;
			$stmt = self::$pdo->prepare('SELECT COUNT(*) FROM videos, video_review_status WHERE video_review_status.video_id = videos.video_id AND video_review_status.project_id = ? AND videos.published_at = ? AND video_review_status.valid = 1');
			while (strtotime($date) <= strtotime($toDate)) {
				$stmt->execute([$projectId, $date]);
				$result = $stmt->fetch(PDO::FETCH_COLUMN);
				$rowData = new stdClass();
				$rowData->date = $date;
				$rowData->videos = $result;
				$rowsData[$date] = $rowData;
				$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
			}

			foreach($rowsData as $r) {
				array_push($videosPerDay, $r);
			}
			return $videosPerDay;
		}

		/*-----------------------------------------------------------------------------*/	
		 # ------------------------ Project stats events -------------------------------
		/*-----------------------------------------------------------------------------*/
		public static function insertEvent($projectId, $date, $name, $desc) {
			self::connect();

			try {
				$stmt = self::$pdo->prepare('INSERT INTO project_stats_event (project_id, project_stats_event_date, project_stats_event_name, project_stats_event_description) VALUES (?,?,?,?)');
				$stmt->execute([$projectId, $date, $name, $desc]);

				$stmt = self::$pdo->prepare("SELECT * FROM project_stats_event WHERE project_stats_event_id = ?");
				$stmt->execute([self::$pdo->lastInsertId()]);
				$row = $stmt->fetch();
				
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				$result->data = $row;
				return $result;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}

		}

		public static function updateEvent($eventId, $date, $name, $desc) {
			self::connect();

			$fieldsString = "";
			$fields = array();

			if($date != null) {
				$fieldsString = $fieldsString . 'project_stats_event_date = ?,';
				array_push($fields, $date);
			} 

			if($name != null) {
				$fieldsString = $fieldsString . 'project_stats_event_name = ?,';
				array_push($fields, $name);
			} 

			if($desc != null) {
				$fieldsString = $fieldsString . 'project_stats_event_description = ?,';
				array_push($fields, $desc);
			} 
			array_push($fields, $eventId);
			$fieldsString = substr($fieldsString, 0, -1);
			
			$stmt = self::$pdo->prepare('UPDATE project_stats_event SET ' . $fieldsString . ' WHERE project_stats_event_id = ?');
			
			try {
				$stmt->execute($fields);
				return ResponseCode::OK;
			}
			catch(Exception $e) {
				error_log($e);
				return $e->getMessage();
			}
		 }

		 public static function deleteEvent($eventId) {
			 self::connect();
			 $stmt = self::$pdo->prepare('DELETE FROM project_stats_event WHERE project_stats_event_id = ?');
			 try {
				$stmt->execute([$eventId]);
				$row = $stmt->fetch();
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				$result->data = $row;
				return $result;
			}
			catch(Exception $e) {
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		 }

		 public static function getEventsPerProjectId($projectId, $fromDate, $toDate, $sorting) {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM project_stats_event WHERE project_id = ? AND project_stats_event_date BETWEEN ? AND ? ORDER BY ' . $sorting);
			$stmt->execute([$projectId, $fromDate, $toDate]);
			return $stmt->fetchAll();
		}

		/*-----------------------------------------------------------------------------*/	
		 # ---------------------- Wishlists and purchases ------------------------------
		/*-----------------------------------------------------------------------------*/
		public static function getWishlistOrPurchaseId($projectId, $statsType, $date) {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT stats_id FROM project_purchase_stats WHERE project_id = ? AND stats_date = ? AND stats_type = ? ');
			$stmt->execute([$projectId, $date, $statsType]);
			return $stmt->fetchAll(PDO::FETCH_COLUMN);
		}

		public static function hasWishlistOrPurchase($projectId, $statsType, $date) {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM project_purchase_stats WHERE project_id = ? AND stats_date = ? AND stats_type = ? ');
			$stmt->execute([$projectId, $date, $statsType]);
			$stmt->fetch();
			return $stmt->rowCount() > 0;
		}

		public static function getWishlistsOrPurchases($project_id, $sorting) {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM project_purchase_stats WHERE project_id = ? ORDER BY ' . $sorting);
			
			try {
				$stmt->execute([$project_id]);
				return $stmt->fetchAll();
			}
			catch(Exception $e) {
				error_log($e);
				return ResponseCode::INTERNAL_SERVER_ERROR;
			}
		 }
		 
		public static function insertWishlistOrPurchase($project_id, $date, $type, $additions, $deletions, $activations, $gifts) {
			self::connect();
			$stmt = self::$pdo->prepare('INSERT INTO project_purchase_stats (project_id, stats_date,stats_type,stats_additions,stats_deletions,stats_activations,stats_gifts) VALUES (?,?,?,?,?,?,?)');
			
			try {
				$stmt->execute([$project_id, $date, $type, $additions, $deletions, $activations, $gifts]);
				
				$stmt = self::$pdo->prepare("SELECT * FROM project_purchase_stats WHERE stats_id = ?");
				$stmt->execute([self::$pdo->lastInsertId()]);
				$row = $stmt->fetch();
				
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				$result->data = $row;
				return $result;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function insertMultiplePurchases($projectId, $purchaseData) {
			$statsType = 1;

			foreach($purchaseData as $purchase) {
				$d = date("Y-m-d", ($purchase[0] / 1000));
				$result = self::getWishlistOrPurchaseId($projectId, $statsType, $d);
				$p = $purchase[1];

				if(empty($p)) {
					$p = 0;
				}

				if(count($result) == 0) {
					self::insertWishlistOrPurchase($projectId, $d, $statsType, $p ?? 0, 0, 0, 0);
				}
				else {
					self::updateWishlistOrPurchase($result[0], $d, $statsType, $p ?? 0, 0, 0, 0);
				}
			}

			$insertMultipleResult = new stdClass();
			$insertMultipleResult->status = ResponseCode::OK;
			return $insertMultipleResult;
		}

		public static function insertMultipleWishlistOrPurchase($projectId, $statsType, $addData, $delData, $actvData, $giftData) {
			//create objects for each available date
			$newEntries = array();
			$dates = array();
			$additions = array();
			$deletions = array();
			$activations = array();
			$gifts = array();

			//merge dates
			//additions
			if(count($addData) > 0) {			
				foreach($addData as $add) {
					$date = $add[0];
					$additions[$date] = $add[1];
					if(!array_key_exists($date, $dates)) {
						array_push($dates, $date);
					}
				}
			}

			//deletions
			if(count($delData) > 0) {
				foreach($delData as $del) {
					$date = $del[0];
					$deletions[$date] = $del[1];
					if(!array_key_exists($date, $dates)) {
						array_push($dates, $date);
					}
				}
			}

			//activations
			if(count($actvData) > 0) {
				foreach($actvData as $actv) {
					$date = $actv[0];
					$activations[$date] = $actv[1];
					if(!array_key_exists($date, $dates)) {
						array_push($dates, $date);
					}
				}
			}

			//gifts
			if(count($giftData) > 0) {
				foreach($giftData as $g) {
					$date = $g[0];
					$gifts[$date] = $g[1];
					if(!array_key_exists($date, $dates)) {
						array_push($dates, $date);
					}
				}
			}

			//add or update wishlists
			// self::connect();
			foreach($dates as $d) {
				$result = self::getWishlistOrPurchaseId($projectId, $statsType, $d);
				
				if(count($result) == 0) {
					self::insertWishlistOrPurchase($projectId, $d, $statsType, $additions[$d] ?? 0, $deletions[$d] ?? 0, $activations[$d] ?? 0, $gifts[$d] ?? 0);
				}
				else {
					self::updateWishlistOrPurchase($result[0], $d, $statsType, $additions[$d] ?? 0, $deletions[$d] ?? 0, $activations[$d] ?? 0, $gifts[$d] ?? 0);
				}
			}
			
			$insertMultipleResult = new stdClass();
			$insertMultipleResult->status = ResponseCode::OK;
			return $insertMultipleResult;
		}
		 
		public static function updateWishlistOrPurchase($stats_id, $date, $type, $additions, $deletions, $activations, $gifts) {
			self::connect();

			$fieldsString = "";
			$fields = array();

			if($date != null) {
				$fieldsString = $fieldsString . 'stats_date = ?,';
				array_push($fields, $date);
			} 

			if($type != null) {
				$fieldsString = $fieldsString . 'stats_type = ?,';
				array_push($fields, $type);
			} 

			if($additions != null) {
				$fieldsString = $fieldsString . 'stats_additions = ?,';
				array_push($fields, $additions);
			} 

			if($deletions != null) {
				$fieldsString = $fieldsString . 'stats_deletions = ?,';
				array_push($fields, $deletions);
			}

			if($activations != null) {
				$fieldsString = $fieldsString . 'stats_activations = ?,';
				array_push($fields, $activations);
			} 

			if($gifts != null) {
				$fieldsString = $fieldsString . 'stats_gifts = ?,';
				array_push($fields, $gifts);
			} 
			array_push($fields, $stats_id);
			$fieldsString = substr($fieldsString, 0, -1);
			
			$stmt = self::$pdo->prepare('UPDATE project_purchase_stats SET ' . $fieldsString . ' WHERE stats_id = ?');
			
			try {
				$stmt->execute($fields);
				return ResponseCode::OK;
			}
			catch(Exception $e) {
				error_log($e);
				return $e->getMessage();
			}
		}
		 
		public static function deleteWishlistOrPurchase($stats_id) {
			self::connect();
			$stmt = self::$pdo->prepare('DELETE FROM project_purchase_stats WHERE stats_id = ?');
			
			try {
				$stmt->execute([$stats_id]);
				return ResponseCode::OK;
			}
			catch(Exception $e) {
				error_log($e);
				return ResponseCode::INTERNAL_SERVER_ERROR;
			}
		}

		public static function getPurchaseStatsType() {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM game_stats_type');
			
			try {
				$stmt->execute();
				return $stmt->fetchAll();
			}
			catch(Exception $e) {
				error_log($e);
				return ResponseCode::INTERNAL_SERVER_ERROR;
			}
		}

		/*-----------------------------------------------------------------------------*/	
		 # -------------------------- Queries methods ---------------------------------
		/*-----------------------------------------------------------------------------*/
		public static function insertQuery($projectId, $queryTerms) {
			self::connect();
			try {
				$stmt = self::$pdo->prepare('INSERT INTO queries (project_id,query_date,query_terms) VALUES (?,UTC_TIMESTAMP(),?)');
				$stmt->execute([$projectId, $queryTerms]);

				$stmt = self::$pdo->prepare("SELECT * FROM queries WHERE query_id = ?");
				$stmt->execute([self::$pdo->lastInsertId()]);
				$row = $stmt->fetch();
					
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				$result->data = $row;
				return $result;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function updateQuery($projectId, $queryId, $searchDate) {
			self::connect();
			try {
				$stmt = self::$pdo->prepare('UPDATE queries SET initial_search_date=?, query_date=UTC_TIMESTAMP() WHERE query_id=?');
				$stmt->execute([$searchDate, $queryId]);

				$result = new stdClass();
				$result->status = ResponseCode::OK;
				$result->data = "Success";
				return $result;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function deleteQuery($queryId) {
			self::connect();
			 $stmt = self::$pdo->prepare('DELETE FROM queries WHERE query_id = ?');
			 try {
				$stmt->execute([$queryId]);
				$row = $stmt->fetch();
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				$result->data = $row;
				return $result;
			}
			catch(Exception $e) {
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function getMostRecentQuery($projectId) {
			self::connect();
			try {
				$stmt = self::$pdo->prepare('SELECT * FROM queries WHERE project_id=? ORDER BY query_date DESC LIMIT 1');
				$stmt->execute([$projectId]);
				$rows = $stmt->fetchAll();

				$result = new stdClass();
				$result->status = ResponseCode::OK;
				$result->data = $rows;
				return $result;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function getQueries($projectId, $sorting) {
			self::connect();
			try {
				$stmt = self::$pdo->prepare('SELECT * FROM queries WHERE project_id=? ORDER BY ' . $sorting);
				$stmt->execute([$projectId]);
				$rows = $stmt->fetchAll();

				$result = new stdClass();
				$result->status = ResponseCode::OK;
				$result->data = $rows;
				return $result;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}


######################################User Matheods############################################################


		public static function getUser($user){
			self::connect();

			try{
				$stmt = self::$pdo->prepare('SELECT * FROM  users WHERE user_login = ?');
				$stmt->execute([$user]);
				$row = $stmt->fetch();
				
				return $row;

			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function insertUser($username, $userpass, $usermail) {
			self::connect();
			try {
				
				$stmt = self::$pdo->prepare('INSERT INTO users (user_id,user_login,user_password, user_email) VALUES (DEFAULT,?,?,?)');
				$stmt->execute([$username, $userpass, $usermail]);
	
					
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				$result->data = $row;
				return $result;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}
		
		//Youtube api keys ========================================================================
		public static function getApiKey($key_id){
			self::connect();

			try{
				$stmt = self::$pdo->prepare('SELECT * FROM  youtube_api_keys WHERE key_id = ?');
				$stmt->execute([$key_id]);
				$row = $stmt->fetch();
				
				return $row;

			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage() ;
				return $result;
			}
		}

		public static function updateApiKey($key_id) {
			self::connect();
			
			$stmt = self::$pdo->prepare('UPDATE youtube_api_keys SET last_usage_date = ? WHERE key_id = ?');
			
			try {
				$stmt->execute([date('Y-m-d'), $key_id]);
				return ResponseCode::OK;
			}
			catch(Exception $e) {
				error_log($e);
				return $e->getMessage();
			}
		}

		public static function getCurrentYoutubeApiKey() {
			self::connect();

			try{
				$stmt = self::$pdo->prepare('SELECT * FROM  current_youtube_api_key');
				$stmt->execute();
				$row = $stmt->fetch();
				
				return $row;

			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage() ;
				return $result;
			}
		}

		public static function getYoutubeApiKey($id) {
			self::connect();

			try{
				$stmt = self::$pdo->prepare('SELECT * FROM  youtube_api_keys WHERE key_id = ?');
				$stmt->execute([$id]);
				$row = $stmt->fetch();
				
				return $row;

			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage() ;
				return $result;
			}
		}

		public static function getOldestApiKey() {
			self::connect();

			try{
				$stmt = self::$pdo->prepare('SELECT * FROM  youtube_api_keys ORDER BY last_usage_date ASC');
				$stmt->execute();
				$row = $stmt->fetch();
				
				return $row;

			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage() ;
				return $result;
			}
		}

		public static function getYoutubeApiKeysCount() {
			self::connect();

			try{
				$stmt = self::$pdo->prepare('SELECT COUNT(*) FROM  youtube_api_keys');
				$stmt->execute();
				$row = $stmt->fetch();
				
				return $row;

			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage() ;
				return $result;
			}
		}

		public static function getNextYoutubeApiKey($required_quota) {
			self::connect();
			self::$KEYS_COUNT = self::getYoutubeApiKeysCount()['COUNT(*)'];

			try {
				$key_id = DatabaseHandler::getCurrentYoutubeApiKey()['key_id'];
				$next_key = self::getApiKey($key_id + 1);

				//if id is too big
				if($next_key == false) {
					//reset it back to 1
					self::updateCurrentYoutubeApiKey($key_id, 1);
					$next_key = self::getApiKey(1);
				}

				//if the next key is from yesterday, we're cool to use it
				if(strtotime($next_key['last_usage_date']) < strtotime(date('Y-m-d'))) {
					//Update the current key and reset its quota
					self::updateCurrentYoutubeApiKey($key_id, $next_key['key_id']);
					$next_key = self::resetYoutubeKeyQuota();
				}
				else {
					//the new key has enough quota?
					if($next_key['quota_usage'] + $required_quota >= self::$MAX_QUOTA) {
						self::$CUR_KEYS_COUNT++;

						if(self::$CUR_KEYS_COUNT < self::$KEYS_COUNT) {
							self::updateCurrentYoutubeApiKey($key_id, $next_key['key_id']);
							return self::getNextYoutubeApiKey($required_quota);
						}
						else
						{
							self::$CUR_KEYS_COUNT = 0;
							return NULL;
						}
					}
					else
					{
						self::updateCurrentYoutubeApiKey($key_id, $next_key['key_id']);
					}
				}
				return $next_key;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage() ;
				return $result;
			}
		}

		public static function updateCurrentYoutubeApiKey($cur_key_id, $new_key_id) {
			self::connect();

			try {
				$stmt = self::$pdo->prepare('UPDATE current_youtube_api_key SET key_id = ? WHERE key_id = ?');
				$stmt->execute([$new_key_id, $cur_key_id]);
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage() ;
				return $result;
			}
		}

		public static function hasQuotaCustomLimit($required_quota, $limit) {
			self::connect();

			try{
				$current_key_id = 1;
				$current_key_data = DatabaseHandler::getApiKey($current_key_id);

				$result = new stdClass();
				$cur_quota = self::$MAX_QUOTA - $current_key_data['quota_usage'];

				if($required_quota < $cur_quota) {
					$result->status = ResponseCode::OK;
				}
				else {
					$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				}
				$result->data = ($current_key_data['quota_usage']) + $required_quota < $limit;
				return $result;
			}
			catch(Exception $e) {
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage() ;
				return $result;
			}
		}

		public static function hasQuota($required_quota) {
			self::connect();

			try{
				$current_key_id = 1;
				$current_key_data = DatabaseHandler::getApiKey($current_key_id);

				return $current_key_data['quota_usage'] + $required_quota < self::$MAX_QUOTA;
			}
			catch(Exception $e) {
				return false;
			}
		}

		public static function hasQuotaByKeyId($key_id, $required_quota) {
			self::connect();

			try{
				$current_key_id = $key_id;
				$current_key_data = DatabaseHandler::getApiKey($current_key_id);

				return $current_key_data['quota_usage'] + $required_quota < self::$MAX_QUOTA;
			}
			catch(Exception $e) {
				return false;
			}
		}

		public static function updateYoutubeKeyQuota($extra_quota, $required_quota) {
			self::connect();

			try{
				$current_key_id = 1;
				$current_key_data = DatabaseHandler::getApiKey($current_key_id);

				$new_quota = $current_key_data['quota_usage'] + $extra_quota;

				$stmt = self::$pdo->prepare('UPDATE youtube_api_keys SET quota_usage = ?, last_usage_date = ? WHERE key_id = ?');
				$stmt->execute([$new_quota, date('Y-m-d'), $current_key_id]);
				
				//If there's no more quota for a full operation, return false, so we know we need to move to the next api key
				if($new_quota + $required_quota >= self::$MAX_QUOTA) {
					return false;
				}

				return true;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage() ;
				return $result;
			}
		}

		public static function updateYoutubeKeyQuotaByKey($key_id, $extra_quota, $required_quota) {
			self::connect();

			try{
				$current_key_id = $key_id;
				$current_key_data = DatabaseHandler::getApiKey($current_key_id);

				$new_quota = $current_key_data['quota_usage'] + $extra_quota;

				$stmt = self::$pdo->prepare('UPDATE youtube_api_keys SET quota_usage = ?, last_usage_date = ? WHERE key_id = ?');
				$stmt->execute([$new_quota, date('Y-m-d'), $current_key_id]);
				
				//If there's no more quota for a full operation, return false, so we know we need to move to the next api key
				if($new_quota + $required_quota >= self::$MAX_QUOTA) {
					return false;
				}

				return true;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage() ;
				return $result;
			}
		}

		public static function resetYoutubeKeyQuota() {
			self::connect();

			try{
				$current_key_id = 1;
				$current_key_data = DatabaseHandler::getApiKey($current_key_id);

				$stmt = self::$pdo->prepare('UPDATE youtube_api_keys SET quota_usage = ?, last_usage_date = ? WHERE key_id = ?');
				$stmt->execute([0, date('Y-m-d'), $current_key_id]);
				return DatabaseHandler::getApiKey($current_key_id);
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage() ;
				return $result;
			}
		}

		public static function resetYoutubeKeyQuotaById($key_id) {
			self::connect();

			try{
				$current_key_id = $key_id;
				$current_key_data = DatabaseHandler::getApiKey($current_key_id);

				$stmt = self::$pdo->prepare('UPDATE youtube_api_keys SET quota_usage = ?, last_usage_date = ? WHERE key_id = ?');
				$stmt->execute([0, date('Y-m-d'), $current_key_id]);
				return DatabaseHandler::getApiKey($current_key_id);
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage() ;
				return $result;
			}
		}
		//Youtube api keys - END ====================================================================

		//Channels search - BEGIN =========================================
		public static function insertOrUpdateSearch($project_id, $video_age) {
			self::connect();
			try {
				
				$stmt = self::$pdo->prepare('INSERT INTO search (project_id, video_age, current_sim_game) VALUES (?,?,?) ON DUPLICATE KEY UPDATE video_age = ?');
				$stmt->execute([$project_id, $video_age, '', $video_age]);
					
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				$result->data = $row;
				return $result;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function getSearch($project_id) {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM search WHERE project_id = ?');

			try
			{
				$stmt->execute([$project_id]);
				return $stmt->fetch();
			}
			catch(Exception $e) {
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function getCurrentSearchSimilarGame($project_id) { 
			self::connect();
			$stmt = self::$pdo->prepare('SELECT current_sim_game FROM search WHERE project_id = ?');

			try
			{
				$stmt->execute([$project_id]);
				return $stmt->fetch()['current_sim_game'];
			}
			catch(Exception $e) {
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function updateCurrentSearchSimilarGame($project_id) { 
			$cur_sim_game = self::getCurrentSearchSimilarGame($project_id);
			$similar_games = self::getSimilarGames($project_id);

			$index = 0;

			if(!empty($cur_sim_game)) {
				foreach($similar_games as $game) {
					if($game['name'] == $cur_sim_game) {
						break;
					}
					$index++;
				}
				unset($game);

				$index++;
				if($index >= count($similar_games)) {
					$index = 0;
				}
			}

			$next_game = $similar_games[$index]['name'];

			self::connect();
			try {
				$stmt = self::$pdo->prepare('UPDATE search SET current_sim_game = ? WHERE project_id = ?');
				$stmt->execute([$next_game, $project_id]);
					
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				return $result;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}

			return $next_game;
		}

		public static function insertOrUpdateSimilarGame($project_id, $game) {
			self::connect();
			try {
				
				$stmt = self::$pdo->prepare('INSERT INTO search_similar_game (project_id, name) VALUES (?,?) ON DUPLICATE KEY UPDATE project_id = ?');
				$stmt->execute([$project_id, $game, $project_id]);
					
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				//$result->data = $row;
				return $result;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function hasSimilarGame($projectId, $game) {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM search_similar_game WHERE project_id = ? AND name = ?');
			$stmt->execute([$projectId, $game]);
			$stmt->fetch();
			return $stmt->rowCount() > 0;
		}

		public static function getSimilarGames($projectId) {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM search_similar_game WHERE project_id = ?');
			$stmt->execute([$projectId]);
			return $stmt->fetchAll();
		}

		public static function removeSimilarGame($projectId, $game) {
			self::connect();
			$stmt = self::$pdo->prepare('DELETE FROM search_similar_game WHERE project_id = ? AND name = ?');
			$stmt->execute([$projectId, $game]);
			return $stmt->fetch();
		}

		public static function insertChannelDetails($project_id, $channel_id, $channel_name, $channel_country) {
			self::connect();
			try {
				
				$stmt = self::$pdo->prepare('INSERT INTO search_channel_details (project_id, id, name, country, valid) VALUES (?,?,?,?,?) ON DUPLICATE KEY UPDATE name = ?, country = ?');
				$stmt->execute([$project_id, $channel_id, $channel_name, $channel_country, 1, $channel_name, $channel_country]);
					
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				//$result->data = $row;
				return $result;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function updateChannelValid($project_id, $channel_id, $valid) {
			self::connect();
			try {
				$stmt = self::$pdo->prepare('UPDATE search_channel_details SET valid = ? WHERE project_id = ? AND id = ?');
				$stmt->execute([$valid, $project_id, $channel_id]);
					
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				return $result;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function updateChannelCountry($channel_id, $country) {
			self::connect();
			try {
				$stmt = self::$pdo->prepare('UPDATE search_channel_details SET country = ? WHERE id = ?');
				$stmt->execute([$country, $channel_id]);
					
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				return $result;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function hasChannelDetails($project_id, $channel_id) {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM search_channel_details WHERE project_id = ? AND id = ?');
			$stmt->execute([$project_id, $channel_id]);
			$stmt->fetch();
			return $stmt->rowCount() > 0;
		}
		
		public static function getChannelsForReview($project_id, $reviewed, $from_date, $to_date, $sorting) {
			self::connect();
			$stmt = self::$pdo->prepare("SELECT * FROM search_channel_details_evaluation WHERE project_id = :project_id AND reviewed = :reviewed AND sim_games_count > :sim_games_count AND date_found BETWEEN :from_date AND :to_date ORDER BY " . $sorting);
			$stmt->bindValue(':project_id', $project_id);
			$stmt->bindValue(':from_date', $from_date);
			$stmt->bindValue(':to_date', $to_date);
			$stmt->bindValue(':reviewed', $reviewed);
			$stmt->bindValue(':sim_games_count', 1);
			$stmt->execute();
			
			return $stmt->fetchAll();
		}

		public static function setChannelValid($projectId, $channelId, $valid) {
			self::connect();
			$stmt = self::$pdo->prepare('UPDATE search_channel_details SET valid = ? WHERE id = ? AND project_id = ?');
			$stmt->execute([$valid, $channelId, $projectId]);
		}

		public static function setChannelReviewed($projectId, $channelId, $reviewed) {
			self::connect();
			$stmt = self::$pdo->prepare('UPDATE search_channel_details SET reviewed = ? WHERE id = ? AND project_id = ?');
			$stmt->execute([$reviewed, $channelId, $projectId]);
		}

		public static function insertChannelSimilarGame($project_id, $channel_id, $similar_game) {
			self::connect();
			try {
				
				$stmt = self::$pdo->prepare('INSERT INTO search_channel_similar_game (project_id, channel_id, similar_game) VALUES (?,?,?) ON DUPLICATE KEY UPDATE project_id = ?');
				$stmt->execute([$project_id, $channel_id, $similar_game, $project_id]);
					
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				//$result->data = $row;
				return $result;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function hasChannelSimilarGame($project_id, $channel_id, $similar_game) {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM search_channel_similar_game WHERE project_id = ? AND channel_id = ? AND similar_game = ?');
			$stmt->execute([$project_id, $channel_id, $similar_game]);
			$stmt->fetch();
			return $stmt->rowCount() > 0;
		}

		public static function getChannelSimilarGames($project_id, $channel_id) {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM search_channel_similar_game WHERE project_id = ? AND channel_id = ?');
			$stmt->execute([$project_id, $channel_id]);
			return $stmt->fetchAll();
		}

		public static function getChannelSimilarGamesPerProjectId($project_id) {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM search_channel_similar_game WHERE project_id = ?');
			$stmt->execute([$project_id]);
			return $stmt->fetchAll();
		}

		public static function insertOrUpdateChannelEvaluation($channel_id, $subscribers, $views_avg, $views_err, $update_date, $active, $evaluated) {
			self::connect();
			try {
				
				$stmt = self::$pdo->prepare('INSERT INTO search_channel_evaluation (channel_id, subscribers, views_avg, views_err, update_date, active, evaluated) VALUES (?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE subscribers = ?, views_avg = ?, views_err = ?, update_date = ?, active = ?, evaluated = ?');
				$stmt->execute([$channel_id, $subscribers, $views_avg, $views_err, $update_date, $active, $evaluated, $subscribers, $views_avg, $views_err, $update_date, $active, $evaluated]);
					
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				//$result->data = $row;
				return $result;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function updateChannelEvaluation($channel_id, $subscribers, $views_avg, $views_err, $update_date, $active, $evaluated) {
			self::connect();
			try {
				$stmt = self::$pdo->prepare('UPDATE search_channel_evaluation SET subscribers = ?, views_avg = ?, views_err = ?, update_date = ?, active = ?, evaluated = ? WHERE channel_id = ?');
				$stmt->execute([$subscribers, $views_avg, $views_err, $update_date, $active, $evaluated, $channel_id]);
					
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				//$result->data = $row;
				return $result;
			}
			catch(Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}

		public static function hasChannelEvaluation($channel_id) {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM search_channel_evaluation WHERE channel_id = ?');
			$stmt->execute([$channel_id]);
			$stmt->fetch();
			return $stmt->rowCount() > 0;
		}

		public static function getChannelsEvaluations() {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM search_channel_evaluation');
			$stmt->execute();
			return $stmt->fetchAll();
		}

		public static function getChannelsDetailsEvaluations($project_id) {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM search_channel_details_evaluation WHERE project_id = ?');
			$stmt->execute([$project_id]);
			return $stmt->fetchAll();
		}

		public static function getChannelsDetailsEvaluationsForEvaluation($project_id) {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM search_channel_details_evaluation WHERE project_id = ? AND evaluated = ? ORDER BY sim_games_count DESC');
			$stmt->execute([$project_id, 0]);
			return $stmt->fetchAll();
		}

		public static function getChannelsDetailsEvaluationsForCSV($project_id) {
			self::connect();
			$stmt = self::$pdo->prepare('SELECT * FROM search_channel_details_evaluation WHERE project_id = ? AND reviewed = ? AND valid = ?');
			$stmt->execute([$project_id, 1, 1]);
			return $stmt->fetchAll();
		}
		//Channels search - BEGIN =========================================
	}
?>
