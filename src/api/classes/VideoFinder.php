<?php

class VideoFinder {
	private static $client;
	private static $youtube;
	private static $MAX_PAGES = 10;
	private static $QUOTA_SEARCH_LIST = 100;

	private static $API_KEY = "AIzaSyBagTzijrSdGdJCxj0wlyWFxi253hQ9Vlo";

    #handle error method
	public static function exception_error_handler($errno, $errstr, $errfile, $errline) {
		throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
	}

	public static function initApiKey() {
		//$key_data = null;
		//$current_key = DatabaseHandler::getCurrentYoutubeApiKey()['key_id'];
		
		//$key_data = DatabaseHandler::getOldestApiKey();

		//if($key_data == NULL) {
			//return false;
		//}

		$key = self::$API_KEY;//$key_data['api_key'];
		self::$client = new Google_Client();

		self::$client->setApplicationName("Youtube Reviewer Finder");
		self::$client->setDeveloperKey($key);

		self::$youtube = new Google_Service_YouTube(self::$client);

		//DatabaseHandler::updateCurrentYoutubeApiKey($current_key, $key_data['key_id']);

		return true;
	}

    #seacher method
	public static function searchVideos($projectId, $query, $fromDate) {
		set_error_handler("VideoFinder::exception_error_handler");
		
		if(!self::initApiKey()) {
			error_log("No valid api key");
			return array();
		}

		try {
			$nextPageToken = '';
			$processedKeyWords = $query;
			$videosIds = array();

			$initalSearchDate = new DateTime($fromDate);
			$publishedAfter = $initalSearchDate->format('Y-m-d\TH:i:s.s\Z');
			$firstTime = true;
			
			$projectName = DatabaseHandler::getProjectData($projectId)->data["project_name"];

			DatabaseHandler::updateYoutubeKeyQuota(self::$QUOTA_SEARCH_LIST, 0);
			// while(isset($nextPageToken)) {
				if($firstTime) {
					$firstTime = false;
					$searchResponse = self::$youtube->search->listSearch('id, snippet', array(
								'q' => $processedKeyWords,
								'maxResults' => 50,
								'publishedAfter' => $publishedAfter
								));
				}
				else {
					$searchResponse = self::$youtube->search->listSearch('id, snippet', array(
								'q' => $processedKeyWords,
								'maxResults' => 50,
								'pageToken' => $nextPageToken,
								'publishedAfter' => $publishedAfter
								));
				}
				
				foreach ($searchResponse['items'] as $searchResult) {
					switch ($searchResult['id']['kind']) {
						case 'youtube#video':
						if(strpos(strtolower($searchResult['snippet']['title']), strtolower($projectName)) !== false ||
							strpos(strtolower($searchResult['snippet']['description']), strtolower($projectName)) != false) {
								array_push($videosIds, $searchResult['id']['videoId']);
						}
					}
				}
				if(isset($searchResponse['nextPageToken'])) {
					$nextPageToken = $searchResponse['nextPageToken'];
				}
				// else {
				// 	break;
				// }
			// }
			return $videosIds;
			
		} catch (Google_Service_Exception $e) {
			error_log($e);
			return array();
		} catch (Google_Exception $e) {
			error_log($e);
			return array();
		} catch (Exception $e) {
			error_log($e);
			return array();
		}
	}

	public static function tagsContainsProjectName($tags, $projectName) {
		$projectNameStrings = explode(' ', $projectName);
		array_push($projectNameStrings, $projectName);
		array_push($projectNameStrings, preg_replace('/\s+/', '', $projectName));

		if(empty($tags)) {
			return false;
		}

		foreach($tags as $tag) {
			foreach($projectNameStrings as $token) {
				if(strpos(strtolower($tag), strtolower($token)) != false) {
					error_log("Tag " . $tag . " contains " . $token);
					return true;
				}
			}
		}
		return false;
	}

	public static function filterNewsVideos($idsList) {
		$processedIdsList = DatabaseHandler::getNonExistentVideosIds($idsList);
		return $processedIdsList;
	}

	public static function filterOldVideos($idsList) {
		$processedIdsList = DatabaseHandler::getOldValidIds($idsList);
		return $processedIdsList;
	}

	public static function getVideosData($idsList, $newVideos) {
		$resultVideos = array();
		$videosIdsLists = array();			
		$currentArray = array();
		$videoIndex = 0;
		
		//Create lists of no more than 50 ids
		foreach($idsList as $id) {
			if(count($currentArray) >= 50) {
				array_push($videosIdsLists, $currentArray);
				$currentArray = array();
			}
			array_push($currentArray, $id);
		}

		if(count($currentArray) > 0) {
			array_push($videosIdsLists,$currentArray);
			$currentArray = array();
		}

		if($newVideos){
		//Gets the videos stats
			try {
				//For each array of up to 50 videos
				foreach($videosIdsLists as $videosList) {
					$inputIds = join(',', $videosList);
					
					$statistics = self::$youtube->videos->listVideos('id,snippet,statistics,contentDetails', array(
											'id' => $inputIds));
					
					foreach ($statistics['items'] as $item) {
						$videoData = new stdClass();
						$videoData->id = $item['id'];

						$dateObject = new DateTime($item['snippet']['publishedAt']);
						$video_date = date_format($dateObject , 'y/m/d');
						$videoData->publishedAt = $video_date;
						
						$videoData->channelId = $item['snippet']['channelId'];
						$videoData->title = $item['snippet']['title'];
						$videoData->description = $item['snippet']['description'];
						$videoData->thumbnail = $item['snippet']['thumbnails']['default']['url'];
						$videoData->tags = $item['snippet']['tags'];
						$videoData->categoryId = $item['snippet']['categoryId'];
						$videoData->viewCount = $item['statistics']['viewCount'];
						$videoData->likeCount = $item['statistics']['likeCount'];
						$videoData->dislikeCount = $item['statistics']['dislikeCount'];
						$videoData->favoriteCount = $item['statistics']['favoriteCount'];
						$videoData->commentCount = $item['statistics']['commentCount'];
						$videoData->duration = $item['contentDetails']['duration'];
						$videoData->dimension = $item['contentDetails']['dimension'];
						$videoData->definition = $item['contentDetails']['definition'];
						$videoData->caption = $item['contentDetails']['caption'];
						array_push($resultVideos, $videoData);
					}
				}
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				$result->data = $resultVideos;
				return $result;
			}
			catch (Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}
		else{
			try{
				foreach($videosIdsLists as $videosList) {
					$inputIds = join(',', $videosList);
					
					$statistics = self::$youtube->videos->listVideos('id,snippet,statistics', array(
													'id' => $inputIds));

					foreach ($statistics['items'] as $item) {
						$videoData = new stdClass();
						$videoData->id = $item['id'];

						$videoData->title = $item['snippet']['title'];
						$videoData->description = $item['snippet']['description'];
						$videoData->viewCount = $item['statistics']['viewCount'];
						$videoData->likeCount = $item['statistics']['likeCount'];
						$videoData->dislikeCount = $item['statistics']['dislikeCount'];
						$videoData->favoriteCount = $item['statistics']['favoriteCount'];
						$videoData->commentCount = $item['statistics']['commentCount'];
						
						array_push($resultVideos, $videoData);
							
					}
				}
				$result = new stdClass();
				$result->status = ResponseCode::OK;
				$result->data = $resultVideos;
				return $result;
			}
			catch (Exception $e) {
				error_log($e);
				$result = new stdClass();
				$result->status = ResponseCode::INTERNAL_SERVER_ERROR;
				$result->data = $e->getMessage();
				return $result;
			}
		}
	}	
}
?>