<?php
    class TwitchFinder {
        private static $CLIENT_ID = "s3b0ckqno9jyp4udy2txel3uq7oqar";
        private static $CLIENT_SECRET = "9zl41vkvwmnu9nhkz3djbcsreslss9";

        private static $videosPath = "https://api.twitch.tv/helix/videos";
        private static $gamesPath = "https://api.twitch.tv/helix/games";
        private static $streamsPath = "https://api.twitch.tv/helix/streams";
        private static $usersPath = "https://api.twitch.tv/helix/users";
        private static $userFollowsPath = "https://api.twitch.tv/helix/users/follows";

        private static $accessToken;

        public static function getAccessToken() {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,"https://id.twitch.tv/oauth2/token".
                "?client_id=" . self::$CLIENT_ID .
                "&client_secret=" . self::$CLIENT_SECRET .
                "&grant_type=client_credentials");
            curl_setopt($ch, CURLOPT_POST, 1);
            
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $server_output = curl_exec($ch);
            if(!$server_output) {
                return curl_error($ch);
            }
            return json_decode($server_output);
        }

        public static function query($queryString) {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_HTTPHEADER => array(
                    'Client-ID: ' . self::$CLIENT_ID,
                    'Authorization: Bearer ' . self::$accessToken
                ),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $queryString
            ));

            $response = curl_exec($curl);
            curl_close($curl);

            return $response;
        }

        public static function searchChannels($gameNames, $online) {
            self::$accessToken = self::getAccessToken();

            if(!isset(self::$accessToken->access_token)) {
                return self::$accessToken;
            }
            else {
                self::$accessToken = self::$accessToken->access_token;
            }

            $searchResult = array();
            $userIds = array();
            $users = null;
            $gamesData = self::getGames($gameNames);
            
            if(!isset($gamesData->data))
            {
                error_log("Data not set");
                return $gamesData;
            }

            $gamesData = $gamesData->data;
            
            foreach($gamesData as $game) {
                //in case $online is true, we'll search for streams
                if($online) {
                    $streams = self::getStreams($game->id);
                    foreach($streams as $stream) {
                        if(!in_array($stream->user_id, $userIds)) {
                            array_push($searchResult, $stream);
                            array_push($userIds, $stream->user_id);
                        }
                    }
                }
                else {
                    $videos = self::getVideos($game->id);
                    foreach($videos as $video) {
                        array_push($searchResult, $video);
                        array_push($userIds, $video->user_id);
                    }
                }
            }

            $users = self::getUsers($userIds);

            return $users;
        }

        public static function getUserFollowers($usersNames) {
            self::$accessToken = self::getAccessToken();

            if(!isset(self::$accessToken->access_token)) {
                return self::$accessToken;
            }
            else {
                self::$accessToken = self::$accessToken->access_token;
            }


            $usersArray = array();
            $userIds = self::getUsersByName($usersNames);

            foreach($userIds as $user) {
                $follows = json_decode(self::query(self::$userFollowsPath . '?to_id=' . $user->id));
                return $follows->total;
            }
        }

        public static function getUserVideos($usersNames) {
            self::$accessToken = self::getAccessToken();

            if(!isset(self::$accessToken->access_token)) {
                return self::$accessToken;
            }
            else {
                self::$accessToken = self::$accessToken->access_token;
            }


            $usersArray = array();
            $userIds = self::getUsersByName($usersNames);

            foreach($userIds as $user) {
                $videos = json_decode(self::query(self::$videosPath . '?user_id=' . $user->id . '&first=' . 40));

                //calculate mean
                $mean = 0;
                $count = 0;

                if(!isset($videos->data)) {
                    return 0;
                }

                foreach($videos->data as $video) {
                    $mean += $video->view_count;
                    $count++;
                }
                if($count > 0) {
                    return $mean / $count;
                }
                else {
                    return 0;
                }
            }
        }

        public static function getUserLastVideo($usersNames) {
            self::$accessToken = self::getAccessToken();

            if(!isset(self::$accessToken->access_token)) {
                return self::$accessToken;
            }
            else {
                self::$accessToken = self::$accessToken->access_token;
            }


            $userIds = self::getUsersByName($usersNames);

            foreach($userIds as $user) {
                $videos = json_decode(self::query(self::$videosPath . '?user_id=' . $user->id . '&first=' . 1));

                if(!isset($videos->data)) {
                    return 0;
                }
                $date = strtotime($videos->data[0]->published_at);
                $newFormat = date('Y-m-d', $date);
                return $newFormat;
            }
        }

        public static function getVideos($gameId) {
            $videosArray = array();

            //first search
            $videos = json_decode(self::query(self::$videosPath . '?first=100&game_id=' . $gameId));
            if(isset($videos->data)) {
                $videosData = $videos->data;
                foreach($videosData as $video) {
                    array_push($videosArray, $video);
                }

                while(isset($videos->data) && isset($videos->pagination) && isset($videos->pagination->cursor)) {
                    $videos = json_decode(self::query(self::$videosPath . '?first=100&game_id=' . $gameId . '&after=' . $videos->pagination->cursor));
                    if(isset($videos->data)) {
                        $videosData = $videos->data;
                        foreach($videosData as $video) {
                            array_push($videosArray, $video);
                        }
                    }
                }
            }
            return $videosArray;
        }

        public static function getGames($gameNames) {
            $gameNamesFormatted = "";
            foreach($gameNames as $name) {
                $gameNamesFormatted = $gameNamesFormatted . 'name=' . urlencode($name) . "&";
            }

            $gameNamesFormatted = mb_substr($gameNamesFormatted, 0, -1);
            $gamesInfo = json_decode(self::query(self::$gamesPath . '?' . $gameNamesFormatted));

            return $gamesInfo;
        }

        public static function getStreams($gameId) {
            $streamsArray = array();

            //first search
            $streams = json_decode(self::query(self::$streamsPath . '?first=100&game_id=' . $gameId));
            if(isset($streams->data)) {
                $streamsData = $streams->data;
                foreach($streamsData as $stream) {
                    array_push($streamsArray, $stream);
                }

                while(isset($streams->data) && isset($streams->pagination) && isset($streams->pagination->cursor)) {
                    $streams = json_decode(self::query(self::$streamsPath . '?first=100&game_id=' . $gameId . '&after=' . $streams->pagination->cursor));
                    if(isset($streams->data)) {
                        $streamsData = $streams->data;
                        foreach($streamsData as $stream) {
                            array_push($streamsArray, $stream);
                        }
                    }
                }
            }
            return $streamsArray;
        }

        public static function getUsersByName($usersNames) {
            $userNamesFormatted = "";
            $userNamesList = array();
            $usersArray = array();
            $i = 0;

            foreach($usersNames as $name) {
                if($i >= 100) {
                    $i = 0;
                    array_push($userNamesList, $userNamesFormatted);
                    $userNamesFormatted = "";
                }

                $userNamesFormatted = $userNamesFormatted . 'login=' . urlencode($name) . "&";
                $i++;
            }

            if($i < 100) {
                array_push($userNamesList, $userNamesFormatted);
            }

            for($i = 0; $i < count($userNamesList); ++$i) {
                $userNamesList[$i] = mb_substr($userNamesList[$i], 0, -1);
            }

            foreach($userNamesList as $userParams) {
                $users = json_decode(self::query(self::$usersPath . '?' . $userParams));

                if(isset($users->data)) {
                    foreach($users->data as $userData) {
                        array_push($usersArray, $userData);
                    }
                }
            }

            return $usersArray;
        }

        public static function getUsers($userIds) {
            $userIdsFormatted = "";
            $userIdsList = array();
            $usersArray = array();
            $i = 0;

            foreach($userIds as $id) {
                if($i >= 100) {
                    $i = 0;
                    array_push($userIdsList, $userIdsFormatted);
                    $userIdsFormatted = "";
                }

                $userIdsFormatted = $userIdsFormatted . 'id=' . $id . "&";
                $i++;
            }

            if($i < 100) {
                array_push($userIdsList, $userIdsFormatted);
            }

            for($i = 0; $i < count($userIdsList); ++$i) {
                $userIdsList[$i] = mb_substr($userIdsList[$i], 0, -1);
            }

            foreach($userIdsList as $userParams) {
                $users = json_decode(self::query(self::$usersPath . '?' . $userParams));

                if(isset($users->data)) {
                    foreach($users->data as $userData) {
                        array_push($usersArray, $userData);
                    }
                }
            }

            return $usersArray;
        }
    }
?>