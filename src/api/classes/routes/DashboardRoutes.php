<?php
require_once '../api/classes/GoogleAnalyticsDataRetriever.php';
require_once '../api/classes/YoutuberFinder.php';

$app->get('/dashboard/projects', function($request, $response, array $args) {
	$projects = DatabaseHandler::getProjects();
    $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", json_encode($projects)));
    return $response;
});

//TEST=============================================================================
/*$app->get('/getApiKey', function($request, $response, array $args) {
    $apiKeys = DatabaseHandler::getApiKey(1);
    $date = date('Y-m-d', strtotime($apiKeys['last_usage_date']));
    $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, "Success " . (strtotime(date('Y-m-d')) == strtotime($apiKeys['last_usage_date'])), json_encode($apiKeys)));
});

$app->get('/getCurrentYoutubeApiKey', function($request, $response, array $args) {
    $currentApiKey = DatabaseHandler::getCurrentYoutubeApiKey();
    $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", json_encode($currentApiKey)));
});

$app->get('/getOldestApiKey', function($request, $response, array $args) {
    $currentApiKey = DatabaseHandler::getOldestApiKey();
    $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", json_encode($currentApiKey)));
});

$app->get('/getNextYoutubeApiKey/{required_quota}', function($request, $response, array $args) {
    $required_quota = $request->getAttribute('required_quota');
    $currentApiKey = DatabaseHandler::getNextYoutubeApiKey($required_quota);
    $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", json_encode($currentApiKey)));
});

$app->get('/updateYoutubeKeyQuotaByKey/{quota}', function($request, $response, array $args) {
    $quota = $request->getAttribute('quota');
    $data = DatabaseHandler::updateYoutubeKeyQuota($quota, 500);
    $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", $data));
    return $response;
});

$app->get('/hasQuota/{key}', function($request, $response, array $args) {
    $key = $request->getAttribute('key');
    $data = DatabaseHandler::hasQuotaByKeyId($key, 500);
    $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", $data));
    return $response;
});

$app->get('/updateYoutubeKeyQuotaByKey/{key}/{quota}', function($request, $response, array $args) {
    $key = $request->getAttribute('key');
    $quota = $request->getAttribute('quota');
    $data = DatabaseHandler::updateYoutubeKeyQuotaByKey($key, $quota, 500);
    $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", $data));
    return $response;
});

$app->get('/resetYoutubeKeyQuota', function($request, $response, array $args) {
    $data = DatabaseHandler::resetYoutubeKeyQuota();
    $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", $data));
    return $response;
});

$app->get('/getOldestApiKey', function($request, $response, array $args) {
    $data = DatabaseHandler::getOldestApiKey();
    $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", $data));
    return $response;
});

$app->get('/resetYoutubeKeyQuotaById/{key}', function($request, $response, array $args) {
    $key = $request->getAttribute('key');
    $data = DatabaseHandler::resetYoutubeKeyQuotaById($key);
    $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", $data));
    return $response;
});*/
//TEST-END============================================================================


//get wishlists balance
$app->post('/dashboard/graph_data/list', function($request, $response, array $args) {
    $projectId = $request->getParsedBody()['projectId'];
	$toDate = $request->getParsedBody()['toDate'];
	$fromDate = $request->getParsedBody()['fromDate'];

    $wishlists = DatabaseHandler::getWishlistsOrPurchaseBalance($projectId, $fromDate, $toDate);
    $events = DatabaseHandler::getEventsPerProjectId($projectId, $fromDate, $toDate, 'project_stats_event_date ASC');
    $views = DatabaseHandler::getViewsDataForChart($projectId, $fromDate, $toDate);
    $videosCount = DatabaseHandler::getVideosPerDayForChart($projectId, $fromDate, $toDate);
    $twitchViews = DatabaseHandler::getTwitchViewsStats($projectId, $fromDate, $toDate);
    $viewId = DatabaseHandler::getProjectData($projectId)->data['analytics_view_id'];

    //[0] - stats_id
    //[1] - project_id
    //[2] - stats_date
    //[3] - stats_type
    //[4] - stats_additions
    //[5] - stats_deletions
    //[6] - stats_activations
    //[7] - stats_gifts

    $wishlistsData = array();
    $sellingData = array();
    $eventsData = array();

    //wishlists
    foreach($wishlists as $w) {
        $r = new stdClass();
        $r->date = $w['stats_date'];
        $r->stats_type = $w['stats_type'];
        $r->balance = $w['stats_additions'] - $w['stats_deletions'] - $w['stats_activations'] - $w['stats_gifts'];

        if($r->stats_type == 1) { //selling
            array_push($sellingData, $r);
        }
        else {
            array_push($wishlistsData, $r);
        }
    }

    //events
    foreach($events as $w) {
        $r = new stdClass();
        $r->date = $w['project_stats_event_date'];
        $r->name = $w['project_stats_event_name'];
        $r->desc = $w['project_stats_event_description'];

        array_push($eventsData, $r);
    }

    $result = new stdClass();
    $result->wishlistsData = $wishlistsData;
    $result->sellingData = $sellingData;
    $result->viewsData = $views;
    $result->videosCountData = $videosCount;
    $result->eventsData = $eventsData;
    $result->analyticsData = GoogleAnalyticsDataRetriever::getDataPerRange($viewId, $fromDate, $toDate);
    $result->twitchViews = $twitchViews;

    $response = JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", $result);
	return $response;
});

//Insert multiple
$app->post('/dashboard/twitch_views_stats/insertMultiple', function($request, $response, array $args) {

	$projectId = $request->getParsedBody()['projectId'];
	$parsedData = $request->getParsedBody()['data'];
	$dates = $parsedData['labels'] ?? array();
	$views = $parsedData['data'] ?? array();

	$result = DatabaseHandler::insertTwitchViewsData($projectId, $dates, $views);
	$response = JsonUtils::generateJsonResponse($result->status, "Success");
	return $response;	
});
?>
