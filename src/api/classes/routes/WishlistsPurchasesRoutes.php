<?php
//list route
$app->post('/dashboard/wishlist_purchase_stats/list', function($request, $response, array $args) {
	$params = null;
	if($request->getParsedBody()['project_id'] != null) {
		$params = $request->getParsedBody();
	}
	else {
		$params = $request->getParams();
	}

	$project_id = $params['project_id'];
	$toDate = $params['toDate'];
	$fromDate = $params['fromDate'];
	$offset = $params['jtStartIndex'];
	$limit = $params['jtPageSize'];
	$sorting = $params['jtSorting'];

	$result = DatabaseHandler::getWishlistsOrPurchases($project_id, $sorting);
	$totalStats = count($result);
	$resultStats = array();
	//filter the stats to return
	for($i = $offset; $i < $offset + $limit && $i < $totalStats; ++$i) {
		array_push($resultStats, $result[$i]);
	}

	if($result != ResponseCode::INTERNAL_SERVER_ERROR) {
		$response = JsonUtils::generateJTableResponse(ResponseCode::OK, "Success", $resultStats, $totalStats);
	}
	else {
		$response = JsonUtils::generateJTableNoRowsResponse($result, $result);
	}
	
	return $response;
});

//Insert route
$app->post('/dashboard/wishlist_stats/insert', function($request, $response, array $args) {
	$project_id = $request->getParams()['project_id'];
	$date = $request->getParsedBody()['stats_date'];
	$type = $request->getParsedBody()['stats_type'];
	$additions = $request->getParsedBody()['stats_additions'];
	$deletions = $request->getParsedBody()['stats_deletions'];
	$activations = $request->getParsedBody()['stats_activations'];
	$gifts = $request->getParsedBody()['stats_gifts'];
	
	$result = DatabaseHandler::insertWishlistOrPurchase($project_id, $date, $type, $additions, $deletions, $activations, $gifts);

	if($result->status == ResponseCode::OK) {
		$response = JsonUtils::generateJTableInsertResponse($result->status, "Success", $result->data);
	}
	else {
		$response = JsonUtils::generateJTableInsertResponse($result->status, $result->data);
	}
	return $response;
});

//Insert multiple wishlists
$app->post('/dashboard/wishlist_stats/insertMultiple', function($request, $response, array $args) {

	$projectId = $request->getParsedBody()['projectId'];
	$statsType = $request->getParsedBody()['statsType'];
	$parsedData = $request->getParsedBody()['data'];
	$add = $parsedData[0] ?? array();
	$del = $parsedData[1] ?? array();
	$actv = $parsedData[2] ?? array();
	$gift = $parsedData[3] ?? array();

	$result = DatabaseHandler::insertMultipleWishlistOrPurchase($projectId, $statsType, $add, $del, $actv, $gift);
	$response = JsonUtils::generateJsonResponse($result->status, "Success");
	return $response;	
});

//Insert multiple purchases
$app->post('/dashboard/purchase_stats/insertMultiple', function($request, $response, array $args) {

	$projectId = $request->getParsedBody()['projectId'];
	$statsType = $request->getParsedBody()['statsType'];
	$parsedData = $request->getParsedBody()['data'];

	$result = DatabaseHandler::insertMultiplePurchases($projectId, $parsedData);
	$response = JsonUtils::generateJsonResponse($result->status, "Success");
	return $response;	
});

//Update route
$app->post('/dashboard/wishlist_purchase_stats/update', function($request, $response, array $args) {
	$params = null;
	if($request->getParsedBody()['stats_id'] != null) {
		$params = $request->getParsedBody();
	}
	else {
		$params = $request->getParams();
	}

	$stats_id = $params['stats_id'] ?? null;
	$project_id = $params['project_id'] ?? null;
	$date = $params['stats_date'] ?? null;
	$type = $params['stats_type'] ?? null;
	$additions = $params['stats_additions'] ?? null;
	$deletions = $params['stats_deletions'] ?? null;
	$activations = $params['stats_activations'] ?? null;
	$gifts = $params['stats_gifts'] ?? null;

	$result = DatabaseHandler::updateWishlistOrPurchase($stats_id, $date, $type, $additions, $deletions, $activations, $gifts);

	if($result == ResponseCode::OK) {
		$response = JsonUtils::generateJTableNoRowsResponse($result, "Success");
	}
	else {
		$response = JsonUtils::generateJTableNoRowsResponse(ResponseCode::INTERNAL_SERVER_ERROR, $result);
	}
	
	return $response;
});

//Delete route
$app->post('/dashboard/wishlist_purchase_stats/delete', function($request, $response, array $args) {
	$stats_id = $request->getParsedBody()['stats_id'];
	
	$result = DatabaseHandler::deleteWishlistOrPurchase($stats_id);
	
	$response = JsonUtils::generateJTableNoRowsResponse($result, $result);
	
	return $response;
});

//stats type list route
$app->post('/dashboard/purchase_stats_type/list', function($request, $response, array $args) {
	$result = DatabaseHandler::getPurchaseStatsType();
	
	if($result != ResponseCode::INTERNAL_SERVER_ERROR) {
		$response = JsonUtils::generateJTableNoRowsResponse(ResponseCode::OK, "Success", $result);
	}
	else {
		$response = JsonUtils::generateJTableNoRowsResponse($result, $result);
	}
	
	return $response;
});
?>