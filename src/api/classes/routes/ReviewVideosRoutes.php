<?php
//Insert or update route
$app->post('/dashboard/query/insert', function($request, $response, array $args) {
	$project_id = $request->getParams()['project_id'];
	$terms = $request->getParsedBody()['query_terms'];

	$result = DatabaseHandler::insertQuery($project_id, $terms);

	if($result->status == ResponseCode::OK) {
		$response = JsonUtils::generateJTableInsertResponse($result->status, "Success", $result->data);
	}
	else {
		$response = JsonUtils::generateJTableInsertResponse($result->status, $result->data);
	}

	return $response;
});

$app->post('/dashboard/query/update', function($request, $response, array $args) {

	foreach($request->getParsedBody() as $key=>$value) {
		error_log($key . ': ' . $value);
	}

    $project_id = $request->getParsedBody()['project_id'];
	$query_id = $request->getParsedBody()['query_id'];
    $initalDate = $request->getParsedBody()['initial_search_date']; 

	$result = DatabaseHandler::updateQuery($project_id, $query_id, $initalDate);
    $response = JsonUtils::generateJTableNoRowsResponse($result->status, "Success", $result->data);

	return $response;
});

$app->post('/dashboard/query/delete', function($request, $response, array $args) {
	$query_id = $request->getParsedBody()['query_id'];

	$result = DatabaseHandler::deleteQuery($query_id, $result_count);
    $response = JsonUtils::generateJsonResponse($result->status, $result->data);

	return $response;
});

$app->post('/dashboard/query/last', function($request, $response, array $args) {
	$project_id = $request->getParsedBody()['project_id'];

    $result = DatabaseHandler::getMostRecentQuery($project_id);
    if($result->status == ResponseCode::OK) {
        $response = JsonUtils::generateJsonResponse($result->status, "Success", $result->data);
    }
    else {
        $response = JsonUtils::generateJsonResponse($result->status, "Error", $result->data);
    }

	return $response;
});

$app->post('/dashboard/query/list', function($request, $response, array $args) {
    $params = null;
	if($request->getParsedBody()['project_id'] != null) {
		$params = $request->getParsedBody();
	}
	else {
		$params = $request->getParams();
	}

	$project_id = $params['project_id'];
    $offset = $params['jtStartIndex'];
	$limit = $params['jtPageSize'];
	$sorting = $params['jtSorting'];

    $result = DatabaseHandler::getQueries($project_id, $sorting);
    $totalStats = count($result->data);
	$resultStats = array();
	//filter the stats to return
	for($i = $offset; $i < $offset + $limit && $i < $totalStats; ++$i) {
		array_push($resultStats, $result->data[$i]);
	}

    if($result->status != ResponseCode::INTERNAL_SERVER_ERROR) {
		$response = JsonUtils::generateJTableResponse(ResponseCode::OK, "Success", $resultStats, $totalStats);
	}
	else {
		$response = JsonUtils::generateJTableNoRowsResponse($result->status, $result->data);
	}

	return $response;
});

$app->post('/dashboard/video_review/list', function($request, $response, $args) {
	$params = null;
	
	if($request->getParsedBody()['fromDate'] != null) {
		$params = $request->getParsedBody();
	}
	else {
		$params = $request->getParams();
	}
	
	$projectId = $params['projectId'];
	$fromDate = $params['fromDate'];
	$toDate = $params['toDate'];
	$reviewed = strcmp($params['reviewed'], 'true') == 0 ? 1 : 0;
	
	$offset = $params['jtStartIndex'];
    $limit = $params['jtPageSize'];
    $sorting = $params['jtSorting'];
	
    $videos = DatabaseHandler::getVideosForReview($projectId, $fromDate, $toDate, $reviewed, $sorting);
	$totalVideos = count($videos);
	$resultVideos = array();
	//filter the videos to return
	for($i = $offset; $i < $offset + $limit && $i < $totalVideos; ++$i) {
		array_push($resultVideos, $videos[$i]);
	}
	
    $response = JsonUtils::generateJTableResponse(ResponseCode::OK, "Success", $resultVideos, $totalVideos);
    return $response;
});

$app->post('/dashboard/video_review/update', function($request, $response, $args) {
	$projectId = $request->getParsedBody()['project_id']; 
	$video_id = $request->getParsedBody()['video_id'];
	$valid = $request->getParsedBody()['valid'] ?? null;
	$reviewed = $request->getParsedBody()['reviewed'] ?? null;
	
	$removed = false;
	if($reviewed != null) {
		$removed = true;
		DatabaseHandler::setVideoReviewed($projectId, $video_id, $reviewed);
	}
	else if($valid != null) {
		DatabaseHandler::setVideoValid($projectId, $video_id, $valid);
	}
	
	$result = new stdClass();
	$result->videoId = $video_id;
	$result->removed = $removed;
	
	$response = JsonUtils::generateJTableNoRowsResponse(ResponseCode::OK, "Success", json_encode($result));
	
	return $response;
});

$app->post('/dashboard/video_review/update_list', function($request, $response, $args) {
	$projectId = $request->getParsedBody()['project_id']; 
	$video_ids= $request->getParsedBody()['video_ids'];
	$valid_values = $request->getParsedBody()['valid_values'] ?? null;
	$reviewed = $request->getParsedBody()['reviewed'] ?? null;

	if($reviewed != null) {
		foreach($video_ids as $video_id) {
			DatabaseHandler::setVideoReviewed($projectId, $video_id, $reviewed);
		}
	}
	else if($valid_values != null) {
		for($i = 0; $i < count($video_ids); $i++) {
			DatabaseHandler::setVideoValid($projectId, $video_ids[$i], $valid_values[$i]);
		}
	}
	
	$result = new stdClass();
	$result->videoIds = $video_ids;
	$result->removed = false;
	
	$response = JsonUtils::generateJTableNoRowsResponse(ResponseCode::OK, "Success", json_encode($result));
	
	return $response;
});
?>