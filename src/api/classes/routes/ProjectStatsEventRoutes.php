<?php
//list route
$app->post('/dashboard/stats_events/list', function($request, $response, array $args) {
	$params = null;
	if($request->getParsedBody()['project_id'] != null) {
		$params = $request->getParsedBody();
	}
	else {
		$params = $request->getParams();
	}
	$project_id = $params['project_id'];
	$toDate = $params['toDate'];
	$fromDate = $params['fromDate'];
	$offset = $params['jtStartIndex'];
	$limit = $params['jtPageSize'];
	$sorting = $params['jtSorting'];


	$result = DatabaseHandler::getEventsPerProjectId($project_id, $fromDate, $toDate, $sorting);
	$totalStats = count($result);
	$resultStats = array();
	//filter the stats to return
	for($i = $offset; $i < $offset + $limit && $i < $totalStats; ++$i) {
		array_push($resultStats, $result[$i]);
	}

	if($result != ResponseCode::INTERNAL_SERVER_ERROR) {
		$response = JsonUtils::generateJTableResponse(ResponseCode::OK, "Success", $resultStats, $totalStats);
	}
	else {
		$response = JsonUtils::generateJTableNoRowsResponse($result, $result);
	}
	
	return $response;
});

//Insert route
$app->post('/dashboard/stats_events/insert', function($request, $response, array $args) {
	$project_id = $request->getParams()['project_id'];
	$date = $request->getParsedBody()['project_stats_event_date'];
	$name = $request->getParsedBody()['project_stats_event_name'];
	$description = $request->getParsedBody()['project_stats_event_description'];

	$result = DatabaseHandler::insertEvent($project_id, $date, $name, $description);

	if($result->status == ResponseCode::OK) {
		$response = JsonUtils::generateJTableInsertResponse($result->status, "Success", $result->data);
	}
	else {
		$response = JsonUtils::generateJTableInsertResponse($result->status, $result->data);
	}

	return $response;
});

//Update route
$app->post('/dashboard/stats_events/update', function($request, $response, array $args) {
	$params = null;
	if($request->getParsedBody()['project_stats_event_id'] != null) {
		$params = $request->getParsedBody();
	}
	else {
		$params = $request->getParams();
	}

	$event_id = $params['project_stats_event_id'] ?? null;
	$date = $params['project_stats_event_date'] ?? null;
	$name = $params['project_stats_event_name'] ?? null;
	$description = $params['project_stats_event_description'] ?? null;

	$result = DatabaseHandler::updateEvent($event_id, $date, $name, $description);

	if($result == ResponseCode::OK) {
		$response = JsonUtils::generateJTableNoRowsResponse($result, "Success");
	}
	else {
		$response = JsonUtils::generateJTableNoRowsResponse(ResponseCode::INTERNAL_SERVER_ERROR, $result);
	}
	
	return $response;
});

//Delete route
$app->post('/dashboard/stats_events/delete', function($request, $response, array $args) {
	$event_id = $request->getParsedBody()['project_stats_event_id'];
	$result = DatabaseHandler::deleteEvent($event_id);

	$response = JsonUtils::generateJTableNoRowsResponse($result->status, $result->data);
	
	return $response;
});
?>