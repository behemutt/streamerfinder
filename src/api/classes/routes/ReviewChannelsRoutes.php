<?php

$app->get('/dashboard/daily_channel_search', function($request, $response, array $args) {
	$project_id = 2;
    $result = YoutuberFinder::executeSearch($project_id);
    
    $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, json_encode($result), $result));
    return $response;
});

//Tests - BEGIN =========================
// $app->get('/standard_deviation', function($request, $response, array $args) {

// 	$array = array(1, 7, 9, 3, 20, 12, 2, 9);
// 	$deviation = stats_standard_deviation($array);

//     $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, json_encode($array), $deviation));
//     return $response;
// });

// $app->get('/set_channels_valid/{project_id}/{valid}/[{channels:.*}]', function($request, $response, array $args) {
// 	$project_id = $request->getAttribute('project_id');
// 	$valid = $request->getAttribute('valid');
// 	$channels = explode('/', $request->getAttribute('channels'));

// 	foreach($channels as $c) {
// 		if(DatabaseHandler::hasChannelDetails($project_id, $c)) {
// 			DatabaseHandler::setChannelValid($project_id, $c, $valid);
// 		}
// 	}

// 	$response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, "Success"));
// 	return $response;
// });

// $app->get('/set_channels_reviewed/{project_id}/{reviewed}/[{channels:.*}]', function($request, $response, array $args) {
// 	$project_id = $request->getAttribute('project_id');
// 	$reviewed = $request->getAttribute('reviewed');
// 	$channels = explode('/',$request->getAttribute('channels'));

// 	foreach($channels as $c) {
// 		if(DatabaseHandler::hasChannelDetails($project_id, $c)) {
// 			DatabaseHandler::setChannelReviewed($project_id, $c, $reviewed);
// 		}
// 	}

// 	$response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, "Success"));
// 	return $response;
// });

//Tests - END =========================

$app->post('/dashboard/channel_review_csv', function($request, $response, $args) {
	$project_id = $request->getParsedBody()['project_id'];
	$channels_data = DatabaseHandler::getChannelsDetailsEvaluationsForCSV($project_id);
	$result = array();
	$tested = false;
	try {
		foreach($channels_data as $row) {
			$newRow = new stdClass();
			$newRow->project_id = $row['project_id'];
			$newRow->id = $row['id'];
			$newRow->name = $row['name'];
			$newRow->country = $row['country'];
			$newRow->valid = $row['valid'];
			$newRow->reviewed = $row['reviewed'];
			$newRow->date_found = $row['date_found'];
			$newRow->subscribers = $row['subscribers'];
			$newRow->views_avg = $row['views_avg'];
			$newRow->views_err = $row['views_err'];
			$newRow->update_date = $row['update_date'];
			$newRow->active = $row['active'];
			$newRow->evaluated = $row['evaluated'];
			$newRow->sim_games_count = $row['sim_games_count'];

			$similar_games_array = DatabaseHandler::getChannelSimilarGames($row['project_id'], $row['id']);
			$similar_games_string = '';

			if(!$tested) {
				error_log("Similar games count: " . count($similar_games_array));
				$tested = true;
			}

			foreach($similar_games_array as $sim_game) {
				$similar_games_string .= $sim_game['similar_game'] . ', ';
			}
			$similar_games_string = rtrim($similar_games_string, ', ');
			$newRow->sim_games_list = $similar_games_string;
			array_push($result, $newRow);
		}
	}
	catch(Exception $e) {
		$response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::INTERNAL_SERVER_ERROR, "Failed", $e->getTraceAsString()));
		return $response;
	}

	$response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", $result));
	return $response;
});

// $app->post('/dashboard/channel_review_csv', function($request, $response, $args) {
// 	$project_id = $request->getParsedBody()['project_id'];
// 	$result = DatabaseHandler::getChannelsDetailsEvaluationsForCSV($project_id);
// 	$response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", $result));
// 	return $response;
// });

$app->post('/dashboard/channel_review/list', function($request, $response, $args) {
	$params = null;
	
	if($request->getParsedBody()['fromDate'] != null) {
		$params = $request->getParsedBody();
	}
	else {
		$params = $request->getParams();
	}
	
	$projectId = $params['projectId'];
	$fromDate = $params['fromDate'];
	$toDate = $params['toDate'];
	$reviewed = strcmp($params['reviewed'], 'true') == 0 ? 1 : 0;
	
	$offset = $params['jtStartIndex'];
    $limit = $params['jtPageSize'];
    $sorting = $params['jtSorting'];
	
    $videos = DatabaseHandler::getChannelsForReview($projectId, $reviewed, $fromDate, $toDate, $sorting);
	$totalVideos = count($videos);
	$resultVideos = array();
	//filter the videos to return
	for($i = $offset; $i < $offset + $limit && $i < $totalVideos; ++$i) {
		array_push($resultVideos, $videos[$i]);
	}
	
    $response = JsonUtils::generateJTableResponse(ResponseCode::OK, "Success", $resultVideos, $totalVideos);
    return $response;
});

$app->post('/dashboard/channel_review/update', function($request, $response, $args) {
	$projectId = $request->getParsedBody()['project_id']; 
	$channel_id = $request->getParsedBody()['id'];
	$valid = $request->getParsedBody()['valid'] ?? null;
	$reviewed = $request->getParsedBody()['reviewed'] ?? null;
	
	$removed = false;
	if($reviewed != null) {
		$removed = true;
		DatabaseHandler::setChannelReviewed($projectId, $channel_id, $reviewed);
	}
	else if($valid != null) {
		DatabaseHandler::setChannelValid($projectId, $channel_id, $valid);
	}
	
	$result = new stdClass();
	$result->id = $channel_id;
	$result->removed = $removed;
	
	$response = JsonUtils::generateJTableNoRowsResponse(ResponseCode::OK, "Success", json_encode($result));
	
	return $response;
});

$app->post('/dashboard/channel_review/update_list', function($request, $response, $args) {
	$projectId = $request->getParsedBody()['project_id']; 
	$channel_ids= $request->getParsedBody()['channel_ids'];
	$valid_values = $request->getParsedBody()['valid_values'] ?? null;
	$reviewed = $request->getParsedBody()['reviewed'] ?? null;

	if($reviewed != null) {
		foreach($channel_ids as $channel_id) {
			DatabaseHandler::setChannelReviewed($projectId, $channel_id, $reviewed);
		}
	}
	else if($valid_values != null) {
		for($i = 0; $i < count($channel_ids); $i++) {
			DatabaseHandler::setVideoValid($projectId, $channel_ids[$i], $valid_values[$i]);
		}
	}
	
	$result = new stdClass();
	$result->channel_ids = $channel_ids;
	$result->removed = false;
	
	$response = JsonUtils::generateJTableNoRowsResponse(ResponseCode::OK, "Success", json_encode($result));
	
	return $response;
});
?>