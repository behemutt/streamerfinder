<?php
    $app->get('/dashboard/videos_stats/daily_update', function($request, $response, array $args) {
        //Reset quota before everything
        DatabaseHandler::resetYoutubeKeyQuota();
        $projectIds = DatabaseHandler::getProjects();
        
        foreach($projectIds as $project) {
            $projectId = $project['project_id'];
            $mostRecentQuery = DatabaseHandler::getMostRecentQuery($projectId);
            $query = $mostRecentQuery->data[0]['query_terms'];
            $fromDate = $mostRecentQuery->data[0]['initial_search_date'];

            #1 Get videos ids
            $videos = VideoFinder::searchVideos($projectId, $query, $fromDate);
        
            #2 Separate old from new videos ids
            $newVideos = VideoFinder::filterNewsVideos($videos);
            $oldVideos = VideoFinder::filterOldVideos($videos);
            
            #3 get the stats of the new and old videos
            $newsVideosData = VideoFinder::getVideosData($newVideos,true);
            $oldVideosData = VideoFinder::getVideosData($oldVideos,false);
        
            if($newsVideosData->status != ResponseCode::OK) {
                $response->getBody()->write(JsonUtils::generateJsonResponse($newsVideosData->status, $newsVideosData->data));
                continue;
            }
            if($oldVideosData->status != ResponseCode::OK) {
                $response->getBody()->write(JsonUtils::generateJsonResponse($newsVideosData->status, $oldVideosData->data));
                continue;
            }

            #4 insert new videos, update old ones
            $result = DatabaseHandler::insertVideos($projectId, $newsVideosData->data);
            if($result->status != ResponseCode::OK) {
                $response->getBody()->write(JsonUtils::generateJsonResponse($result->status, $result->data));
                continue;
            }

            $result = DatabaseHandler::updateVideos($oldVideosData->data);
            if($result->status != ResponseCode::OK) {
                $response->getBody()->write(JsonUtils::generateJsonResponse($result->status, $result->data));
                continue;
            }

            #5 insert videos stats
            $result = DatabaseHandler::insertVideoStats($projectId, $newsVideosData->data, true);
            if($result->status != ResponseCode::OK) {
                $response->getBody()->write(JsonUtils::generateJsonResponse($result->status, $result->data));
                continue;
            }

            $result = DatabaseHandler::insertVideoStats($projectId, $oldVideosData->data, false);
            if($result->status != ResponseCode::OK) {
                $response->getBody()->write(JsonUtils::generateJsonResponse($result->status, $result->data));
                continue;
            }
        }
        $response = JsonUtils::generateJsonResponse(ResponseCode::OK, "Success");
        return $response;
    });

    $app->post('/dashboard/videos_stats/collect', function($request, $response, array $args) {
        $projectId = $request->getParsedBody()["project_id"];

        $mostRecentQuery = DatabaseHandler::getMostRecentQuery($projectId);
        $query = $mostRecentQuery->data[0]['query_terms'];
        $fromDate = $mostRecentQuery->data[0]['initial_search_date'];

        #1 Get videos ids
        $videos = VideoFinder::searchVideos($projectId, $query, $fromDate);
    
        #2 Separate old from new videos ids
        $newVideos = VideoFinder::filterNewsVideos($videos);
        $oldVideos = VideoFinder::filterOldVideos($videos);
        
        #3 get the stats of the new and old videos
        $newsVideosData = VideoFinder::getVideosData($newVideos,true);
        $oldVideosData = VideoFinder::getVideosData($oldVideos,false);
    
        if($newsVideosData->status != ResponseCode::OK) {
            $response->getBody()->write(JsonUtils::generateJsonResponse($newsVideosData->status, $newsVideosData->data));
            return $response;
        }
        if($oldVideosData->status != ResponseCode::OK) {
            $response->getBody()->write(JsonUtils::generateJsonResponse($newsVideosData->status, $oldVideosData->data));
            return $response;
        }

        #4 insert new videos, update old ones
        $result = DatabaseHandler::insertVideos($projectId, $newsVideosData->data);
        if($result->status != ResponseCode::OK) {
            $response->getBody()->write(JsonUtils::generateJsonResponse($result->status, $result->data));
            return $response;
        }

        $result = DatabaseHandler::updateVideos($oldVideosData->data);
        if($result->status != ResponseCode::OK) {
            $response->getBody()->write(JsonUtils::generateJsonResponse($result->status, $result->data));
            return $response;
        }

        #5 insert videos stats
        $result = DatabaseHandler::insertVideoStats($projectId, $newsVideosData->data, true);
        if($result->status != ResponseCode::OK) {
            $response->getBody()->write(JsonUtils::generateJsonResponse($result->status, $result->data));
            return $response;
        }

        $result = DatabaseHandler::insertVideoStats($projectId, $oldVideosData->data, false);
        if($result->status != ResponseCode::OK) {
            $response->getBody()->write(JsonUtils::generateJsonResponse($result->status, $result->data));
            return $response;
        }

        $response = JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", $result->data);
        return $response;
    }
);

$app->get('/dashboard/videos_stats/chart_data', function($request, $response, array $args) {
    $projectId = $request->getParam('projectId');
    $fromDate = $request->getParam('fromDate');
    $toDate = $request->getParam('toDate');

    $data = DatabaseHandler::getDailyVideosStats($projectId, $fromDate, $toDate);
    $chartData = DatabaseHandler::processVideoStatsForChart($data);

    $response = JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", $chartData);
    return $response;
});

$app->get('/dashboard/videos_stats/test', function($request, $response, array $args) {
    $projectId = $request->getParam('projectId');
    $videoId = $request->getParam('videoId');
    
    $result = DatabaseHandler::insertVideoStatus($projectId, $videoId);
    $response = JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", $result);
    return $response;
});
?>