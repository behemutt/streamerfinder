<?php
    require_once '../api/classes/DouyuFinder.php';

    $app->get('/douyu/search/[{similarGames:.*}]', function ($request, $response, array $args) {
        $similarGames = explode('/',$request->getAttribute('similarGames'));
        $channelsData = DouyuFinder::searchChannels($similarGames);
	    $response = JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", $channelsData);
        return $response;
    });
  
?>