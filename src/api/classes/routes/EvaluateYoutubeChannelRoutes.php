<?php
    $app->post('/youtube/evaluator/{lastVideoAge}/[{similarGames:.*}]', function($request, $response, array $args) {
        $channelsData = $request->getParsedBody()['channelsData'];
        $lastVideoAge = $request->getAttribute('lastVideoAge');
        $similarGames = explode('/',$request->getAttribute('similarGames'));

        unset($similarGames[0]);
        $channelsDataParsed = import_csv_to_array($channelsData);

        $json = new stdClass();
        $data = YoutubeChannelEvaluator::evaluateChannels($channelsDataParsed, $lastVideoAge, $similarGames);

        $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, json_encode($json), $data));
    //    $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, 'Result:'.$keywords.$videoAge.$minSubscribers.$maxSubscribers));
        return $response;
    });

    $app->get('/youtube/evaluate_channels_daily', function($request, $response, array $args) {
        $project_id = 2;
        $result = YoutubeChannelEvaluator::evaluateChannelsDaily($project_id);
        $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, json_encode($result), $result));
        return $response;
    });

    function import_csv_to_array($csv_string, $enclosure = '"')
    {
        // Let's detect what is the delimiter of the CSV file
        $delimiter = detect_delimiter($csv_string);

        // Get all the lines of the CSV string
        $lines = explode("\n", $csv_string);

        // The first line of the CSV file is the headers that we will use as the keys
        $head = str_getcsv(array_shift($lines),$delimiter,$enclosure);

        $array = array();

        // For all the lines within the CSV
        foreach ($lines as $line) {

            // Sometimes CSV files have an empty line at the end, we try not to add it in the array
            if(empty($line)) {
                continue;
            }

            // Get the CSV data of the line
            $csv = str_getcsv($line,$delimiter,$enclosure);

            // Combine the header and the lines data
            $array[] = array_combine( $head, $csv );

        }

        // Returning the array
        return $array;
    }

    /**
     *
     * This function detects the delimiter inside the CSV file.
     *
     * It allows the function to work with different types of delimiters, ";", "," "\t", or "|"
     *
     *
     *
     * @param string $csv_string    The content of the CSV file
     * @return string               The delimiter used in the CSV file
     */
    function detect_delimiter($csv_string)
    {

        // List of delimiters that we will check for
        $delimiters = array(';' => 0,',' => 0,"\t" => 0,"|" => 0);

        // For every delimiter, we count the number of time it can be found within the csv string
        foreach ($delimiters as $delimiter => &$count) {
            $count = substr_count($csv_string,$delimiter);
        }

        // The delimiter used is probably the one that has the more occurrence in the file
        return array_search(max($delimiters), $delimiters);

    }

    $app->get('/youtube/evaluator/clear', function (Request $resquest, Response $response) {
        YoutubeChannelEvaluator::clearResults();
        return $response;
    });

    

?>