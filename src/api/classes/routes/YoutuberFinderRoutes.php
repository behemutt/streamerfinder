<?php
    $app->post('/search/insertOrUpdate', function($request, $response, array $args) {
        $project_id = $request->getParsedBody()['project_id'];
        $video_age = $request->getParsedBody()['video_age'];
        $similar_games = $request->getParsedBody()['similar_games'];
    
        $result = DatabaseHandler::insertOrUpdateSearch($project_id, $video_age);

        if($result->status == ResponseCode::OK) {

            foreach($similar_games as $game) {
                $result = DatabaseHandler::insertOrUpdateSimilarGame($project_id, $game);
            }
            unset($game);

            $allExistingGames = DatabaseHandler::getSimilarGames($project_id);

            foreach($allExistingGames as $game) {
                 if(in_array($game['name'], $similar_games)) {
                     //Do nothing
                 }
                 else
                 {
                     //remove games from db which are not in the new similar games list
                     DatabaseHandler::removeSimilarGame($project_id, $game['name']);
                 }
            }
            unset($game);

            if($result->status == ResponseCode::OK) {
                $response = JsonUtils::generateJTableInsertResponse($result->status, "Success", "");
            }
            else
            {
                $response = JsonUtils::generateJTableInsertResponse($result->status, $result->data);
            }
        }
        else {
            $response = JsonUtils::generateJTableInsertResponse($result->status, $result->data);
        }
    
        return $response;
    });

    //tests - BEGIN ===================
    // $app->get('/insertChannel/{project_id}/{channel_id}/{channel_name}/{channel_country}', function($request, $response, array $args) {
    //     $project_id = $request->getAttribute('project_id');
    //     $channel_id = $request->getAttribute('channel_id');
    //     $channel_name = $request->getAttribute('channel_name');
    //     $channel_country = $request->getAttribute('channel_country');
    //     $result = DatabaseHandler::insertChannelDetails($project_id, $channel_id, $channel_name, $channel_country);
        
    //     $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, json_encode($result), $result));
    //     return $response;
    // });

    // $app->get('/insertChannelSimilarGame/{project_id}/{channel_id}/{similar_game}', function($request, $response, array $args) {
    //     $project_id = $request->getAttribute('project_id');
    //     $channel_id = $request->getAttribute('channel_id');
    //     $similar_game = $request->getAttribute('similar_game');
    //     $result = DatabaseHandler::insertChannelSimilarGame($project_id, $channel_id, $similar_game);
        
    //     $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, json_encode($result), $result));
    //     return $response;
    // });

    // $app->get('/hasChannelSimilarGame/{project_id}/{channel_id}/{similar_game}', function($request, $response, array $args) {
    //     $project_id = $request->getAttribute('project_id');
    //     $channel_id = $request->getAttribute('channel_id');
    //     $similar_game = $request->getAttribute('similar_game');
    //     $result = DatabaseHandler::hasChannelSimilarGame($project_id, $channel_id, $similar_game);
        
    //     $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, json_encode($result), $result));
    //     return $response;
    // });

    // $app->get('/getChannelSimilarGames/{project_id}/{channel_id}', function($request, $response, array $args) {
    //     $project_id = $request->getAttribute('project_id');
    //     $channel_id = $request->getAttribute('channel_id');
    //     $result = DatabaseHandler::getChannelSimilarGames($project_id, $channel_id);
        
    //     $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, json_encode($result), $result));
    //     return $response;
    // });

    // $app->get('/getCurrentSearchSimilarGame/{project_id}', function($request, $response, array $args) {
    //     $project_id = $request->getAttribute('project_id');
    //     $result = DatabaseHandler::getCurrentSearchSimilarGame($project_id);
        
    //     $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, json_encode($result), $result));
    //     return $response;
    // });

    // $app->get('/updateCurrentSearchSimilarGame/{project_id}', function($request, $response, array $args) {
    //     $project_id = $request->getAttribute('project_id');
    //     $result = DatabaseHandler::updateCurrentSearchSimilarGame($project_id);
        
    //     $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, json_encode($result), $result));
    //     return $response;
    // });

    // $app->get('/getSearch/{project_id}', function($request, $response, array $args) {
    //     $project_id = $request->getAttribute('project_id');
    //     $result = DatabaseHandler::getSearch($project_id);
        
    //     $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, json_encode($result), $result));
    //     return $response;
    // });

    // $app->get('/executeSearch/{project_id}', function($request, $response, array $args) {
    //     $project_id = $request->getAttribute('project_id');
    //     $result = YoutuberFinder::executeSearch($project_id);
        
    //     $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, json_encode($result), $result));
    //     return $response;
    // });

    // $app->get('/insertOrUpdateChannelEvaluation/{channel_id}/{subscribers}/{views_avg}/{views_err}/{update_date}', function($request, $response, array $args) {
    //     $channel_id = $request->getAttribute('channel_id');
    //     $subscribers = $request->getAttribute('subscribers');
    //     $views_avg = $request->getAttribute('views_avg');
    //     $views_err = $request->getAttribute('views_err');
    //     $update_date = $request->getAttribute('update_date');

    //     $result = DatabaseHandler::insertOrUpdateChannelEvaluation($channel_id, $subscribers, $views_avg, $views_err, $update_date);
        
    //     $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, json_encode($result), $result));
    //     return $response;
    // });

    // $app->get('/getChannelSimilarGamesPerProjectId/{project_id}', function($request, $response, array $args) {
    //     $project_id = $request->getAttribute('project_id');
    //     $result = DatabaseHandler::getChannelSimilarGamesPerProjectId($project_id);
        
    //     $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, json_encode($result), $result));
    //     return $response;
    // });
    
    // $app->get('/initChannelSimilarGames/{project_id}', function($request, $response, array $args) {
    //     $project_id = $request->getAttribute('project_id');
    //     $result = DatabaseHandler::getChannelSimilarGamesPerProjectId($project_id);
        
    //     $today = new DateTime();
    //     $todayFormatted = $today->format('Y-m-d');

    //     foreach($result as $r) {
    //         if(!DatabaseHandler::hasChannelEvaluation($r['channel_id'])) {
    //             DatabaseHandler::insertOrUpdateChannelEvaluation($r['channel_id'], 0, 0, 0, $todayFormatted);
    //         }
    //     }

    //     $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, json_encode($result), $result));
    //     return $response;
    // });
    
    //tests - END ===================

    $app->get('/search/{clearSession}/{keywords}/{videoAge}/{minSubscribers}/{maxSubscribers}/{language}/[{similarGames:.*}]', function($request, $response, array $args) {
        $clearSession = $request->getAttribute('clearSession');

        if(strcmp($clearSession, 'true') == 0) {
            YoutuberFinder::clearResults();
        }

        $keywords = $request->getAttribute('keywords');
        $videoAge = $request->getAttribute('videoAge');
        $minSubscribers = $request->getAttribute('minSubscribers');
        $maxSubscribers = $request->getAttribute('maxSubscribers');
        $similarGames = explode('/',$request->getAttribute('similarGames'));
        $language = $request->getAttribute('language');
        
        $json = new stdClass();
        if(isset($_SESSION['evaluated_pages'])) {
            $json->evaluated_pages = $_SESSION['evaluated_pages'];
        }
        else {
            $json->evaluated_page = 0;
        }
        $data = YoutuberFinder::searchChannels($keywords, $videoAge, $minSubscribers, $maxSubscribers, $language, $similarGames);

        if(isset($_SESSION['similarGameIndex'])) {
            $json->similarGameIndex = $_SESSION['similarGameIndex'];
        }
        else {
            $json->similarGameIndex = 1;
        }

        $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, json_encode($json), $data));
    //    $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, 'Result:'.$keywords.$videoAge.$minSubscribers.$maxSubscribers));
        return $response;
    });

    $app->get('/clear', function (Request $resquest, Response $response) {
        YoutuberFinder::clearResults();
        return $response;
    });

    $app->get('/report_daily', function($request, $response, array $args) {
        $data = DatabaseHandler::getVideosFoundToday();

        $today = date('Y-m-d');
        //formatting e-mail
        $to = 'douglas@behemutt.com';
        $subject = "[LetsPlayerFinder] Videos found ".$today;
        $headers = "From: behemutt@gmail\n";
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\n";

        $message = '<html>';
        $message = '<head><style>';
        $message = 'table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;}td, th {border: 1px solid #dddddd;text-align: left;padding: 8px;}tr:nth-child(even) {background-color: #dddddd;}';
        $message = '</style></head>';
        $message = '<body>';
        $message .= '<table>';
        $message .= '<tr><th>Project</th><th>Thumb</th><th>URL</th><th>Title</th><th>Description</th><th>Project</th></tr>';

        foreach($data as $row) {
            $message .= '<tr>';
            $message .= '<td>'.$row['project_name'].'</td>';
            $message .= '<td>'.$row['thumbnail'].'</td>';
            $message .= '<td>https://www.youtube.com/watch?v='.$row['video_id'].'</td>';
            $message .= '<td>'.$row['video_title'].'</td>';
            $message .= '<td>'.$row['video_desc'].'</td>';
            $message .= '</tr>';
        }

        $message .= '</table>';
        $message = '</body></html>';
        $mailResponse = mail($to, $subject, $message, $headers);

        $resultSuccessMessage = '';
        if($mailResponse) {
            $resultSuccessMessage = 'true';
        }
        else{
            $resultSuccessMessage = 'false';
        }

        $response->getBody()->write(JsonUtils::generateJsonResponse(ResponseCode::OK, "Succeeded: " . $resultSuccessMessage));
        return $response;
    });

?>