<?php
    require_once '../api/classes/TwitchFinder.php';

    $app->get('/twitch/search/{online}/[{similarGames:.*}]', function ($request, $response, array $args) {
        $similarGames = explode('/',$request->getAttribute('similarGames'));
        $online = $request->getAttribute('online');

        $result = TwitchFinder::searchChannels($similarGames, $online);
	    $response = JsonUtils::generateJsonResponse(ResponseCode::OK, "Success", $result);
        return $response;
    });

    $app->get('/twitch/user_videos_views/[{userNames:.*}]', function ($request, $response, array $args) {
        $userNames = explode('/',$request->getAttribute('userNames'));

        $result = TwitchFinder::getUserVideos($userNames);
        $response->getBody()->write($result);
        return $response;
    });

    $app->get('/twitch/user_follows/[{userNames:.*}]', function ($request, $response, array $args) {
        $userNames = explode('/',$request->getAttribute('userNames'));

        $result = TwitchFinder::getUserFollowers($userNames);
        $response->getBody()->write($result);
        return $response;
    });

    $app->get('/twitch/user_last_video/[{userNames:.*}]', function ($request, $response, array $args) {
        $userNames = explode('/',$request->getAttribute('userNames'));

        $result = TwitchFinder::getUserLastVideo($userNames);
        $response->getBody()->write($result);
        return $response;
    });    
?>