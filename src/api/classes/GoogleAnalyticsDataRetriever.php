<?php

class GoogleAnalyticsDataRetriever {

    public static function initializeAnalytics()
    {
        $KEY_FILE_LOCATION = __DIR__ . '/../../keys/lp-finder-f8dd270f1d0d.json';

        // Create and configure a new client object.
        $client = new Google_Client();
        $client->setApplicationName("Let's player finder");
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        $analytics = new Google_Service_AnalyticsReporting($client);

        return $analytics;
    }

    public static function getReport($analytics, $fromDate, $toDate, $viewId) {

        // Replace with your view ID, for example XXXX.
        $VIEW_ID = $viewId;
      
        $query = [
            "viewId" => $VIEW_ID,
            "dateRanges" => [
                "startDate" => (string)$fromDate, "endDate" => (string)$toDate
            ],
            "metrics" => [
                "expression" => "ga:sessions"
            ],
            "dimensions" => [
                "name" => "ga:date"
            ],
        ];
      
        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests( array( $query) );
        return $analytics->reports->batchGet( $body );
      }

      public static function getDataPerRange($viewId, $fromDate, $toDate) {
	$analytics = self::initializeAnalytics();

	if($viewId === NULL) {
		$data = new stdClass();
		$data->dates = array();
		$data->sessions = array();
		$response = $data;
		return $response;
	}
	else {
        	$reports = self::getReport($analytics, $fromDate, $toDate, $viewId);
	}

        $dates = array();
        $sessions = array();

        for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
            $report = $reports[ $reportIndex ];
            $header = $report->getColumnHeader();
            $dimensionHeaders = $header->getDimensions();
            $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
            $rows = $report->getData()->getRows();
        
            for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
                $row = $rows[ $rowIndex ];
                $dimensions = $row->getDimensions();
                $metrics = $row->getMetrics();
                for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
                    $date = DateTime::createFromFormat('Ymd', $dimensions[$i]);
                    array_push($dates, $date->format('Y-m-d'));
                }
            
                for ($j = 0; $j < count($metrics); $j++) {
                    $values = $metrics[$j]->getValues();
                    for ($k = 0; $k < count($values); $k++) {
                        $entry = $metricHeaders[$k];
                        array_push($sessions, $values[$k]);
                    }
                }
            }
        }
        
        $data = new stdClass();
        $data->dates = $dates;
        $data->sessions = $sessions;

        $response = $data;
	    return $response;
      }
}
// $app->post('/dashboard/google_analytics/list', function($request, $response, array $args) {
// 	$toDate = $request->getParsedBody()['toDate'];
//     $fromDate = $request->getParsedBody()['fromDate'];

//     $analytics = GoogleAnalyticsManager::initializeAnalytics();
//     $reports = GoogleAnalyticsManager::getReport($analytics, $fromDate, $toDate);

//     $dates = array();
//     $sessions = array();

//     for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
//         $report = $reports[ $reportIndex ];
//         $header = $report->getColumnHeader();
//         $dimensionHeaders = $header->getDimensions();
//         $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
//         $rows = $report->getData()->getRows();
    
//         for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
//           $row = $rows[ $rowIndex ];
//           $dimensions = $row->getDimensions();
//           $metrics = $row->getMetrics();
//           for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
//             $date = DateTime::createFromFormat('Ymd', $dimensions[$i]);
//             array_push($dates, $date->format('Y-m-d'));
//           }
    
//           for ($j = 0; $j < count($metrics); $j++) {
//             $values = $metrics[$j]->getValues();
//             for ($k = 0; $k < count($values); $k++) {
//               $entry = $metricHeaders[$k];
//               array_push($sessions, $values[$k]);
//             }
//           }
//         }
//       }
    
//     $data = new stdClass();
//     $data->dates = $dates;
//     $data->sessions = $sessions;

//     $response = JsonUtils::generateJsonResponse(ResponseCode::OK, "Success ", $data);
// 	return $response;
// });
?>
