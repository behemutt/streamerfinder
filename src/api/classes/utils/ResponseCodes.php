<?php
class ResponseCode {
    //Signup    
    const DUPLICATED_EMAIL = 1;
    const DUPLICATED_NICK = 2;
    const INVALID_EMAIL = 3;
    const INVALID_NICKNAME_SIZE = 4;
    const INVALID_PASSWORD = 5;
    const PASSWORDS_DOESNT_MATCH = 6;
    
    //HTTP statuses
    const OK = 200;
    const CREATED = '201';
    const ACCEPTED = 202;
    
    const BAD_REQUEST = 400;
    const UNAUTHORIZED = 401;
    const PAYMENT_REQUIRED = 402;
    const FORBIDDEN = 403;
    const NOT_FOUND = 404;
    
    const INTERNAL_SERVER_ERROR = 500;
    const NOT_IMPLEMENTED = 501;
    const SERVICE_TEMPORARYLY_OVERLOADED = 502;
    const GATEWAY_TIMEOUT = 503;
}
?>