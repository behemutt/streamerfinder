<?php
class JsonUtils {
	
	public static function generateJsonResponse($status, $message, $data = null) {
		$json = new stdClass();
		$json->status = $status;
		$json->message = utf8_encode($message);
        
		if ($data != null) {
			$json->data = $data;
		}
		
		return json_encode($json);
	}

	public static function generateJTableResponse($status, $message, $data = null, $totalRecords) {
		$json = new stdClass();
		if($status==200){
			$json->Result = "OK";
		}
		else
		{
			$json->Result = "ERROR";
		}
		$json->Message = utf8_encode($message);
		
		if ($data != null) {
			$json->Records = $data;
		}
		
		$json->TotalRecordCount = $totalRecords;
		return json_encode($json, JSON_UNESCAPED_SLASHES);
	}
	
	public static function generateJTableNoRowsResponse($status, $message, $data = null) {
		$json = new stdClass();
		if($status==200){
			$json->Result = "OK";
		}
		else
		{
			$json->Result = "ERROR";
		}
		$json->Message = utf8_encode($message);
		
		if ($data != null) {
			$json->Records = $data;
		}
		else{
			$json->Records = 'no data';
		}

		return json_encode($json, JSON_UNESCAPED_SLASHES);
	}

	public static function generateJTableInsertResponse($status, $message, $data = null) {
		$json = new stdClass();
		if($status==200){
			$json->Result = "OK";
		}
		else
		{
			$json->Result = "ERROR";
		}
		$json->Message = utf8_encode($message);
		if ($data != null) {
			$json->Record = $data;
		}
		return json_encode($json, JSON_UNESCAPED_SLASHES);
	}
} 
?>
