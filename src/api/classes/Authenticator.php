<?php

class Authenticator
{
    
    private $user;
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
        
    
    
    #Method to chec if user has active session
    public function check($user, $pass) {

        if (!isset($pass) or !isset($this->data['user_password'])) {
            return false;
        }

         //if (password_verify($pass, $this->data['user_password'])) {
        if($pass == $this->data['user_password']) {
            return true;
        }

        return false;
    }

    #Setting Data in Seesions
    public function access()
    {
        $_SESSION['user'] = $this->data;
    }

}
