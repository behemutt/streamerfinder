<?php
    require_once '../vendor/sunra/php-simple-html-dom-parser/Src/Sunra/PhpSimple/HtmlDomParser.php';

    use Sunra\PhpSimple\HtmlDomParser;

    class DouyuFinder {

        private static $DOUYU_LINK = "https://v.douyu.com";

        public static function searchChannels($similarGames) {
            $videos = self::getVideosFromUniqueChannels($similarGames);
            $uniqueChannels = self::extractChannelsFromVideos($videos);
            return $uniqueChannels;
        }

        private static function getVideosFromUniqueChannels($similarGames) {
            $videos = array();
            $channelNames = array();

            foreach($similarGames as $game) {
                $numberOfPages = 1;
                $page = 1;
                $numPagesSet = false;
                do {
                    $url = 'https://www.douyu.com/search/?kw=' . urlencode($game) . '&label=video&type=relevant&page='.$page;
                    $root = HtmlDomParser::file_get_html( $url );
                    $elems = $root->find('a.list-item');
                    $page++;

                    if(!$numPagesSet) {
                        $numPagesSet = true;
                        $numPageElement = $root->find('div.tcd-page-code');

                        if(count($numPageElement) > 0) {
                            $numberOfPages = $numPageElement[0]->{'data-pagecount'};
                        }
                    }

                    foreach($elems as $video) {
                        $channelName = $video->find('span')[1]->find('span')[1]->find('strong')[0]->innertext;

                        if(mb_strpos(mb_strtolower($video->title), mb_strtolower($game)) !== false) {
                            if(!in_array($channelName, $channelNames)) {
                                $videoData = new stdClass();
                                $videoData->title = $video->title;
                                $videoData->url = $video->href;
                                $videoData->channelName = $channelName;
                                $videoData->gameFound = $game;
                                array_push($videos, $videoData);
                            }
                        }
                    }
                } while($page <= $numberOfPages);
            }
            return $videos;
        }

        private static function extractChannelsFromVideos($videos) {
            $channels = array();
            $channelsData = array();

            foreach($videos as $video) {
                $html = HtmlDomParser::file_get_html( $video->url );
                $authorLink = $html->find('div.author-info')[0]->find('a')[0]->href;

                if(!in_array($authorLink, $channels)) {
                    $authorContactLink = $html->find('div.author-info')[0]->find('span')[1]->find('a')[0]->href;

                    $htmlAuthor =  HtmlDomParser::file_get_html( self::$DOUYU_LINK . $authorLink );
                    $followers = $htmlAuthor->find('p.up-des')[2]->{'data-fnum'};
                    $totalViews =$htmlAuthor->find('p.up-des')[1]->innertext;
                    $lastActivityString = $htmlAuthor->find('div.label-list')[0]->find('span')[1]->innertext;
                    $lastActivity = mb_substr($lastActivityString, mb_strpos($lastActivityString, '：') + 1);
                    $lastActivity = strtotime($lastActivity);
                    $lastActivity = date('Y-m-d', $lastActivity);

                    $channel = new stdClass();
                    $channel->authorPage = self::$DOUYU_LINK . $authorLink;
                    $channel->contactPage = mb_substr($authorContactLink, 2);
                    $channel->totalViews = $totalViews;
                    $channel->followers = $followers;
                    $channel->lastActivity = $lastActivity;
                    $channel->gameFound = $video->gameFound;
                    array_push($channelsData, $channel);
                    array_push($channels, $authorLink);
                }
            }
            return $channelsData;
        }
    }
?>