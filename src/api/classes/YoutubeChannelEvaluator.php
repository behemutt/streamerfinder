<?php

class YoutubeChannelEvaluator {
	private static $API_KEY = "AIzaSyBagTzijrSdGdJCxj0wlyWFxi253hQ9Vlo"; //production key
	// private static $API_KEY = "AIzaSyC4wdeY2XfY0z2cequFQE5pRPfCg_0zFgM"; //Test key 2
	//private static $API_KEY = "AIzaSyDeoSwdFdZRNfSThnP6VHgS17bmN7P4n_g"; //Test key 3
	private static $client;
	private static $youtube;
	private static $MAX_VIDEOS_PER_CHANNEL = 25;
	private static $QUOTA_SEARCH_LIST = 100;
	private static $QUOTA_VIDEOS_LIST = 1;
	private static $QUOTA_CHANNELS_LIST = 1;
	private static $MIN_QUOTA_TO_BEGIN_EVAL = 500;

	private static $MAX_VIDEO_AGE_DAYS = 30;

	private static $MAX_EVALUATION_DAILY_QUOTA = 3200;

	public static function stats_standard_deviation(array $a, $sample = false) {
		$n = count($a);
		if ($n === 0) {
			trigger_error("The array has zero elements", E_USER_WARNING);
			return false;
		}
		if ($sample && $n === 1) {
			trigger_error("The array has only 1 element", E_USER_WARNING);
			return false;
		}
		$mean = array_sum($a) / $n;
		$carry = 0.0;
		foreach ($a as $val) {
			$d = ((double) $val) - $mean;
			$carry += $d * $d;
		};
		if ($sample) {
		   --$n;
		}
		return sqrt($carry / $n);
	}

	public static function exception_error_handler($errno, $errstr, $errfile, $errline) {
		throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
	}

	public static function initApiKey() {
		$key = self::$API_KEY;//$key_data['api_key'];
		self::$client = new Google_Client();

		self::$client->setApplicationName("Youtube Reviewer Finder");
		self::$client->setDeveloperKey($key);

		self::$youtube = new Google_Service_YouTube(self::$client);
		return true;
	}

	public static function evaluateChannelsDaily($project_id) {
		$channels = DatabaseHandler::getChannelsDetailsEvaluationsForEvaluation($project_id);

		$quotaLimit = self::$MAX_EVALUATION_DAILY_QUOTA;
		$required_quota = self::$QUOTA_SEARCH_LIST + (self::$QUOTA_CHANNELS_LIST + self::$MAX_VIDEOS_PER_CHANNEL * self::$QUOTA_VIDEOS_LIST);

		$cur_consumed_quota = 0;

		foreach($channels as $c)
		{
			if(DatabaseHandler::hasQuota($required_quota) && ($cur_consumed_quota + $required_quota) < $quotaLimit) {
				$cur_consumed_quota += $required_quota;
				self::evaluateSingleChannel($c['id'], self::$MAX_VIDEO_AGE_DAYS);
			}
			else {
				break;
			}
		}

		$result = new stdClass();
		$result->status = ResponseCode::OK;
		$result->data = "Success!";
		return $result;
	}

	public static function evaluateSingleChannel($channel_id, $last_video_age) {
		try {
			self::initApiKey();

			$today = new DateTime();
			$todayFormatted = $today->format('Y-m-d');
			$maxVideoAge = new DateTime();
			$maxVideoAge->sub(new DateInterval('P'.$last_video_age.'M'));
			$publishedAfter = $maxVideoAge->format('Y-m-d\TH:i:s.s\Z');

			$channel_stats = self::$youtube->channels->listChannels('snippet,contentDetails,statistics,brandingSettings', array(
				'id' => $channel_id
				));
			
			$channel_title = $channel_stats["items"][0]['snippet']['title'];
			$subscriberCount = $channel_stats["items"][0]['statistics']['subscriberCount'];
			$country = $channel_stats["items"][0]['snippet']['country'];

			if(!empty($country)) {
				DatabaseHandler::updateChannelCountry($channel_id, $country);
			}

			DatabaseHandler::updateYoutubeKeyQuota(self::$QUOTA_CHANNELS_LIST, 0);

			$channel_data = self::getChannelDataSimple($channel_id, $channel_title, $subscriberCount, $publishedAfter);
		
			if(!is_null($channel_data)) {
				$active = $channel_data['active'] ? 1 : 0;
				if(!DatabaseHandler::hasChannelEvaluation($channel_id)) {
					DatabaseHandler::insertOrUpdateChannelEvaluation($channel_data['channelId'], $subscriberCount, $channel_data['viewsAvg'], $channel_data['viewsError'], $todayFormatted, $active, 1);
				}
				else
				{
					DatabaseHandler::updateChannelEvaluation($channel_data['channelId'], $subscriberCount, $channel_data['viewsAvg'], $channel_data['viewsError'], $todayFormatted, $active, 1);
				}
				return true;
			}
			else {
				return false;
			}
		} catch (Google_Service_Exception $e) {
			error_log($e);
			return false;
		} catch (Google_Exception $e) {
			error_log($e);
			return false;
		} catch (Exception $e) {
			error_log($e);
			return false;
		}
	}

	public static function evaluateChannels($channelsData, $lastVideoAge, $similarGames) {
		//set_error_handler("YoutubeChannelEvaluator::exception_error_handler");
		if(!self::initApiKey()) {
			error_log("No valid api key to evaluate");
			return array();
		}

		try {
			$maxVideoAge = new DateTime();
			$maxVideoAge->sub(new DateInterval('P'.$lastVideoAge.'M'));
			$publishedAfter = $maxVideoAge->format('Y-m-d\TH:i:s.s\Z');

			$result = array();

			error_log("Evaluating channels");
			foreach($channelsData as $channelData) {
				$channel_data = self::getChannelData($channelData, $publishedAfter, $similarGames);
				if(!is_null($channel_data)) {
					error_log("Adding channel");
					array_push($result, $channel_data);
				}
				else {
					error_log("Invalid channel");
				}
			}
			return $result;

		} catch (Google_Service_Exception $e) {
			error_log($e);
			return array();
		} catch (Google_Exception $e) {
			error_log($e);
			return array();
		} catch (Exception $e) {
			error_log($e);
			return array();
		}
	}

	private static function getChannelData($channelData, $publishedAfter, $similarGames) {
		$channelId = str_replace("https://www.youtube.com/channel/", "", $channelData["Channel Link"]);
		$channelTitle = $channelData['Channel Name'];
		$subscriberCount = $channelData['Subscriber Count'];

		if(!self::initApiKey()) {
			error_log("No valid api key to evaluate");
			return null;
		}

		DatabaseHandler::updateYoutubeKeyQuota(self::$QUOTA_SEARCH_LIST, 0);

		$searchResponse = self::$youtube->search->listSearch('snippet', array(
			'channelId' => $channelId,
			'maxResults' => self::$MAX_VIDEOS_PER_CHANNEL,
			'publishedAfter' => $publishedAfter,
			'order' => "date",
			"type" => "video",
		));

		$views = 0;
		$likes = 0;
		$dislikes = 0;
		$comments = 0;
		$itemsCount = count($searchResponse['items']);

		$viewsAvg = 0;
		$likesAvg = 0;
		$dislikesAvg = 0;
		$commentsAvg = 0;

		$viewsError = 0;
		$likesError = 0;
		$dislikesError = 0;
		$commentsError = 0;

		$videosStats = array();

		if($itemsCount > 0) {
			//Sum the totals
			foreach($searchResponse['items'] as $searchResult) {
				$title = $searchResult['snippet']['title'];
				$desc = $searchResult['snippet']['description'];

				foreach($similarGames as $sg) {
					if(strpos(strtolower($title), strtolower($sg)) !== false ||
					   strpos(strtolower($desc), strtolower($sg)) !== false) {
					}
				}

				if(!self::initApiKey()) {
					error_log("No valid api key to evaluate");
					return null;
				}
		
				DatabaseHandler::updateYoutubeKeyQuota(self::$QUOTA_VIDEOS_LIST, 0);

				$videoDataResponse = self::$youtube->videos->listVideos('statistics', array(
					'id' => $searchResult['id']['videoId'],
				));
				
				array_push($videosStats, $videoDataResponse['items'][0]['statistics']);

				$comments += $videoDataResponse['items'][0]['statistics']['commentCount'];
				$dislikes += $videoDataResponse['items'][0]['statistics']['dislikeCount'];
				$likes += $videoDataResponse['items'][0]['statistics']['likeCount'];
				$views += $videoDataResponse['items'][0]['statistics']['viewCount'];
			}

			//Calculate averages
			$viewsAvg = $views / $itemsCount;
			$likesAvg = $likes / $itemsCount;
			$dislikesAvg = $dislikes / $itemsCount;
			$commentsAvg = $comments / $itemsCount;

			//Calculate error
			if($itemsCount > 1) {
				foreach($videosStats as $stats) {
					//comments
					$diff = $stats['commentCount'] - $commentsAvg;
					$commentsError += $diff * $diff;
					//dislikes
					$diff = $stats['dislikeCount'] - $dislikesAvg;
					$dislikesError += $diff * $diff;
					//likes
					$diff = $stats['likeCount'] - $likesAvg;
					$likesError += $diff * $diff;
					//views
					$diff = $stats['viewCount'] - $viewsAvg;
					$viewsError += $diff * $diff;
				}

				$commentsError = round(sqrt($commentsError / ($itemsCount - 1)));
				$likesError = round(sqrt($likesError / ($itemsCount - 1)));
				$dislikesError = round(sqrt($dislikesError / ($itemsCount - 1)));
				$viewsError = round(sqrt($viewsError / ($itemsCount - 1)));
			}
		}

		$resultFormatted['channelId'] = $channelId;
		$resultFormatted['channelTitle'] = $channelTitle;
		$resultFormatted['subscriberCount'] = $subscriberCount;
		$resultFormatted['views'] = $views;
		$resultFormatted['comments'] = $comments;
		$resultFormatted['dislikes'] = $dislikes;
		$resultFormatted['likes'] = $likes;
		$resultFormatted['viewsAvg'] = $viewsAvg;
		$resultFormatted['commentsAvg'] = $commentsAvg;
		$resultFormatted['dislikesAvg'] = $dislikesAvg;
		$resultFormatted['likesAvg'] = $likesError;
		$resultFormatted['viewsError'] = $viewsError;
		$resultFormatted['commentsError'] = $commentsError;
		$resultFormatted['dislikesError'] = $dislikesError;
		$resultFormatted['likesError'] = $likesError;
		$resultFormatted['refsToSimilarGames'] = $referencesToSimilarGames;
		$resultFormatted['active'] = $itemsCount > 0;

		return $resultFormatted;
	}

	private static function getChannelDataSimple($channelId, $channelTitle, $subscriberCount, $publishedAfter) {
		
		DatabaseHandler::updateYoutubeKeyQuota(self::$QUOTA_SEARCH_LIST, 0);

		$searchResponse = self::$youtube->search->listSearch('snippet', array(
			'channelId' => $channelId,
			'maxResults' => self::$MAX_VIDEOS_PER_CHANNEL,
			'publishedAfter' => $publishedAfter,
			'order' => "date",
			"type" => "video",
		));

		$itemsCount = count($searchResponse['items']);

		$views = 0;
		$viewsList = array();
		$viewsAvg = 0;
		$viewsError = 0;

		$videosStats = array();
		$referencesToSimilarGames = 0;

		if($itemsCount > 0) {
			//Sum the totals
			foreach($searchResponse['items'] as $searchResult) {
		
				DatabaseHandler::updateYoutubeKeyQuota(self::$QUOTA_VIDEOS_LIST, 0);

				$videoDataResponse = self::$youtube->videos->listVideos('statistics', array(
					'id' => $searchResult['id']['videoId'],
				));
				
				array_push($videosStats, $videoDataResponse['items'][0]['statistics']);
				$viewCount = $videoDataResponse['items'][0]['statistics']['viewCount'];
				$views += $viewCount;
				array_push($viewsList, $viewCount);
			}

			//Calculate averages
			$viewsAvg = $views / $itemsCount;
			//Calculate error
			if($itemsCount > 1) {
				$viewsError = self::stats_standard_deviation($viewsList);
			}
		}

		$resultFormatted['channelId'] = $channelId;
		$resultFormatted['channelTitle'] = $channelTitle;
		$resultFormatted['subscriberCount'] = $subscriberCount;
		$resultFormatted['views'] = $views;
		$resultFormatted['viewsAvg'] = $viewsAvg;
		$resultFormatted['viewsError'] = $viewsError;
		$resultFormatted['active'] = $itemsCount > 0;

		return $resultFormatted;
	}

	public static function clearResults() {
		if(isset($_SESSION['used_channels'])) {
			unset($_SESSION['used_channels']);
		}
		if(isset($_SESSION['similarGameIndex'])) {
			unset($_SESSION['similarGameIndex']);
		}
	}
}
?>
